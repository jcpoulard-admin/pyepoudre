<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">
<html lang="fr" dir="ltr" version="HTML+RDFa 1.1"
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://www.cda95.fr/sites/centredesarts-v2/themes/omega3/favicon.ico" type="image/vnd.microsoft.icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no" />
<meta name="generator" content="Drupal 7 (https://www.drupal.org)" />
<link rel="canonical" href="https://www.cda95.fr/fr" />
<link rel="shortlink" href="https://www.cda95.fr/fr" />
  <title>Centre des arts |</title>  
  <link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_vZ_wrMQ9Og-YPPxa1q4us3N7DsZMJa-14jShHgRoRNo.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_5MxKZl0c8mrvT0SjInNfluIP5u64TezWSRybEY4u76Q.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_qHtObQL3PlqKgunDhvpOkbSopXfrIiY7QMSX-1rDDkI.css" media="all" />
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
#sliding-popup.sliding-popup-bottom,#sliding-popup.sliding-popup-bottom .eu-cookie-withdraw-banner,.eu-cookie-withdraw-tab{background:#000000;}#sliding-popup.sliding-popup-bottom.eu-cookie-withdraw-wrapper{background:transparent}#sliding-popup .popup-content #popup-text h1,#sliding-popup .popup-content #popup-text h2,#sliding-popup .popup-content #popup-text h3,#sliding-popup .popup-content #popup-text p,#sliding-popup label,#sliding-popup div,.eu-cookie-compliance-secondary-button,.eu-cookie-withdraw-tab{color:#fff !important;}.eu-cookie-withdraw-tab{border-color:#fff;}.eu-cookie-compliance-more-button{color:#fff !important;}

/*]]>*/-->
</style>
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_C0teG_IthjCkExFomHXH8rIru5rrGEXaBscqZtnLMJk.css" media="all" />

<!--[if (lt IE 9)&(!IEMobile)]>
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_U-7qLYlbjDH36bcKqVy8PEgBWOrOAF07oWlZbCgUSok.css" media="all" />
<![endif]-->

<!--[if gte IE 9]><!-->
<link type="text/css" rel="stylesheet" href="https://www.cda95.fr/sites/centredesarts-v2/files/css/css_pHP7YiqCfw88r1NAiDrqvMbJwieyKhH0VyQaUv1fddc.css" media="all" />
<!--<![endif]-->
  <script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_Y0h5bfLnfnDMuXvZ68knECFX2MBKKLVm4NaVQIcb4RE.js"></script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js__9oq5QJIsQfvpXuUsWT7ml6kV6hGwGkrQDoVcRjpKt8.js"></script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_VTEtv7_7mAZG2WJaoSRGOAV5Q0K0EC4ORpUKEjR5gZ8.js"></script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_N7XQ0J10aSpRhs_2KGUdyPVUpQCNU71JdTyRCg4nlUc.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--

    jQuery(document).ready(function($){$('#om-maximenu-menu-mobile li.om-leaf .om-maximenu-content').css('position', 'relative').addClass('om-maximenu-displace');     
      $('#om-maximenu-menu-mobile li.om-leaf .om-maximenu-content').removeClass('om-maximenu-content-nofade');
      $('#om-maximenu-menu-mobile li.om-leaf .om-link').click(function(ev) {
        ev.stopPropagation();
        if($(this).parent().hasClass('open')) {
          $(this).parent().removeClass('open');
        }
        else {
          $('#om-maximenu-menu-mobile li.om-leaf').removeClass('open');
          $(this).parent().addClass('open');
        }  
        
        $(this).parent().siblings().children('.om-maximenu-content').slideUp('slow');        
        $(this).siblings('.om-maximenu-content').toggle('slow');       
        return false;
      });
      $('#om-maximenu-menu-mobile .om-maximenu-content').click(function(ev) {
        ev.stopPropagation();
      });        
      $('html').click(function() {
        // Close any open menus.
        $('#om-maximenu-menu-mobile .om-maximenu-content:visible').each(function() {
          $(this).parent().removeClass('open');
          $(this).hide();
        });
      });      
    }); 
    
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_rsGiM5M1ffe6EhN-RnhM5f3pDyJ8ZAPFJNKpfjtepLk.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(i,s,o,g,r,a,m){i["GoogleAnalyticsObject"]=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,"script","https://www.google-analytics.com/analytics.js","ga");ga("create", "UA-24180753-1", {"cookieDomain":".cda95.fr"});ga("require", "displayfeatures");ga("send", "pageview");
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_3CJ_VXeHLMjDyJrpWxZUuEB980Pj1t36z5WFWWX31zo.js"></script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_43n5FBy8pZxQHxPXkf-sQF7ZiacVZke14b0VlvSA554.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"fr\/","ajaxPageState":{"theme":"omega3","theme_token":"x9w-QYcbirirTkOfmH_DBqdpgyiMR6MHD2J8dNPFnI8","js":{"0":1,"1":1,"sites\/centredesarts-v2\/modules\/eu_cookie_compliance\/js\/eu_cookie_compliance.js":1,"sites\/all\/modules\/jquery_update\/replace\/jquery\/1.8\/jquery.js":1,"misc\/jquery-extend-3.4.0.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/views_slideshow\/js\/views_slideshow.js":1,"sites\/centredesarts-v2\/modules\/nice_menus\/js\/jquery.bgiframe.js":1,"sites\/centredesarts-v2\/modules\/nice_menus\/js\/jquery.hoverIntent.js":1,"sites\/centredesarts-v2\/modules\/nice_menus\/js\/superfish.js":1,"sites\/centredesarts-v2\/modules\/nice_menus\/js\/nice_menus.js":1,"sites\/all\/modules\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/centredesarts-v2\/modules\/beautytips\/js\/jquery.bt.min.js":1,"sites\/centredesarts-v2\/modules\/beautytips\/js\/beautytips.min.js":1,"sites\/all\/modules\/om_maximenu\/js\/om_maximenu.js":1,"sites\/all\/modules\/extlink\/extlink.js":1,"public:\/\/languages\/fr_f4J_rEwvNzl9bUt0fRe1Pirn8vNl3QYH5hZlPBQL9Cs.js":1,"sites\/centredesarts-v2\/modules\/cleantalk\/src\/js\/apbct-public.js":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_load.js":1,"sites\/all\/modules\/colorbox\/js\/colorbox_inline.js":1,"sites\/all\/libraries\/jquery.cycle\/jquery.cycle.all.min.js":1,"sites\/all\/libraries\/json2\/json2.js":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/js\/views_slideshow_cycle.js":1,"2":1,"sites\/all\/modules\/google_analytics\/googleanalytics.js":1,"3":1,"sites\/centredesarts-v2\/themes\/omega3\/js\/gallimedia.js":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/js\/jquery.formalize.js":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/js\/omega-mediaqueries.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/simplenews\/simplenews.css":1,"sites\/centredesarts-v2\/modules\/calendar_tooltips\/calendar_tooltips.css":1,"modules\/comment\/comment.css":1,"sites\/all\/modules\/date\/date_api\/date.css":1,"sites\/all\/modules\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/om_maximenu\/css\/om_maximenu.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/calendar\/css\/calendar_multiday.css":1,"sites\/all\/modules\/extlink\/extlink.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/colorbox\/styles\/default\/colorbox_style.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/panels\/plugins\/layouts\/flexible\/flexible.css":1,"sites\/all\/modules\/views_slideshow\/contrib\/views_slideshow_cycle\/views_slideshow_cycle.css":1,"public:\/\/ctools\/css\/ad636f917b8db4b746720e51283c76c8.css":1,"modules\/locale\/locale.css":1,"sites\/all\/modules\/om_maximenu\/skin\/no_style\/no_style.css":1,"sites\/centredesarts-v2\/modules\/nice_menus\/css\/nice_menus.css":1,"sites\/centredesarts-v2\/themes\/omega3\/css\/menu.css":1,"sites\/centredesarts-v2\/modules\/eu_cookie_compliance\/css\/eu_cookie_compliance.css":1,"0":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/alpha-reset.css":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/alpha-mobile.css":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/alpha-alpha.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/formalize.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/omega-text.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/omega-branding.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/omega-menu.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/omega-forms.css":1,"sites\/centredesarts-v2\/themes\/omega\/omega\/css\/omega-visuals.css":1,"sites\/centredesarts-v2\/themes\/omega3\/css\/global.css":1,"ie::normal::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default.css":1,"ie::normal::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default-normal.css":1,"ie::normal::sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"narrow::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default.css":1,"narrow::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default-narrow.css":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/grid\/alpha_default\/narrow\/alpha-default-narrow-12.css":1,"normal::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default.css":1,"normal::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default-normal.css":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/grid\/alpha_default\/normal\/alpha-default-normal-12.css":1,"wide::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default.css":1,"wide::sites\/centredesarts-v2\/themes\/omega3\/css\/omega3-alpha-default-wide.css":1,"sites\/centredesarts-v2\/themes\/omega\/alpha\/css\/grid\/alpha_default\/wide\/alpha-default-wide-12.css":1}},"colorbox":{"transition":"elastic","speed":"350","opacity":"0.90","slideshow":false,"slideshowAuto":true,"slideshowSpeed":"2500","slideshowStart":"start slideshow","slideshowStop":"stop slideshow","current":"{current} sur {total}","previous":"\u00ab Pr\u00e9c","next":"Suiv \u00bb","close":"Fermer","overlayClose":true,"returnFocus":true,"maxWidth":"100%","maxHeight":"100%","initialWidth":"300","initialHeight":"100","fixed":true,"scrolling":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"jcarousel":{"ajaxPath":"\/fr\/jcarousel\/ajax\/views"},"viewsSlideshow":{"carrousel-block_1_1":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"pause":["viewsSlideshowControls","viewsSlideshowCycle"],"play":["viewsSlideshowControls","viewsSlideshowCycle"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowCycle"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowPager":{"carrousel-block_1_1":{"bottom":{"type":"viewsSlideshowPagerFields","master_pager":"0"}}},"viewsSlideshowPagerFields":{"carrousel-block_1_1":{"bottom":{"activatePauseOnHover":0}}},"viewsSlideshowCycle":{"#views_slideshow_cycle_main_carrousel-block_1_1":{"num_divs":3,"id_prefix":"#views_slideshow_cycle_main_","div_prefix":"#views_slideshow_cycle_div_","vss_id":"carrousel-block_1_1","effect":"scrollHorz","transition_advanced":0,"timeout":5000,"speed":700,"delay":0,"sync":1,"random":0,"pause":1,"pause_on_click":0,"play_on_hover":0,"action_advanced":1,"start_paused":0,"remember_slide":0,"remember_slide_days":1,"pause_in_middle":0,"pause_when_hidden":0,"pause_when_hidden_type":"full","amount_allowed_visible":"","nowrap":0,"pause_after_slideshow":0,"fixed_height":1,"items_per_slide":1,"wait_for_image_load":1,"wait_for_image_load_timeout":5000,"cleartype":0,"cleartypenobg":0,"advanced_options":"{}","advanced_options_choices":0,"advanced_options_entry":""}},"urlIsAjaxTrusted":{"\/":true},"nice_menus_options":{"delay":"800","speed":"slow"},"eu_cookie_compliance":{"popup_enabled":1,"popup_agreed_enabled":0,"popup_hide_agreed":0,"popup_clicking_confirmation":1,"popup_scrolling_confirmation":0,"popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--default\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003E\tNous utlisons des cookies sur ce site pour am\u00e9liorer l\u0027exp\u00e9rience utilisateur.\u003C\/p\u003E\n\u003Cp\u003E\tEn cliquant sur un lien de cette page, vous consentez \u00e0 l\u0027installation de cookies.\u003C\/p\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EJe souhaite plus d\u0027inforrmation.\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, je suis d\u0027accord\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","use_mobile_message":false,"mobile_popup_html_info":"\u003Cdiv class=\u0022eu-cookie-compliance-banner eu-cookie-compliance-banner-info eu-cookie-compliance-banner--default\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n                    \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button\u0022\u003EJe souhaite plus d\u0027inforrmation.\u003C\/button\u003E\n          \u003C\/div\u003E\n    \n    \u003Cdiv id=\u0022popup-buttons\u0022 class=\u0022\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022agree-button eu-cookie-compliance-default-button\u0022\u003EOK, je suis d\u0027accord\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","mobile_breakpoint":"768","popup_html_agreed":"\u003Cdiv\u003E\n  \u003Cdiv class=\u0022popup-content agreed\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Ch2\u003E\tThank you for accepting cookies\u003C\/h2\u003E\n\u003Cp\u003E\tYou can now hide this message or find out more about cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022hide-popup-button eu-cookie-compliance-hide-button\u0022\u003EMasquer\u003C\/button\u003E\n              \u003Cbutton type=\u0022button\u0022 class=\u0022find-more-button eu-cookie-compliance-more-button-thank-you\u0022 \u003EPlus d\u0027infos\u003C\/button\u003E\n          \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E","popup_use_bare_css":false,"popup_height":"auto","popup_width":"100%","popup_delay":1000,"popup_link":"http:\/\/www.cda95.fr\/fr\/content\/mentions-legales","popup_link_new_window":1,"popup_position":null,"fixed_top_position":1,"popup_language":"fr","store_consent":false,"better_support_for_screen_readers":0,"reload_page":0,"domain":"","domain_all_sites":null,"popup_eu_only_js":0,"cookie_lifetime":"100","cookie_session":false,"disagree_do_not_show_popup":0,"method":"default","whitelisted_cookies":"","withdraw_markup":"\u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-tab\u0022\u003EPrivacy settings\u003C\/button\u003E\n\u003Cdiv class=\u0022eu-cookie-withdraw-banner\u0022\u003E\n  \u003Cdiv class=\u0022popup-content info\u0022\u003E\n    \u003Cdiv id=\u0022popup-text\u0022\u003E\n      \u003Cp\u003E\tMerci d\u0027avoir install\u00e9 les cookies\u003C\/p\u003E\n\u003Cp\u003E\tVous pouvez cacher ce message ou en savoir plus sur les cookies.\u003C\/p\u003E\n    \u003C\/div\u003E\n    \u003Cdiv id=\u0022popup-buttons\u0022\u003E\n      \u003Cbutton type=\u0022button\u0022 class=\u0022eu-cookie-withdraw-button\u0022\u003EWithdraw consent\u003C\/button\u003E\n    \u003C\/div\u003E\n  \u003C\/div\u003E\n\u003C\/div\u003E\n","withdraw_enabled":false,"withdraw_button_on_info_popup":false,"cookie_categories":[],"enable_save_preferences_button":true,"fix_first_cookie_category":true,"select_all_categories_by_default":false},"extlink":{"extTarget":"_blank","extClass":0,"extLabel":"(link is external)","extImgClass":0,"extIconPlacement":"append","extSubdomains":1,"extExclude":"","extInclude":".*\\.pdf|.*\\.jpg","extCssExclude":"","extCssExplicit":"","extAlert":0,"extAlertText":"This link will take you to an external web site.","mailtoClass":"mailto","mailtoLabel":"(link sends e-mail)"},"googleanalytics":{"trackOutbound":1,"trackMailto":1,"trackDownload":1,"trackDownloadExtensions":"7z|aac|arc|arj|asf|asx|avi|bin|csv|doc(x|m)?|dot(x|m)?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt(x|m)?|pot(x|m)?|pps(x|m)?|ppam|sld(x|m)?|thmx|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls(x|m|b)?|xlt(x|m)|xlam|xml|z|zip","trackColorbox":1,"trackDomainMode":1},"beautytipStyles":{"default":{"fill":"#F1F1F1","strokeStyle":"#F1F1F1","shadow":"","cssStyles":{"color":"#000000"}},"plain":[],"netflix":{"positions":["right","left"],"fill":"#FFF","padding":5,"shadow":true,"shadowBlur":12,"strokeStyle":"#B9090B","spikeLength":50,"spikeGirth":60,"cornerRadius":10,"centerPointY":0.1,"overlap":-8,"cssStyles":{"fontSize":"12px","fontFamily":"arial,helvetica,sans-serif"}},"facebook":{"fill":"#F7F7F7","padding":8,"strokeStyle":"#B7B7B7","cornerRadius":0,"cssStyles":{"fontFamily":"\u0022lucida grande\u0022,tahoma,verdana,arial,sans-serif","fontSize":"11px"}},"transparent":{"fill":"rgba(0, 0, 0, .8)","padding":20,"strokeStyle":"#CC0","strokeWidth":3,"spikeLength":40,"spikeGirth":40,"cornerRadius":40,"cssStyles":{"color":"#FFF","fontWeight":"bold"}},"big-green":{"fill":"#00FF4E","padding":20,"strokeWidth":0,"spikeLength":40,"spikeGirth":40,"cornerRadius":15,"cssStyles":{"fontFamily":"\u0022lucida grande\u0022,tahoma,verdana,arial,sans-serif","fontSize":"14px"}},"google-maps":{"positions":["top","bottom"],"fill":"#FFF","padding":15,"strokeStyle":"#ABABAB","strokeWidth":1,"spikeLength":65,"spikeGirth":40,"cornerRadius":25,"centerPointX":0.9,"cssStyles":[]},"hulu":{"fill":"#F4F4F4","strokeStyle":"#666666","spikeLength":20,"spikeGirth":10,"width":350,"overlap":0,"centerPointY":1,"cornerRadius":0,"cssStyles":{"fontFamily":"\u0022Lucida Grande\u0022,Helvetica,Arial,Verdana,sans-serif","fontSize":"12px","padding":"10px 14px"},"shadow":true,"shadowColor":"rgba(0,0,0,.5)","shadowBlur":8,"shadowOffsetX":4,"shadowOffsetY":4}},"beautytips":{"calendar-tooltips":{"cssSelect":".calendar-calendar .mini-day-on a, .calendar-calendar .day a, .calendar-calendar .mini-day-on span, .calendar-calendar .day span","contentSelector":"$(this).next().html()","trigger":["mouseover","click"],"style":"plain","list":["contentSelector","trigger"]}},"omega":{"layouts":{"primary":"normal","order":["narrow","normal","wide"],"queries":{"narrow":"all and (min-width: 740px) and (min-device-width: 740px), (max-device-width: 800px) and (min-width: 740px) and (orientation:landscape)","normal":"all and (min-width: 980px) and (min-device-width: 980px), all and (max-device-width: 1024px) and (min-width: 1024px) and (orientation:landscape)","wide":"all and (min-width: 1220px)"}}}});
//--><!]]>
</script>
  <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body class="html front not-logged-in page-front i18n-fr">
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable">Aller au contenu principal</a>
  </div>
  <div class="region region-page-top" id="region-page-top">
  <div class="region-inner region-page-top-inner">
      </div>
</div>  <div class="page clearfix" id="page">
      <header id="section-header" class="section section-header">
  <div id="zone-user-wrapper" class="zone-wrapper zone-user-wrapper clearfix">  
  <div id="zone-user" class="zone zone-user clearfix container-12">
    <div class="grid-12 region region-user-first" id="region-user-first">
  <div class="region-inner region-user-first-inner">
    <div class="block block-search block-form block-search-form odd block-without-title" id="block-search-form">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <form action="/" method="post" id="search-block-form" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Formulaire de recherche</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--2">Rechercher </label>
 <input title="Indiquer les termes à rechercher" type="text" id="edit-search-block-form--2" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Rechercher" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-g56_aWZKhfZaKtcjniC8ujj_M4sKxFyuhZf6WfbrqsQ" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form>    </div>
  </div>
</div><div class="block block-locale block-language block-locale-language even block-without-title" id="block-locale-language">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="language-switcher-locale-url"><li class="en first"><a href="/en" class="language-link" xml:lang="en">English</a></li><li class="fr last active"><a href="/fr" class="language-link active" xml:lang="fr">Français</a></li></ul>    </div>
  </div>
</div><div class="block block-om-maximenu block-om-maximenu-1 block-om-maximenu-om-maximenu-1 odd block-without-title" id="block-om-maximenu-om-maximenu-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
       

  <div id="om-maximenu-menu-mobile" class="om-maximenu om-maximenu-no-style om-maximenu-block om-maximenu-column om-maximenu-block-down code-om-u122-1932768626">     
            

<div id="om-menu-menu-mobile-ul-wrapper" class="om-menu-ul-wrapper">
  <ul id="om-menu-menu-mobile" class="om-menu">
                  

   
  <li id="om-leaf-om-u122-1932768626-1" class="om-leaf first last leaf-menu-button om-leaf-icon">   
    <span class="om-link  link-menu-button" ></span>      
  <div class="om-maximenu-content om-maximenu-content-nofade closed">
    <div class="om-maximenu-top">
      <div class="om-maximenu-top-left"></div>
      <div class="om-maximenu-top-right"></div>
    </div><!-- /.om-maximenu-top --> 
    <div class="om-maximenu-middle">
      <div class="om-maximenu-middle-left">
        <div class="om-maximenu-middle-right">
           

<div class="block block-search block-search-id-form first">           
      <div class="content"><form action="/" method="post" id="search-block-form--2" accept-charset="UTF-8"><div><div class="container-inline">
      <h2 class="element-invisible">Formulaire de recherche</h2>
    <div class="form-item form-type-textfield form-item-search-block-form">
  <label class="element-invisible" for="edit-search-block-form--4">Rechercher </label>
 <input title="Indiquer les termes à rechercher" type="text" id="edit-search-block-form--4" name="search_block_form" value="" size="15" maxlength="128" class="form-text" />
</div>
<div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Rechercher" class="form-submit" /></div><input type="hidden" name="form_build_id" value="form-8eXprrT61mYG8HuDtU8nOCpBY4LYqzeAo6FgDQ8uR1Y" />
<input type="hidden" name="form_id" value="search_block_form" />
</div>
</div></form></div>
  </div><!-- /.block -->





 

<div class="block block-nice_menus block-nice_menus-id-4">           
      <div class="content"><ul class="nice-menu nice-menu-down nice-menu-menu-offre-du-cda" id="nice-menu-4"><li class="menu-4108 menu-path-taxonomy-term-283 first odd "><a href="/fr/offre-du-cda/librairie" title="Librairie">Librairie</a></li>
<li class="menu-3756 menu-path-taxonomy-term-178  even "><a href="/fr/menu-principal/spectacle" title="Spectacle">Spectacle</a></li>
<li class="menu-3757 menu-path-taxonomy-term-179  odd "><a href="/fr/menu-principal/cinema" title="Cinéma">Cinéma</a></li>
<li class="menu-3758 menu-path-taxonomy-term-180  even "><a href="/fr/menu-principal/concert" title="Concert">Concert</a></li>
<li class="menu-3759 menu-path-taxonomy-term-181  odd "><a href="/fr/menu-principal/expo" title="Expo">Expo</a></li>
<li class="menu-3760 menu-path-taxonomy-term-182  even "><a href="/fr/menu-principal/jeune-public" title="Jeune Public">Jeune Public</a></li>
<li class="menu-3762 menuparent  menu-path-taxonomy-term-184  odd last"><a href="/fr/menu-principal/le-cda" title="Le Cda">Le Cda</a><ul><li class="menu-4235 menu-path-cda95fr-fr-footer-contacts first odd "><a href="http://www.cda95.fr/fr/footer/contacts" title="">L&#039;équipe</a></li>
<li class="menu-3794 menu-path-taxonomy-term-203  even "><a href="/fr/offre-du-cda/presentation" title="Présentation">Présentation</a></li>
<li class="menu-3795 menu-path-taxonomy-term-204  odd "><a href="/fr/offre-du-cda/les-espaces" title="Les espaces">Les espaces</a></li>
<li class="menu-3809 menu-path-taxonomy-term-209  even "><a href="/fr/offre-du-cda/expertises-technologiques" title="Expertises technologiques">Expertises technologiques</a></li>
<li class="menu-3799 menu-path-taxonomy-term-208  odd "><a href="/fr/offre-du-cda/laction-artistique" title="L&#039;action artistique">L&#039;action artistique</a></li>
<li class="menu-3790 menu-path-taxonomy-term-201  even "><a href="/fr/offre-du-cda/cooperation-internationale" title="Coopération internationale">Coopération internationale</a></li>
<li class="menu-3797 menu-path-taxonomy-term-206  odd "><a href="/fr/offre-du-cda/bains-numeriques" title="Bains numériques">Bains numériques</a></li>
<li class="menu-3819 menu-path-taxonomy-term-220  even "><a href="/fr/offre-du-cda/numeric-lab" title="Numeric lab">Numeric lab</a></li>
<li class="menu-4249 menu-path-cda95fr-fr-content-partenaires  odd "><a href="http://www.cda95.fr/fr/content/partenaires" title="">Les partenaires</a></li>
<li class="menu-3761 menu-path-taxonomy-term-183  even "><a href="/fr/menu-principal/le-cafe" title="Le comptoir">Le bar / café</a></li>
<li class="menu-3798 menu-path-taxonomy-term-207  odd last"><a href="/fr/offre-du-cda/pole-residences" title="Soutien à la création">Pôle résidences</a></li>
</ul></li>
</ul>
</div>
  </div><!-- /.block -->





 

<div class="block block-nice_menus block-nice_menus-id-6">           
      <div class="content"></div>
  </div><!-- /.block -->





 

<div class="block block-block block-block-id-30">           
      <div class="content"><p style="text-align: center;margin-top: 5px;display: flex;justify-content: space-between;">
	<a href="https://twitter.com/centredesarts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/twitter-accueil.png" /></a> <a href="https://www.facebook.com/centredesarts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/facebook-accueil.png" /></a> <a href="/user/CentreDesArts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/youtube.png" /></a> <a href="https://www.instagram.com/centredesarts95/" target="_blank"><img src="/sites/centredesarts-v2/files/media/Numeric-Lab/instagramvetor1_0.png" /></a></p>
</div>
  </div><!-- /.block -->





 

<div class="block block-block block-block-id-1">           
      <div class="content"><div>
	<a href="/content/billetterie">Billetterie</a></div>
<div>
	<a href="/offre-du-cda/librairie">Boutique </a></div>
</div>
  </div><!-- /.block -->





 

<div class="block block-block block-block-id-20 last">           
      <div class="content"><div>
	<a href="/en/infos-pratiques/billetterie">Online ticketing</a></div>
<div>
	<a href="/en/editions">Shop </a></div>
</div>
  </div><!-- /.block -->





          <div class="om-clearfix"></div>
        </div><!-- /.om-maximenu-middle-right --> 
      </div><!-- /.om-maximenu-middle-left --> 
    </div><!-- /.om-maximenu-middle --> 
    <div class="om-maximenu-bottom">
      <div class="om-maximenu-bottom-left"></div>
      <div class="om-maximenu-bottom-right"></div>
    </div><!-- /.om-maximenu-bottom -->  
    <div class="om-maximenu-arrow"></div>
    <div class="om-maximenu-open">
      <input type="checkbox" value="" />
      Stay    </div><!-- /.om-maximenu-open -->  
  </div><!-- /.om-maximenu-content -->  
 

      
  </li>
  
    
  

  
          
      </ul><!-- /.om-menu -->    
</div><!-- /.om-menu-ul-wrapper -->   



      </div><!-- /#om-maximenu-[menu name] -->   


    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix container-12">
    <div class="grid-12 region region-branding" id="region-branding">
  <div class="region-inner region-branding-inner">
        <div class="branding-data clearfix">
                        <hgroup class="site-name-slogan">        
                                
        <h1 class="site-name element-invisible"><a href="/fr" title="Accueil" class="active">Centre des arts</a></h1>
                              </hgroup>
          </div>
        <div class="block block-nice-menus block-4 block-nice-menus-4 odd block-without-title" id="block-nice-menus-4">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="nice-menu nice-menu-down nice-menu-menu-offre-du-cda" id="nice-menu-4"><li class="menu-4108 menu-path-taxonomy-term-283 first odd "><a href="/fr/offre-du-cda/librairie" title="Librairie">Librairie</a></li>
<li class="menu-3756 menu-path-taxonomy-term-178  even "><a href="/fr/menu-principal/spectacle" title="Spectacle">Spectacle</a></li>
<li class="menu-3757 menu-path-taxonomy-term-179  odd "><a href="/fr/menu-principal/cinema" title="Cinéma">Cinéma</a></li>
<li class="menu-3758 menu-path-taxonomy-term-180  even "><a href="/fr/menu-principal/concert" title="Concert">Concert</a></li>
<li class="menu-3759 menu-path-taxonomy-term-181  odd "><a href="/fr/menu-principal/expo" title="Expo">Expo</a></li>
<li class="menu-3760 menu-path-taxonomy-term-182  even "><a href="/fr/menu-principal/jeune-public" title="Jeune Public">Jeune Public</a></li>
<li class="menu-3762 menuparent  menu-path-taxonomy-term-184  odd last"><a href="/fr/menu-principal/le-cda" title="Le Cda">Le Cda</a><ul><li class="menu-4235 menu-path-cda95fr-fr-footer-contacts first odd "><a href="http://www.cda95.fr/fr/footer/contacts" title="">L&#039;équipe</a></li>
<li class="menu-3794 menu-path-taxonomy-term-203  even "><a href="/fr/offre-du-cda/presentation" title="Présentation">Présentation</a></li>
<li class="menu-3795 menu-path-taxonomy-term-204  odd "><a href="/fr/offre-du-cda/les-espaces" title="Les espaces">Les espaces</a></li>
<li class="menu-3809 menu-path-taxonomy-term-209  even "><a href="/fr/offre-du-cda/expertises-technologiques" title="Expertises technologiques">Expertises technologiques</a></li>
<li class="menu-3799 menu-path-taxonomy-term-208  odd "><a href="/fr/offre-du-cda/laction-artistique" title="L&#039;action artistique">L&#039;action artistique</a></li>
<li class="menu-3790 menu-path-taxonomy-term-201  even "><a href="/fr/offre-du-cda/cooperation-internationale" title="Coopération internationale">Coopération internationale</a></li>
<li class="menu-3797 menu-path-taxonomy-term-206  odd "><a href="/fr/offre-du-cda/bains-numeriques" title="Bains numériques">Bains numériques</a></li>
<li class="menu-3819 menu-path-taxonomy-term-220  even "><a href="/fr/offre-du-cda/numeric-lab" title="Numeric lab">Numeric lab</a></li>
<li class="menu-4249 menu-path-cda95fr-fr-content-partenaires  odd "><a href="http://www.cda95.fr/fr/content/partenaires" title="">Les partenaires</a></li>
<li class="menu-3761 menu-path-taxonomy-term-183  even "><a href="/fr/menu-principal/le-cafe" title="Le comptoir">Le bar / café</a></li>
<li class="menu-3798 menu-path-taxonomy-term-207  odd last"><a href="/fr/offre-du-cda/pole-residences" title="Soutien à la création">Pôle résidences</a></li>
</ul></li>
</ul>
    </div>
  </div>
</div><div class="block block-block block-19 block-block-19 even block-without-title" id="block-block-19">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>
	<a href="/"><img alt="Logo Centre des arts" src="/sites/centredesarts-v2/files/media/logo-cda-mobile-new.jpg" style="width: 400px; height: 153px;" /></a></p>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div><div id="zone-menu-wrapper" class="zone-wrapper zone-menu-wrapper clearfix">  
  <div id="zone-menu" class="zone zone-menu clearfix container-12">
    <div class="grid-12 region region-menu" id="region-menu">
  <div class="region-inner region-menu-inner">
        <div class="block block-nice-menus block-6 block-nice-menus-6 odd block-without-title" id="block-nice-menus-6">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
          </div>
  </div>
</div>  </div>
</div>
  </div>
</div></header>    
      <section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix container-12">    
        
        <aside class="grid-3 region region-sidebar-first" id="region-sidebar-first">
  <div class="region-inner region-sidebar-first-inner">
    <div class="block block-block block-12 block-block-12 odd block-without-title" id="block-block-12">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p>
	<a href="http://www.cda95.fr"><img alt="Logo Centre des arts" src="/sites/centredesarts-v2/files/media/logo_cda_site.png" style="width: 360px; height: 600px;" /></a></p>
    </div>
  </div>
</div><div class="block block-block block-1 block-block-1 even block-without-title" id="block-block-1">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div>
	<a href="/content/billetterie">Billetterie</a></div>
<div>
	<a href="/offre-du-cda/librairie">Boutique </a></div>
    </div>
  </div>
</div><div class="block block-block block-30 block-block-30 odd block-without-title" id="block-block-30">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <p style="text-align: center;margin-top: 5px;display: flex;justify-content: space-between;">
	<a href="https://twitter.com/centredesarts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/twitter-accueil.png" /></a> <a href="https://www.facebook.com/centredesarts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/facebook-accueil.png" /></a> <a href="/user/CentreDesArts95" target="_blank"><img src="/sites/centredesarts-v2/themes/omega3/images/youtube.png" /></a> <a href="https://www.instagram.com/centredesarts95/" target="_blank"><img src="/sites/centredesarts-v2/files/media/Numeric-Lab/instagramvetor1_0.png" /></a></p>
    </div>
  </div>
</div><div class="block block-block block-2 block-block-2 even block-without-title" id="block-block-2">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div style="background-color: rgb(140, 140, 140);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<a href="http://www.cda95.fr/fr/content/inscriptions-newsletters"><strong><span style="color:#ffffff;">INSCRIPTION NEWSLETTERS</span></strong></a></p>
<p>	</p></center>
</div>
<p>
	 </p>
<div style="background-color: rgb(140, 140, 140);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<strong><a href="http://www.ville-enghienlesbains.fr/"><span style="color:#ffffff;">ENGHIEN-LES-BAINS</span></a></strong></p>
<p>	</p></center>
</div>
<p>
	 </p>
<div style="background-color: rgb(227, 6, 19);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<strong><a href="https://www.parisimages-digitalsummit.com/"><span style="color:#ffffff;">PARIS IMAGES DIGITAL SUMMIT</span></a></strong></p>
<p>	</p></center>
</div>
<p>
	 </p>
<div style="background-color: rgb(227, 6, 19);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<a href="http://www.cda95.fr/fr/bains-numeriques-10e-edition"><span style="color:#FFFFFF;"><b>BAINS NUMÉRIQUES</b></span></a></p>
<p>	</p></center>
</div>
<p>
	 </p>
<div style="background-color: rgb(0, 0, 0);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<strong><a href="http://www.cda95.fr/fr/offre-du-cda/numeric-lab"><span style="color:#ffffff;">NUMERIC LAB</span></a></strong></p>
<p>	</p></center>
</div>
<p>
	 </p>
<div style="background-color: rgb(0, 0, 0);" valign="middle">
	<center>
<p font-size:="1.4em" padding:="5px">
			<strong><a href="https://www.cda95.fr/fr/offre-du-cda/pole-residences"><span style="color:#FFFFFF;">P</span></a></strong><a href="https://www.cda95.fr/fr/offre-du-cda/pole-residences"><strong><span style="color:#ffffff;">ÔLE RÉSIDENCES</span></strong></a></p>
<p>	</p></center>
</div>
    </div>
  </div>
</div>  </div>
</aside><div class="grid-9 region region-content" id="region-content">
  <div class="region-inner region-content-inner">
    <a id="main-content"></a>
                        <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <div class="panel-flexible panels-flexible-9 clearfix" >
<div class="panel-flexible-inside panels-flexible-9-inside">
<div class="panels-flexible-row panels-flexible-row-9-1 panels-flexible-row-first clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-1-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-9-centre panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-centre-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-1"   class="panel-pane pane-custom pane-1">
  
      
  
  <div class="pane-content">
    <h2 class="rtecenter">
	 </h2>
<h1 class="rtecenter">
	<span style="color:#FF0000;"><b>HORAIRES EXCEPTIONNELS</b></span></h1>
<h2 class="rtecenter">
	<b>Le CDA sera fermé le 10/12/19 exceptionnellement entre 13h et 14h.</b></h2>
<p>
	 </p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-views pane-carrousel"   class="panel-pane pane-views pane-carrousel">
  
      
  
  <div class="pane-content">
    <div class="view view-carrousel view-id-carrousel view-display-id-block_1 view-dom-id-c4e14b0f10e2cf62d54e6e2ac9c444f2">
        
  
  
      <div class="view-content">
      
  <div class="skin-default">
    
    <div id="views_slideshow_cycle_main_carrousel-block_1_1" class="views_slideshow_cycle_main views_slideshow_main"><div id="views_slideshow_cycle_teaser_section_carrousel-block_1_1" class="views-slideshow-cycle-main-frame views_slideshow_cycle_teaser_section">
  <div id="views_slideshow_cycle_div_carrousel-block_1_1_0" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-1 views-row-first views-row-odd" aria-labelledby='views_slideshow_pager_field_item_bottom_carrousel-block_1_1_0'>
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd views-row-first">
    
  <div class="views-field views-field-field-affiche">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cda95.fr/sites/centredesarts-v2/files/styles/carrousel/public/aap.jpg?itok=A1gIPKW_" width="1200" height="550" alt="" /></div>  </div>  
  <div class="views-field views-field-field-plus-d-infos">        <div class="field-content"></div>  </div>  
  <div class="views-field views-field-field-date-texte">        <div class="field-content"></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">APPEL À PROJETS</span>  </div>  
  <div class="views-field views-field-field-sous-titre">        <div class="field-content">NUMERIC LAB - INCUBATEUR</div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_carrousel-block_1_1_1" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-2 views_slideshow_cycle_hidden views-row-even" aria-labelledby='views_slideshow_pager_field_item_bottom_carrousel-block_1_1_1'>
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
  <div class="views-field views-field-field-affiche">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cda95.fr/sites/centredesarts-v2/files/styles/carrousel/public/prog-19-2010.jpg?itok=FUikhtVd" width="1200" height="550" alt="" /></div>  </div>  
  <div class="views-field views-field-field-plus-d-infos">        <div class="field-content"></div>  </div>  
  <div class="views-field views-field-field-date-texte">        <div class="field-content">MAR 17 DÉC · 20H30</div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">ANGELIN PRELJOCAJ</span>  </div>  
  <div class="views-field views-field-field-sous-titre">        <div class="field-content">DANSE</div>  </div></div>
</div>
<div id="views_slideshow_cycle_div_carrousel-block_1_1_2" class="views-slideshow-cycle-main-frame-row views_slideshow_cycle_slide views_slideshow_slide views-row-3 views_slideshow_cycle_hidden views-row-last views-row-odd" aria-labelledby='views_slideshow_pager_field_item_bottom_carrousel-block_1_1_2'>
  <div class="views-slideshow-cycle-main-frame-row-item views-row views-row-0 views-row-odd">
    
  <div class="views-field views-field-field-affiche">        <div class="field-content"><img typeof="foaf:Image" src="https://www.cda95.fr/sites/centredesarts-v2/files/styles/carrousel/public/expo_jlp.jpg?itok=t6bFJ7GE" width="1200" height="550" alt="" /></div>  </div>  
  <div class="views-field views-field-field-plus-d-infos">        <div class="field-content"></div>  </div>  
  <div class="views-field views-field-field-date-texte">        <div class="field-content">20.09 &gt; 27.12</div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content">Julio Le Parc · Réels &amp; Virtuels 1958-2019</span>  </div>  
  <div class="views-field views-field-field-sous-titre">        <div class="field-content">Exposition · Entrée libre</div>  </div></div>
</div>
</div>
</div>
          <div class="views-slideshow-controls-bottom clearfix">
        <div id="widget_pager_bottom_carrousel-block_1_1" class="views-slideshow-pager-fields widget_pager widget_pager_bottom views_slideshow_pager_field">
  <div id="views_slideshow_pager_field_item_bottom_carrousel-block_1_1_0" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd views-row-first" aria-controls="views_slideshow_cycle_div_carrousel-block_1_1_0">
  </div>
<div id="views_slideshow_pager_field_item_bottom_carrousel-block_1_1_1" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-even" aria-controls="views_slideshow_cycle_div_carrousel-block_1_1_1">
  </div>
<div id="views_slideshow_pager_field_item_bottom_carrousel-block_1_1_2" class="views-slideshow-pager-field-item views_slideshow_pager_field_item views-row-odd views-row-last" aria-controls="views_slideshow_cycle_div_carrousel-block_1_1_2">
  </div>
</div>
      </div>
      </div>
    </div>
  
  
  
  
  
  
</div>  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-9-2 clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-2-inside clearfix">
<div class="panels-flexible-column panels-flexible-column-9-3 panels-flexible-column-first ">
  <div class="inside panels-flexible-column-inside panels-flexible-column-9-3-inside panels-flexible-column-inside-first">
<div class="panels-flexible-row panels-flexible-row-9-5 panels-flexible-row-first clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-5-inside panels-flexible-row-inside-first clearfix">
<div class="panels-flexible-region panels-flexible-region-9-twitter panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-twitter-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-8"   class="panel-pane pane-custom pane-8">
  
      
  
  <div class="pane-content">
    <h2>
	<span style="color:#FF0000;"><strong><span style="background-color:#FFFFFF;">PROCHAINS SPECTACLES</span></strong></span></h2>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-18"   class="panel-pane pane-block pane-block-18">
  
      
  
  <div class="pane-content">
    <p>
	<a href="/content/music-corner-9"><img alt="Image carrée" class="accueil" src="/sites/centredesarts-v2/files/media/soulreaktive.jpg" style="width: 150px; height: 150px;" /></a></p>
<p>
	<strong>JEU 12 DÉCEMBRE</strong><br /><a href="/content/music-corner-9"><b>Music Corner #9</b></a></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-13"   class="panel-pane pane-block pane-block-13">
  
      
  
  <div class="pane-content">
    <p>
	<a href="/content/ghost-still-life-complet"><img alt="" src="/sites/centredesarts-v2/files/media/preljo.jpg" style="width: 150px; height: 150px;" /></a></p>
<p>
	<span style="font-size:12px;"><strong>MAR 17 DÉCEMBRE</strong></span><br /><span style="font-size: 12px;"><b><a href="/content/ghost-still-life-complet">B</a></b></span><a href="/content/ghost-still-life-complet"><span style="font-size: 12px;"><b>allet Preljocaj</b></span></a></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-16"   class="panel-pane pane-block pane-block-16">
  
      
  
  <div class="pane-content">
    <p>
	<a href="https://www.cda95.fr/fr/content/music-corner-10"><img alt="" src="/sites/centredesarts-v2/files/media/hanami.jpg" style="width: 150px; height: 150px;" /></a></p>
<p>
	<strong><span style="font-size:12px;">JEU 09 JANVIER<br /><a href="https://www.cda95.fr/fr/content/music-corner-10">Music Corner #10 · Hanami</a></span></strong></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-custom pane-9"   class="panel-pane pane-custom pane-9">
  
      
  
  <div class="pane-content">
    <h2>
	<span style="color:#FF0000;"><b>ACTUALITES</b></span></h2>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-17"   class="panel-pane pane-block pane-block-17">
  
      
  
  <div class="pane-content">
    <p>
	<a href="/content/visites-guidees-julio-le-parc-reels-virtuels-1958-2019"><img alt="" src="/sites/centredesarts-v2/files/media/julio_1_2.jpg" style="width: 150px; height: 150px;" /></a></p>
<p>
	<strong>MER 18 DÉCEMBRE<br /><a href="/content/chiquenaudes-romance-en-stuc">V</a><a href="/content/visites-guidees-julio-le-parc-reels-virtuels-1958-2019">isite guidée</a></strong></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-14"   class="panel-pane pane-block pane-block-14">
  
      
  
  <div class="pane-content">
    <p>
	<a href="/offre-du-cda/numeric-lab"><img alt="" src="/sites/centredesarts-v2/files/media/appel_a_candidatures_-_numeric_lab_0.jpg" style="width: 150px; height: 150px;" /></a></p>
<p>
	<b>NUMERIC LAB</b><br /><a href="/offre-du-cda/numeric-lab"><b>Appel à projets</b></a></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-block pane-block-15"   class="panel-pane pane-block pane-block-15">
  
      
  
  <div class="pane-content">
    <p>
	<a href="/offre-du-cda/numeric-lab"><img alt="" src="/sites/centredesarts-v2/files/media/numeric_lab_2.png" style="width: 150px; height: 150px;" /></a></p>
<p>
	<strong>NUMERIC LAB<br /><a href="/node/3693">I</a><a href="/offre-du-cda/numeric-lab">ncubateur</a></strong></p>
  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-9-9 panels-flexible-row-last clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-9-inside panels-flexible-row-inside-last clearfix">
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-column panels-flexible-column-9-4 panels-flexible-column-last ">
  <div class="inside panels-flexible-column-inside panels-flexible-column-9-4-inside panels-flexible-column-inside-last">
<div class="panels-flexible-region panels-flexible-region-9-infos_pratiques panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-infos_pratiques-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-4"   class="panel-pane pane-custom pane-4">
  
      
  
  <div class="pane-content">
    <h2>
	<span style="color:#FF0000;"><strong>Vidéo</strong></span></h2>
<p><iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="180" src="https://www.youtube.com/embed/JBhvdc2BhXU" width="280"></iframe></p>
  </div>

  
  </div>
<div class="panel-separator"></div><div class="panel-pane pane-custom pane-5"   class="panel-pane pane-custom pane-5">
  
      
  
  <div class="pane-content">
    <h2>
	<span style="color:#FF0000;"><strong>Podcast</strong></span></h2>
<p>
	<span style="color:#000000;"><strong>Rencontre du Numeric Lab #6 - Investir sur les réseaux sociaux, une bonne stratégie dans l'industrie musicale ?</strong></span></p>
<p>
	<a href="https://soundcloud.com/user-604163880"><img alt="" src="/sites/centredesarts-v2/files/media/podcast.png" style="width: 300px; height: 102px;" /></a></p>
<p class="rtecenter">
	<a href="https://soundcloud.com/user-604163880"><strong>écouter &gt;</strong></a></p>
  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-9-12 clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-12-inside clearfix">
<div class="panels-flexible-region panels-flexible-region-9-r__gion_1 panels-flexible-region-first ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-r__gion_1-inside panels-flexible-region-inside-first">
<div class="panel-pane pane-custom pane-6"   class="panel-pane pane-custom pane-6">
  
      
  
  <div class="pane-content">
    <h3 class="rtecenter">
	<span style="color:#FF0000;"><strong>Programme culturel bimestriel</strong></span></h3>
<p>
	 </p>
<p class="rtecenter">
	<a href="https://fr.calameo.com/read/001903748914770b1ef4a"><img alt="" src="/sites/centredesarts-v2/files/media/cda_bimestriel_nov-dec19_couv.jpg" style="width: 163px; height: 230px;" /></a></p>
  </div>

  
  </div>
  </div>
</div>
<div class="panels-flexible-region panels-flexible-region-9-infos_ ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-infos_-inside">
<div class="panel-pane pane-custom pane-3"   class="panel-pane pane-custom pane-3">
  
      
  
  <div class="pane-content">
    <h3 class="rtecenter">
	<span style="color:#FF0000;"><strong>Programme cinéma mensuel</strong></span></h3>
<p>
	 </p>
<div style="text-align:center;">
	<a href="https://fr.calameo.com/read/000099622f94dee596007"><img alt="" src="/sites/centredesarts-v2/files/media/cda_mensuelcine_dec19_hd2-2dh.jpg" style="width: 159px; height: 230px;" /></a></div>
  </div>

  
  </div>
  </div>
</div>
<div class="panels-flexible-region panels-flexible-region-9-r__gion_3 panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-r__gion_3-inside panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-7"   class="panel-pane pane-custom pane-7">
  
      
  
  <div class="pane-content">
    <h3 class="rtecenter">
	<span style="color:#FF0000;"><strong>Catalogue - Julio Le Parc {Extrait}</strong></span></h3>
<div style="text-align:center;">
	 </div>
<div style="text-align:center;">
	<a href="https://www.cda95.fr/fr/content/julio-le-parc-du-reel-au-virtuel-1958-2019"><img alt="" src="/sites/centredesarts-v2/files/media/couvjlp5_1.png" style="width: 160px; height: 189px;" /></a></div>
<div style="text-align:center;">
	 </div>
<div style="text-align:center;">
	 </div>
<div style="text-align:center;">
	 </div>
  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
<div class="panels-flexible-row panels-flexible-row-9-13 panels-flexible-row-last clearfix ">
  <div class="inside panels-flexible-row-inside panels-flexible-row-9-13-inside panels-flexible-row-inside-last clearfix">
<div class="panels-flexible-region panels-flexible-region-9-infos panels-flexible-region-first panels-flexible-region-last ">
  <div class="inside panels-flexible-region-inside panels-flexible-region-9-infos-inside panels-flexible-region-inside-first panels-flexible-region-inside-last">
<div class="panel-pane pane-custom pane-2"   class="panel-pane pane-custom pane-2">
  
      
  
  <div class="pane-content">
    <h4 class="rtecenter">
	<strong><a href="http://www.cda95.fr/fr/node/1860"><span style="color:#FF0000;">Horaires  </span></a><span style="color:#FF0000;">|   </span><a href="http://www.cda95.fr/fr/infos-pratiques/acces"><span style="color:#FF0000;">Accès</span></a><span style="color:#FF0000;">  |  </span><a href="http://www.cda95.fr/fr/infos-pratiques/tarifs"><span style="color:#FF0000;">Tarifs</span></a><span style="color:#FF0000;">  |  </span><a href="http://www.cda95.fr/fr/infos-pratiques/contacts"><span style="color:#FF0000;">Contacts</span></a></strong></h4>
  </div>

  
  </div>
  </div>
</div>
  </div>
</div>
</div>
</div>
    </div>
  </div>
</div>      </div>
</div>  </div>
</div></section>    
  
      <footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix container-12">
    <div class="grid-12 region region-footer-first" id="region-footer-first">
  <div class="region-inner region-footer-first-inner">
    <div class="block block-nice-menus block-5 block-nice-menus-5 odd block-without-title" id="block-nice-menus-5">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="nice-menu nice-menu-down nice-menu-menu-footer" id="nice-menu-5"><li class="menu-3772 menu-path-taxonomy-term-186 first odd "><a href="/fr/footer/contacts" title="Contacts">Contacts</a></li>
<li class="menu-3792 menu-path-cda95fr-fr-bains-numeriques-10e-edition  even "><a href="http://www.cda95.fr/fr/bains-numeriques-10e-edition" title="">Festival Bains numériques</a></li>
<li class="menu-3800 menu-path-espace-presse  odd "><a href="/fr/espace-presse" title="">Espace presse</a></li>
<li class="menu-3801 menu-path-espace-pro  even "><a href="/fr/espace-pro" title="">Espace pro</a></li>
<li class="menu-3802 menu-path-archives  odd "><a href="/fr/archives" title="">Archives</a></li>
<li class="menu-3817 menu-path-galerie  even "><a href="/fr/galerie" title="">Photos &amp; Vidéos</a></li>
<li class="menu-3808 menu-path-plan-du-site  odd "><a href="/fr/plan-du-site" title="">Plan du site</a></li>
<li class="menu-3859 menu-path-cda95fr-fr-content-les-partenaires-du-centre-des-arts  even "><a href="http://www.cda95.fr/fr/content/les-partenaires-du-centre-des-arts" title="">Partenaires</a></li>
<li class="menu-3776 menu-path-user  odd "><a href="/fr/user" title="">S&#039;identifier</a></li>
<li class="menu-4090 menu-path-node-224  even "><a href="/fr/content/mentions-legales" title="">Mentions légales</a></li>
<li class="menu-4361 menu-path-node-3377  odd last"><a href="/fr/content/protection-des-donnees-personnelles" title="">Données personnelles</a></li>
</ul>
    </div>
  </div>
</div>  </div>
</div>  </div>
</div></footer>  </div>  <div class="region region-page-bottom" id="region-page-bottom">
  <div class="region-inner region-page-bottom-inner">
    <div><a rel="nofollow" href="http://www.solidhostdesign.com/shd.php?month=0044"><!-- randomness --></a></div>  </div>
</div><script type="text/javascript">
<!--//--><![CDATA[//><!--
ctSetCookie("ct_check_js", "960681ff721ebd01ab3d57b5bc185df2", "0");
//--><!]]>
</script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var eu_cookie_compliance_cookie_name = "";
//--><!]]>
</script>
<script type="text/javascript" src="https://www.cda95.fr/sites/centredesarts-v2/files/js/js_Llgek5Zasqh0wiimoKH-uIdmSIEO0i9Cbi7UdXEdRgw.js"></script>
</body>
</html>