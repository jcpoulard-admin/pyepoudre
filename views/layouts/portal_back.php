<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\PortalAsset;
use yii\helpers\Url;
use app\modules\cms\models\Menu;

PortalAsset::register($this);

$all_main_menu = Menu::findBySql("SELECT m.* FROM menus m  INNER JOIN layouts l ON (l.id = m.layouts_id) WHERE parent_id is NULL AND publish = 1 AND l.slug <> 'menu-lateral' ORDER BY absolute_position ASC")->all();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">

 <a class="navbar-brand" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index?pos=home"><?= ""; ?> <img src="<?= Url::to("@web/portal_assets/images/pyepoudre.png") ?>" alt="homepage" class="light-logo" /></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
             <li class="nav-item">
                <a class="nav-link <?php if(isset($_GET['pos']) && $_GET['pos'] == 'home' ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index?pos=home"><?= Yii::t('app','Home'); ?></a>
            </li> 
            <li class="nav-item">
                <a class="nav-link <?php if(isset($_GET['pos']) && $_GET['pos'] == 'prog' ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/list-event?pos=prog"><?= Yii::t('app','Programmation'); ?></a>
            </li>
           <?php
           foreach($all_main_menu as $amm){
               if($amm->is_parent !== 1){
               ?>
            
          <li class="nav-item">
            <a class="nav-link <?php if(isset($_GET['slug']) && $_GET['slug'] == $amm->slug ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $amm->slug; ?>"><?= $amm->name; ?></a>
          </li>
            <?php
               }else{
                   $all_sub_menu = Menu::findBySql("SELECT * FROM menus WHERE publish = 1 AND parent_id = $amm->id ORDER BY relative_position ASC ")->all();
                      if(!empty($all_sub_menu)){
                   ?>
                   <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle <?php if(isset($_GET['upm']) && $_GET['upm'] == $amm->slug ) echo "active"; ?>" href="" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?= $amm->name; ?>
                    </a>
                    <?php


                    ?>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                      <?php
                        foreach($all_sub_menu as $asm){
                      ?>
                        <a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $asm->slug; ?>&upm=<?= $amm->slug ?>"><?= $asm->name; ?></a>
                     <?php
                        }

                      ?>

                    </div>

                </li>

          <?php
               }
               }
           }
           ?>
          
        </ul>
      </div>
    </div>
  </nav>

<div class="jeneral">


            <?= $content ?>

</div>


 <footer class="py-5 bg-dark-bottom">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Pyepoudre <?= date('Y'); ?></p>
    </div>
    <!-- /.container -->
  </footer>

        <?php //Alert::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
