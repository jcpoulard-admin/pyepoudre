<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Url;
use yii\helpers\Html;
//use Yii; 

?>
<div class="card">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> 
            <a class="nav-link <?php if(isset($_GET['wh']) && $_GET['wh']=='useon') echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/viewonlineusers?wh=useon" role="tab">
                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">
                    <i class="mdi mdi-account-network"></i>
                    <?= Yii::t('app','User(s) online'); ?>
                </span>
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link <?php if(isset($_GET['wh']) && $_GET['wh']=='use') echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/index?wh=use" role="tab">
                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">
                    <i class="mdi mdi-account-multiple"></i>
                    Utilisateurs
                </span>
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link <?php if(isset($_GET['wh']) && $_GET['wh']=='per') echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/permission/index?wh=per" role="tab">
                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">
                    <i class="mdi mdi-key"></i>
                    Droit
                </span>
            </a> 
        </li>
        <li class="nav-item"> 
            <a class="nav-link <?php if(isset($_GET['wh']) && $_GET['wh']=='rol') echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/role/index?wh=rol" role="tab">
                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">
                    <i class="mdi mdi-lock"></i>
                    Rôle
                </span>
            </a> 
        </li>
     </ul>
</div>

<!--
<div class="card">
    <ul class="nav nav-tabs">
   <?php 
    if( Yii::$app->user->can("superadmin"))
       {
  ?>
   <li class="nav-item <?php if(isset($_GET['wh']) && $_GET['wh']=='useon') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-globe"></span> <span >'.Yii::t('app','User(s) online').'</span>', ['user/viewonlineusers','wh'=>'useon'],['title'=>Yii::t('app','User(s) online')]) ?></li>
      <?php } ?>
      
   <li class="nav-item <?php if(isset($_GET['wh']) && $_GET['wh']=='usel') echo "active"; ?>"><?= Html::a('<i class="fa fa-user"></i><span class="fa fa-list"></span><span >'.Yii::t('app','Username list').'</span>', ['user/usernamelist','wh'=>'usel'],['title'=>Yii::t('app','Username list')]) ?></li>
 
 
  <?php 
    if(Yii::$app->user->can("superadmin")){
  ?>
  <li class="nav-item <?php if(isset($_GET['wh']) && $_GET['wh']=='per') echo "active"; ?>"><?= Html::a('<i class="fa fa-ban"></i><span>'.Yii::t('app','Permission').'</span>', ['permission/index','wh'=>'per'],['title'=>Yii::t('app','Permission')]) ?>    

  </li>
      <?php } ?>
 
  
</ul>

</div>
-->