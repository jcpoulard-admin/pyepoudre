<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\PortalAsset;
use yii\helpers\Url;
use app\modules\cms\models\Menu;
use app\modules\cms\models\Image; 

$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1"; 
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();

PortalAsset::register($this);

$all_main_menu = Menu::findBySql("SELECT m.* FROM menus m  INNER JOIN layouts l ON (l.id = m.layouts_id) WHERE parent_id is NULL AND publish = 1 AND l.slug <> 'menu-lateral' ORDER BY absolute_position ASC")->all();

$sql_last_album = "SELECT * FROM  images WHERE component_id = (SELECT id FROM components WHERE is_publish = 1 AND type = 'album' ORDER BY id LIMIT 0,1) LIMIT 0,6";

$last_six_images = Image::findBySql($sql_last_album)->all();

$les_partenaires = Image::findBySql("SELECT * FROM  images WHERE component_id = (SELECT id FROM components WHERE name = 'Logo') AND is_publish = 1")->all(); 

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">

 <a class="navbar-brand" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index?pos=home"><?= ""; ?> <img src="<?= Url::to("@web/portal_assets/images/pyepoudre.png") ?>" alt="homepage" class="light-logo" /></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
             <li class="nav-item">
                <a class="nav-link <?php if(isset($_GET['pos']) && $_GET['pos'] == 'home' ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index?pos=home"><?= Yii::t('app','Home'); ?></a>
            </li> 
            <li class="nav-item">
                <a class="nav-link <?php if(isset($_GET['pos']) && $_GET['pos'] == 'prog' ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/list-event?pos=prog"><?= Yii::t('app','Programme'); ?></a>
            </li>
           <?php
           foreach($all_main_menu as $amm){
               if($amm->is_parent !== 1){
               ?>
            
          <li class="nav-item">
            <a class="nav-link <?php if(isset($_GET['slug']) && $_GET['slug'] == $amm->slug ) echo "active"; ?>" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $amm->slug; ?>&idm=<?= $amm->id?>"><?= $amm->name; ?></a>
          </li>
            <?php
               }else{
                   $all_sub_menu = Menu::findBySql("SELECT * FROM menus WHERE publish = 1 AND parent_id = $amm->id ORDER BY relative_position ASC ")->all();
                      if(!empty($all_sub_menu)){
                   ?>
                   <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle <?php if(isset($_GET['upm']) && $_GET['upm'] == $amm->slug ) echo "active"; ?>" href="" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <?= $amm->name; ?>
                    </a>
                    <?php


                    ?>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
                      <?php
                        foreach($all_sub_menu as $asm){
                      ?>
                        <a class="dropdown-item" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $asm->slug; ?>&upm=<?= $amm->slug ?>"><?= $asm->name; ?></a>
                     <?php
                        }

                      ?>

                    </div>

                </li>

          <?php
               }
               }
           }
           ?>
          
        </ul>
      </div>
    </div>
  </nav>

<div class="jeneral">


            <?= $content ?>

</div>

<!-- Construire un pied de page --> 
 
    
       <!-- Debut du nouveau pied de page --> 
            <section class="footer1 cid-r7p6WoyuC0" id="footer1-a">

    

    

    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-12 col-sm-12">


                <h3 class="mbr-text mbr-bold mbr-fonts-style group-title display-7"><?= Yii::t('app','Quick Link') ?></h3>
                <div class="message-item" style="align-content: center">
                      <ul class="list-unstyled">
                          <li>
                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                <?= Yii::t('app','Libray Catalog')?></a>
                          </li>
                          <li>
                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library"">
                                  <?= Yii::t('app','Bulletin'); ?>
                              </a>
                          </li>
                          <li>
                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie">
                                <?= Yii::t('app','Gallery Photos'); ?>
                              </a>
                          </li>
                          <?php 
                            foreach($menu_lateral as $ml){
                                ?>
                          <li>
                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $ml->slug; ?>"><?= $ml->name; ?></a>
                          </li>
                          <?php 
                            }
                          ?>
                          <li>
                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/contact">
                                    <?= Yii::t('app','Contact Us'); ?>
                                </a>
                          </li>
                          
                          <!--
                          <li>
                              <a href="#">
                                  <?= Yii::t('app','Partners'); ?>
                              </a>
                          </li>
                          -->
                          

                      </ul>
                 </div>

               
             </div>


            <div class="col-lg-4 col-md-12 col-sm-12">

                <h3 class="mbr-text mbr-bold mbr-fonts-style group-title display-7"><?= Yii::t('app','Contacts'); ?></h3>

                <div class="mbr-row items mbr-white">
                    



                    
                    

                <div class="list-item mbr-col-lg-12 mbr-col-md-12 mbr-col-sm-12">
                        <span class="mbr-iconfont listico mbri-tablet-vertical" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                        <h5 class="mbr-fonts-style text2 display-4">T&eacute;l&eacute;phone: +509 28 13 18 12</h5>
                    </div><div class="list-item mbr-col-lg-12 mbr-col-md-12 mbr-col-sm-12">
                        <span class="mbr-iconfont listico mbri-pin" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                        <h5 class="mbr-fonts-style text2 display-4">312, Bourdon, Port-au-Prince, Haiti HT 6111 </h5>
                    </div><div class="list-item mbr-col-lg-12 mbr-col-md-12 mbr-col-sm-12">
                        <span class="mbr-iconfont listico mbri-clock" style="color: rgb(255, 255, 255); fill: rgb(255, 255, 255);"></span>
                        <h5 class="mbr-fonts-style text2 display-4">
                            Mardi-Vendredi : 9h00 AM - 4h00 PM <br/><br/> Samedi : 9h00 AM - 2h00 PM 
                        </h5>
                    </div></div>



                <div class="social-list py-4">
                    <div class="soc-item">
                        <a href="https://twitter.com/312pyepoudre" target="_blank">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://www.facebook.com/312pyepoudre" target="_blank">
                            <i class="fab fa-facebook fa-2x"></i>
                         </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://www.youtube.com/channel/UC4u6XGQcrpibrQ2idtLOaWA" target="_blank">
                            <i class="fab fa-youtube fa-2x"></i>
                        </a>
                    </div>
                    <div class="soc-item">
                        <a href="https://instagram.com/312pyepoudre" target="_blank">
                            <i class="fab fa-instagram fa-2x"></i>
                         </a>
                    </div>
                    
                    
                </div>


            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 img-list">
                <h3 class="mbr-text mbr-bold mbr-fonts-style group-title display-7"><?= Yii::t('app','Nos partenaires'); ?></h3>
               
                <?php
                    foreach($les_partenaires as $lp){
                        ?>
                    <a href="<?= $lp->url; ?>" target="_new">
                        <img class="tips" src="<?= $lp->name; ?>" alt="<?= $lp->title; ?>" title="<?= $lp->title; ?>">
                    </a>
                        
                <?php 
                    }
                ?>
                
            </div>
        </div>
    </div>
</section>

<section class="footer2 cid-r7p6VgYzWi" id="footer2-9">
 <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <p class="mbr-text links mbr-fonts-style display-4">
                   Copyright &copy; Pyepoudre <?= date('Y'); ?>
                </p>
            </div>
        </div>
    </div>
</section>

       <!-- Fin de la page --> 
      <p class="m-0 text-center text-white">
         <!--  Copyright &copy; Pyepoudre <?= date('Y'); ?> -->
      </p>
      
   
    <!-- /.container -->
 



        <?php //Alert::widget() ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

