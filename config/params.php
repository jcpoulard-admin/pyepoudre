<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'supportEmail'=>'info@logipam.com',
    'user.passwordResetTokenExpire'=>3600,
];
