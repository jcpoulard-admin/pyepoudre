<?php

/**

 * This is the configuration for generating message translations

 * for the Yii framework. It is used by the 'yiic message' command.

 */

return array(

	//'sourcePath'=>'/', // eg. '/projects/myproject/protected'

	'messagePath'=>'messages', // '/projects/myproject/protected/messages'

	'languages'=>array('en','fr'), // according to your translation needs

	'fileTypes'=>array('php'),

	'overwrite'=>true,

	'removeOld'=>true,

	'sort'=>true,

	'exclude'=>array(

		'.svn',

		'.gitignore',

		'yiilite.php',

		'yiit.php',

		'/i18n/data',

		'/messages',

		'/vendor',

		'/web/js',

		'UserModule.php', // if you are using yii-user ... 

	),

);
