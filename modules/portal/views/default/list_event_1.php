<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\cms\models\Calendar;
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu;

$sql_event = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 10";
$all_events_proche = Calendar::findBySql($sql_event )->all();
setlocale(LC_TIME, "fr_FR.utf8");
$articles_latest = Article::findBySql("SELECT a.id, a.menus_id, a.body, a.images_id, a.title FROM  articles a INNER JOIN menus m ON (a.menus_id = m.id) INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.name = 'Blog' ORDER BY a.id DESC LIMIT 1")->all();
$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1";
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();

$this->title = Yii::t('app','List of events')

?>
<div class="container">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>

      <li class="breadcrumb-item active"><?= $this->title;  ?></li>
    </ol>

    <div class="row">
        <div class="col-lg-9">


            <!-- Events place  -->
            <div id="second-section2">

              <div class="row">
                  <div class="col-md-12">

                  <div class="triangle"></div>
                  </div>
              </div>

            <div class="row">
                <div class="col-md-12">

               <?php
                    foreach($all_events_proche as $aep){

                        $youtube_arr = explode("=",$aep->media);
                        if(isset($youtube_arr[1])){
                            $youtube_link = $youtube_arr[1];
                            $youtube_id = str_replace("&list","",$youtube_link);
                            $make_media_link = "<img src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg'  youtubeid='$youtube_id' style='width:500px;height:400px;'/>";
                             }else{
                                 $make_media_link = "<img style='width:500px;height:400px;' src='$aep->media'/>";
                             }
               ?>
                        <div class="cd-timeline-block">
                            <div class="cd-timeline-img cd-picture">
                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">

                            </div>



                            <div class="cd-timeline-content projects">
                               <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                  <?= $make_media_link; ?>
                               </a>
                                <div class="project-content">
                                    <h2>
                                        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                          <?= $aep->title; ?>
                                        </a>
                                    </h2>
                                    <span>
                                        <?= $aep->intervenant; ?>
                                    </span>
                                    <span>
                                        <?php
                                           $string_day = date("d",strtotime($aep->date_start));
                                           $string_month = strftime("%b",strtotime($aep->date_start));
                                           $string_year = date("Y",strtotime($aep->date_start));
                                           $str_time = date("h:i a",strtotime($aep->date_start));
                                           echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;
                                            //Yii::$app->formatter->locale = 'fr_FR';
                                            // echo Yii::t('app','{date}',['date'=>Yii::$app->formatter->AsDateTime($aep->date_start, 'long')]);

                                            ?>
                                    </span>
                                    <span>
                                        <?= $aep->location; ?>
                                    </span>
                                </div>
                            </div>
                

                        </div>






                    <?php } ?>

                    </section>
                </div>
            </div>

    </div>
            <!-- end of events place -->
        </div>

        <!-- A kote a  -->
        <div class="col-lg-3">


                    <!-- Page Content -->
                    <div class="container kote">

                      <h4 class="my-4"><?= Yii::t('app','The last news'); ?></h4>

                      <!-- Marketing Icons Section -->
                      <div class="row">
                          <?php
                              foreach($articles_latest as $al){
                                  $slug = "";
                                  $upm = "";
                                  if(isset(Menu::findOne($al->menus_id)->slug)){
                                      $slug = Menu::findOne($al->menus_id)->slug;
                                  }

                                  if(isset(Menu::findOne($al->menus_id)->parent_id)){
                                      $id_parent = Menu::findOne($al->menus_id)->parent_id;
                                      $upm = Menu::findOne($id_parent)->slug;
                                  }

                                  ?>
                          <div class="col-lg-11 kat">
                                  <div class="card h-100">
                                  <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>">
                                      <img class="card-img-top" src="<?= $al->images_id; ?>" alt=""></a>
                                  <!--  http://logipam.com/s/portal_assets/ckfinder/core/connector/php/images/images/Aide.png -->
                                  <h6 class="card-header">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>">
                                          <?= $al->title; ?>
                                      </a>
                                  </h6>
                                    <div class="card-body">
                                      <p class="card-text"><?= substr(strip_tags($al->body), 0,280); ?>.</p>
                                    </div>
                                    <div class="card-footer">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>" class="btn btn-primary"><?= Yii::t('app','Learn more'); ?></a>
                                    </div>
                                  </div>
                         </div>
                                  <?php
                                      }
                                  ?>
                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="social" style="align-content: center">
                                      <a href="#"><i class="fab fa-facebook-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-twitter-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-youtube-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-instagram fa-3x"></i></a>
                                  </div>
                              </div>
                          </div>

                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="message-item" style="align-content: center">
                                      <ul class="list-unstyled">
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie">
                                                <?= Yii::t('app','Gallery Photos'); ?>
                                              </a>
                                          </li>
                                          <li>
                                                <a href="#">
                                                    <?= Yii::t('app','Contact Us'); ?>
                                                </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                <?= Yii::t('app','Libray Catalog')?></a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <?= Yii::t('app','Bulletin'); ?>
                                              </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <?= Yii::t('app','Partners'); ?>
                                              </a>
                                          </li>

                                          <?php
                                            foreach($menu_lateral as $ml){
                                                ?>
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $ml->slug; ?>"><?= $ml->name; ?></a>
                                          </li>
                                          <?php
                                            }
                                          ?>

                                      </ul>
                                  </div>
                              </div>
                          </div>

                            </div>

                            </div>


            </div>

        <!-- Fen a kote a -->



    </div>

</div>
