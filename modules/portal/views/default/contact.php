<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use app\modules\cms\models\CmsIndex; 
use app\modules\cms\models\Article; 

$this->title = Yii::t('app','Contact us'); 


?>
<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      
      <li class="breadcrumb-item active"><?= $this->title;  ?></li>
    </ol>
    
    <section class="cid-raTa9YqTqg" id="form2-8">
    <div class="">
        <div class="row justify-content-center">
            <div class="form-1 col-md-8 col-sm-12" data-form-type="formoid">
                
                <form class="mbr-form" action="#" method="post" data-form-title="My Mobirise Form"><input type="hidden" name="email" data-form-email="true" value="bTID20xgsjELWbxea5y8Eiq01RcNU5R236drcBZtyWDUMP2URwWniD3R8+rS41g8IQRQDfFaePaVUbRx8wTWd4mxxJltuTQNmcvtbM1APp58U4HfnfmZp6BH/BlVkHWT">
                    <div class="row input-main">
                        <div class="col-md-6 input-wrap" data-for="name">
                            <input type="text" class="field display-7" name="name" data-form-field="Name" placeholder="<?= Yii::t('app','Nom'); ?>" required="" id="name-form2-8">
                        </div>
                        <div class="col-md-6 input-wrap" data-for="email">
                            <input type="email" class="field display-7" name="email" data-form-field="Email" placeholder="<?= Yii::t('app','Email'); ?>" required="" id="email-form2-8">
                        </div>
                    </div>
                    <div class="row input-main">
                        
                        <div class="col-md-6 input-wrap" data-for="subject">
                            
                        </div>
                    </div>
                    <div class="form-group" data-for="message">
                        <textarea type="text" class="form-control display-7" name="message" rows="7" placeholder="<?= Yii::t('app','Message'); ?>" data-form-field="Message" id="message-form2-8"></textarea>
                    </div>
                    <span class="input-group-btn">
                        <a href=""  class="btn btn-form btn-primary display-4"><?= Yii::t('app','Envoyer'); ?></a>
                    </span>
                </form>
                <p>
                    <br/>
                    <br/>
                    <h4>Retrouvez-nous </h4>
                    
                </p>
                 
                 <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3782.8247194260425!2d-72.31940993369487!3d18.536821373141024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8eb9e7e569b3a64f%3A0xb64bd4b1983133c2!2sCentre%20Culturel%20Pyepoudre!5e0!3m2!1sen!2sht!4v1591287238297!5m2!1sen!2sht" width="850" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">

                 </iframe>
              
            </div>
        </div>
   </div>
        
</section>
    
</div>



<?php
/*
$sim = similar_text('bafoobar', 'barfoo', $perc);
echo "similarity: $sim ($perc %)\n";
$sim = similar_text('barfoo', 'bafoobar', $perc);
echo "similarity: $sim ($perc %)\n";

$one_article = Article::findOne(22);
$sql_src = "SELECT * FROM articles WHERE id <> $one_article->id AND is_publish = 1"; 
$all_other_articles = Article::findBySql($sql_src)->all();



$similar_id = [];
$k = 0; 
foreach($all_other_articles as $aoa){
    $sim = similar_text(strip_tags($one_article->body), strip_tags($aoa->body), $perc);
    echo $aoa->id.' -- '.$perc.'<br/>';
    if($perc > 15.00){
        $similar_id[$k] = $aoa->id; 
        $k++;
    }
}

print_r($similar_id); 

// print_r($all_other_articles);
*/


