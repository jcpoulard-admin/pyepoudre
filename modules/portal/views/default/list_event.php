<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\cms\models\Calendar;
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu;
use app\modules\cms\models\Epingles; 

$sql_event = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 10";
$all_events_proche = Calendar::findBySql($sql_event )->all();
setlocale(LC_TIME, "fr_FR.utf8");
$articles_latest = Article::findBySql("SELECT a.id, a.menus_id, a.body, a.images_id, a.title FROM  articles a INNER JOIN menus m ON (a.menus_id = m.id) INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.name = 'Blog' ORDER BY a.id DESC LIMIT 1")->all();
$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1";
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();
$all_epingles = Epingles::findbySql("SELECT * FROM epingles WHERE is_publish = 1 ORDER BY id DESC LIMIT 5 ")->all(); 

$this->title = Yii::t('app','List of events')

?>
<div class="container">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>

      <li class="breadcrumb-item active"><?= $this->title;  ?></li>
    </ol>
    
    <div class="row">
        <div class="col-lg-9">
            <div class="row"> 
                <?php
                    foreach($all_events_proche as $aep){
                        $youtube_arr = explode("=",$aep->media);
                        if(isset($youtube_arr[1])){
                            $youtube_link = $youtube_arr[1];
                            $youtube_id = str_replace("&list","",$youtube_link);
                            $make_media_link = "<img  class='card-img-top' src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg'  youtubeid='$youtube_id' />";
                             }else{
                                 $make_media_link = "<img class='card-img-top' src='$aep->media'/>";
                             }
                        ?>
                <div class="col-lg-6 taylin portfolio-item">
                    <div class="card h-100">
                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog"><?= $make_media_link; ?></a>
                      <div class="card-body">
                        <h4>
                          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog"><?= $aep->title; ?></a>
                        </h4>
                          
                        <p class="card-text">
                            <span>
                                <strong> <?= $aep->intervenant; ?></strong>
                            </span>
                            <br/>
                            <span>
                                <strong>
                                <?php
                                   $string_day = date("d",strtotime($aep->date_start));
                                   $string_month = strftime("%b",strtotime($aep->date_start));
                                   $string_year = date("Y",strtotime($aep->date_start));
                                   $str_time = date("h:i a",strtotime($aep->date_start));
                                   echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;
                                    //Yii::$app->formatter->locale = 'fr_FR';
                                    // echo Yii::t('app','{date}',['date'=>Yii::$app->formatter->AsDateTime($aep->date_start, 'long')]);

                                    ?>
                                    </strong>
                                
                            </span>
                            <br/>
                            <span>
                               <strong> <?= $aep->location; ?></strong>
                            </span>
                        </p>
                      </div>
                    </div>
                </div>
                <?php 
                    }
                ?>
                
            </div>
        </div>
        <!-- A kote a  -->
        <div class="col-lg-3">


                    <!-- Page Content -->
                    <div class="container kote">
                        
                    <!--    
                      <h4 class="my-4"><i class="fas fa-thumbtack">&nbsp;&nbsp;&nbsp;&nbsp;<?= Yii::t('app','Epingl&eacute;s'); ?></i></h4>
                    -->
                      <!-- Marketing Icons Section -->
                      <div class="row">
                         <div class="col-lg-11 kat">
                          <div class="card h-100">
                              <div class="message-item" style="align-content: center">


                                  <div class="social" style="align-content: center">
                                      <h4> <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                              &nbsp;&nbsp;<span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>  <?= Yii::t('app','Libray Catalog')?></a>
                                      </h4>
                                  </div>
                              </div>
                          </div>
                          </div> 
                          
                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="message-item" style="align-content: center">
                                      <ul class="list-unstyled">
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                                <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>   <?= Yii::t('app','Bulletin'); ?>
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie">
                                               <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span> <?= Yii::t('app','Gallery Photos'); ?>
                                              </a>
                                          </li>
                                          <?php 
                                            foreach($menu_lateral as $ml){
                                                ?>
                                          <li>
                                             <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span> <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $ml->slug; ?>"><?= $ml->name; ?></a>
                                          </li>
                                          <?php 
                                            }
                                          ?>
                                          <li>
                                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/contact">
                                                  <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>  <?= Yii::t('app','Contact Us'); ?>
                                                </a>
                                          </li>

                                      </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-11 kat">
                                  <div class="card h-100">
                                      <h4 class="my-4"> <i class="fas fa-thumbtack">&nbsp;&nbsp;&nbsp;&nbsp;<?= Yii::t('app','Epingl&eacute;s'); ?></i></h4>
                                      <div class="message-item" style="align-content: center">
                                          <ul class="list-unstyled">
                                              <?php 
                                                foreach($all_epingles as $ae){
                                                    ?>
                                              <li>
                                                  <a href="<?= $ae->url_epingle;?>" target="_new">
                                                    <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>  <?= $ae->nom_epingle; ?>
                                                  </a>
                                              </li>
                                              <?php
                                                }
                                              ?>
                                         
                                          </ul>
                                      </div>
                                  </div>
                            </div>
                          <div class="col-lg-11 kat">
                            
                          </div>

                          

                            </div>

                            </div>


            </div>

        <!-- Fen a kote a -->
    </div>
    
    
</div>
