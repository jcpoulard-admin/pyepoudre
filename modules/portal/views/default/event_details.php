<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


use yii\helpers\Url;
use app\modules\cms\models\Calendar;
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu;
setlocale(LC_TIME, "fr_FR.utf8");
//setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
$articles_latest = Article::findBySql("SELECT a.id, a.menus_id, a.body, a.images_id, a.title FROM  articles a INNER JOIN menus m ON (a.menus_id = m.id) INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.name = 'Blog' ORDER BY a.id DESC LIMIT 1")->all();
$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1";
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();

$sql_event = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) AND id <> $id ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 20";
$all_events_proche = Calendar::findBySql($sql_event )->all();


$this->title = Calendar::findOne($id)->title;
$calendar = Calendar::findOne($id);
?>
<div class="container">


    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      <li class="breadcrumb-item">
          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/list-event?pos=prog"><?= Yii::t('app','List events'); ?></a>
      </li>
      <li class="breadcrumb-item active"><?= $this->title;  ?></li>
    </ol>

    <div class="row">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-lg-6 detaya">
<h1 class="mt-4 mb-3">
  <?= $this->title;  ?>
</h1>
                    <?php
                 $youtube_arr = explode("=",$calendar->media);
                if(isset($youtube_arr[1])){
                    $youtube_link = $youtube_arr[1];
                    $youtube_id = str_replace("&list","",$youtube_link);
                    
                    $make_media_link = "<iframe width='420' height='350' src='https://www.youtube.com/embed/$youtube_id' frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
                    
                   // $make_media_link = "<img src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg' style='width:500px;height:400px;' class='img-fluid rounded'  youtubeid='$youtube_id' />";
                     }else{
                         $make_media_link = "<img  class='img-fluid rounded' style='width:500px;height:400px;' src='$calendar->media'/>";
                     }
                    echo $make_media_link;
                    ?>
                </div>
                <div class="col-lg-6 detayb">

                    <h3> <?= $calendar->intervenant; ?></h3>

                    <h3> <?= $calendar->location; ?></h3>
                    <h4>
                        <?php
                            $string_day = date("d",strtotime($calendar->date_start));
                           $string_month = strftime("%b",strtotime($calendar->date_start));
                           $string_year = date("Y",strtotime($calendar->date_start));
                           $str_time = date("h:i a",strtotime($calendar->date_start));
                           echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;
                           // Yii::$app->formatter->locale = 'fr_FR';
                           // echo Yii::t('app','{date}',['date'=>Yii::$app->formatter->AsDateTime($calendar->date_start, 'long')]);

                        ?>
                    </h4>
                    <h4>
                        <?= $calendar->prix; ?>
                    </h4>
                    <p>
                        <?= $calendar->description; ?>
                    </p>
                </div>
            </div>

        </div>

         <!-- A kote a  -->
        <div class="col-lg-4">
           <div class="container">

   <div class="row">

      <div class="col-md-12 col-lg-12">
         <div id="tracking-pre"></div>
         <div id="tracking">
            <div class="text-center tracking-status-intransit">
               <p class="tracking-status text-tight"><?= Yii::t('app','Autres évènements'); ?></p>
            </div>
            <div class="tracking-list">
                <?php
                foreach($all_events_proche as $aep){
                ?>

               <div class="tracking-item">
                   <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                   <div class="tracking-icon status-inforeceived">
                     <svg class="svg-inline--fa fa-clipboard-list fa-w-12" aria-hidden="true" data-prefix="fas" data-icon="clipboard-list" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                        <path fill="currentColor" d="M336 64h-80c0-35.3-28.7-64-64-64s-64 28.7-64 64H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM96 424c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm0-96c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm0-96c-13.3 0-24-10.7-24-24s10.7-24 24-24 24 10.7 24 24-10.7 24-24 24zm96-192c13.3 0 24 10.7 24 24s-10.7 24-24 24-24-10.7-24-24 10.7-24 24-24zm128 368c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-96c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16zm0-96c0 4.4-3.6 8-8 8H168c-4.4 0-8-3.6-8-8v-16c0-4.4 3.6-8 8-8h144c4.4 0 8 3.6 8 8v16z"></path>
                     </svg>
                     <!-- <i class="fas fa-clipboard-list"></i> -->
                  </div>
                   </a>
                  <div class="tracking-date">
                      <span>
                      <?php
                       Yii::$app->formatter->locale = 'fr_FR';
                       $string_day = date("d",strtotime($aep->date_start));
                       $string_month = strftime("%b",strtotime($aep->date_start));
                       $string_year = date("Y",strtotime($aep->date_start));
                       echo $string_day.' '.$string_month.' '.$string_year;

                      ?>
                      </span>
                      <span>
                          <?php
                        // Yii::$app->formatter->locale = 'fr_FR';
                       // echo Yii::t('app','{date}',['date'=>Yii::$app->formatter->AsTime($aep->date_start, 'short')]);
                       echo date("h:i a",strtotime($aep->date_start));
                       //echo strftime('%A',strtotime($le->date_start));
                         ?>

                      </span></div>
                  <div class="tracking-content">
                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                       <?= $aep->title; ?>
                      </a>
                      <span>
                          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                         <?= $aep->intervenant; ?>, <?= $aep->location; ?>
                          </a>
                      </span></div>
               </div>
                <?php
                }
                ?>

            </div>
         </div>
      </div>
   </div>
</div>
        </div>

        <!-- Fen a kote a -->

    </div>
</div>
