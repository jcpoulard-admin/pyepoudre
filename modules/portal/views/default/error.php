<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title  = Yii::t('app','Page not found !');
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        Page introuvable 
        <?= nl2br(Html::encode($slug)) ?>
        
    </div>

    
</div>
