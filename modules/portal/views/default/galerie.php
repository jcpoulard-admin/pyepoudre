<?php 
use app\modules\cms\models\Image; 
$this->title  = Yii::t('app','Photo Gallery');

?>


<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      <li class="breadcrumb-item active"><?= Yii::t('app','Photo gallery');  ?></li>
    </ol>
    
    <div class="row">
      
    <?php
        foreach($all_galerie as $ag){
            $all_image_in_album = Image::findOne(['component_id'=>$ag->id, 'is_publish'=>1]);
            if(isset($all_image_in_album)){
            
            ?>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <?= $ag->name; ?>
            <a class="thumbnail" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie-detail?id=<?= $ag->id; ?>"  data-title="<?= $all_image_in_album->title; ?>"
               data-image="<?= $all_image_in_album->name; ?>"
               data-target="#image-gallery">
                <img class="img-thumbnail"
                     src="<?= $all_image_in_album->name; ?>"
                     alt="<?= $all_image_in_album->title; ?>">
            </a>
        </div>
            
    <?php 
            }   
        }
    ?>
    </div>
    
    
</div>


