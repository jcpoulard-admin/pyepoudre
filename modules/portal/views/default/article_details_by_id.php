<?php
use app\modules\cms\models\Article;
use yii\helpers\Url;
use app\modules\cms\models\Component; 
use app\modules\cms\models\Image; 
use app\modules\cms\models\Menu; 


$this->title = $all_article->title;
setlocale(LC_TIME, "fr_FR");
$url =  "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$article_id = $all_article->id;

$twitter_account = infoGeneralConfig('twitter_account'); 

if(isset($twitter_account) && $twitter_account !== ''){
    $twitter_account = infoGeneralConfig('twitter_account'); 
}else{
    $twitter_account = 'jcpoulard'; 
}

if(isset($_GET['id'])){
    $id_article = $_GET['id']; 
    $id_menu = Article::findOne($id_article)->menus_id; 
    $menu_name = Menu::findOne($id_menu)->name; 
    $upm = Menu::findOne($id_menu)->slug;
    if(Menu::findOne($id_menu)->parent_id!=null){
        $id_parent = Menu::findOne($id_menu)->parent_id;
        $parent_menu = Menu::findOne($id_parent)->name; 
        $upm = Menu::findOne($id_parent)->slug;
    }else{
        $parent_menu = null;
    }
    
}else{
    $menu_name = null; 
}

if(isset($_GET['upm']) && $_GET['upm']!=''){
    $origin = $_GET['upm']; 
    $origin_menu = Menu::findOne(['slug'=>$origin])->name; 
    $upm = Menu::findOne(['slug'=>$origin])->slug;
}else{
    $origin_menu = null;
    //$upm  = null;
}

?>

<div class="container">
    <!-- Page Heading/Breadcrumbs -->

   
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      <?php 
        if(isset($parent_menu)){
            ?>
          <li class="breadcrumb-item">
                <?= $parent_menu; ?>
          </li>
      <?php 
        }
      ?>
       <?php 
        if(isset($menu_name)){
            ?>
          <li class="breadcrumb-item">
                <?= $menu_name; ?>
          </li>
      <?php 
        }
      ?>
      <li class="breadcrumb-item active"><?= $all_article->title;  ?></li>
    </ol>

    <div class="row">
        <div class="col-lg-9">
            
            <?php 
                if($all_article->components_id != NULL || $all_article->components_id != ''){
                    // detect if the component is a album 
                  
                           if(isset(Component::findOne(['id'=>$all_article->components_id,'is_publish'=>1])->type)){
                                 $component_type = Component::findOne(['id'=>$all_article->components_id,'is_publish'=>1])->type;    
                   if($component_type == 'album'){
                            
                            // Load all image associate with this component 
                            if(isset(Image::findOne(['component_id'=>$all_article->components_id, 'is_publish'=>1])->id)){
                                $sql_str = "SELECT * FROM images WHERE component_id = $all_article->components_id AND is_publish = 1 ORDER BY id ASC LIMIT 8"; 
                                $all_image_in_album = Image::findBySql($sql_str)->all();  
                                
                       ?>
            
                        <!--  Place for Gallerie Photo here -->
            <div class="row">
		<div class="row">
                   
                    <?php 
                        foreach($all_image_in_album as $aia){
                    ?>
                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="<?= $aia->title; ?>"
                           data-image="<?= $aia->name; ?>"
                           data-target="#image-gallery">
                            <img class="img-thumbnail"
                                 src="<?= $aia->name; ?>"
                                 alt="<?= $aia->title; ?>">
                        </a>
                    </div>

                    <?php
                        }
                    ?>

                </div>


        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
	</div>
           
            <!-- End of place for gallery photo -->

            <?php 
                            }
                   }elseif($component_type == 'carousel'){
                        // Load all image associate with this component 
                            if(isset(Image::findOne(['component_id'=>$all_article->components_id, 'is_publish'=>1])->id)){
                                $sql_str = "SELECT * FROM images WHERE component_id = $all_article->components_id AND is_publish = 1 ORDER BY id ASC LIMIT 8"; 
                                $all_carousel = Image::findBySql($sql_str)->all();
                                ?>
            <header>
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                            $k = 0;
                            foreach($all_carousel as $carou){
                                if($all_article->components_id == $carou->component_id){
                        ?>
                      <li data-target="#carouselExampleIndicators" data-slide-to="<?= $k?>" class="<?php if($k==0) echo "active" ?>"></li>
                      
                            <?php 
                            $k++; 
                                }
                            } 
                            ?>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                          $i = 0;
                          foreach($all_carousel as $ac){
                              if($all_article->components_id == $ac->component_id){
                              ?>
                        <!-- Slide One - Set the background image for this slide in the line below -->
                      <div class="carousel-item <?php if($i==0) echo "active" ?>" style="background-image: url('<?= $ac->name; ?>')">
                        <div class="carousel-caption d-none d-md-block">
                          <h3><?= $ac->title; ?> </h3>

                        </div>
                      </div>
                        <?php
                        
                          $i++;
                              }
                          }
                        ?>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </header>
            
            <?php 
                                
                                
                    }
                       
                       
                   }
                   }
           }
           ?>    
            
             <!-- Preview Image -->
             <?php
                    // Teste si un album photo ou un carrusel n'a pas ete choisi lors de la creation d'un article
                    if($all_article->components_id == '' || $all_article->components_id == NULL) {
                       
            ?>
              <img class="img-fluid rounded" src="<?= $all_article->images_id ?>" alt="<?= $all_article->components_id; ?>">
                 
             <?php }else{
                
                 ?>
                
                <?php 
             } ?>
<h1 class="mt-4 mb-3">
     <?= $all_article->title;  ?>
   </h1>

            <?= closetags($all_article->body);  ?>
            <!-- Date/Time -->
            <p class="dat">
                <?php 
                Yii::$app->formatter->locale = 'fr_FR';
                echo Yii::t('app','Posted at {date}',['date'=>Yii::$app->formatter->format($all_article->create_date, 'date')]);?>
            </p>  
             
             <div class="row" id="social-media-share">
                     <div class="col-lg-2">
                         <a id="facebook-pataj" >
                            <i class="fab fa-facebook fa-3x" style="color : #4267B2;" title="<?= Yii::t('app','Share on facebook'); ?>"></i>
                        </a>
                     </div>
                     <div class="col-lg-2">
                         <a id="twitter-pataj" >
                             <i class="fab fa-twitter fa-3x" style="color : #1A91DA;" title="<?= Yii::t('app','Share on twitter'); ?>"></i>
                         </a>
                     </div>
                 </div>
        </div>
        
        <div class="col-md-3 kote">
            <!-- Search Widget -->
            <!--
            <div class="card mb-4">
              <h5 class="card-header"><?= Yii::t('app','Search'); ?></h5>
              <div class="card-body">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="<?= Yii::t('app','Search for ...'); ?>">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><?= Yii::t('app','Go!'); ?></button>
                  </span>
                </div>
              </div>
            </div>
            -->
            <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header"><?= Yii::t('app','Related articles'); ?></h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <ul class="list-unstyled mb-0">
                    <?php 
                        $all_related_article = relatedArticle($article_id); 
                        if(!empty($all_related_article)){
                            for($i=0; $i<count($all_related_article); $i++){
                                $une_article_lie = Article::findOne($all_related_article[$i]); 
                                $menu_id = $une_article_lie->menus_id; 
                                if(isset(Menu::findOne($menu_id)->parent_id)){
                                    $parent_id = Menu::findOne($menu_id)->parent_id; 
                                    $upm = Menu::findOne($parent_id)->slug; 
                                }else{
                                     Menu::findOne($menu_id)->slug;
                                }
                                
                            ?>
                            <li>
                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details-by-id?id=<?= $une_article_lie->id; ?>&upm=<?= $upm; ?>">
                                    <?= $une_article_lie->title; ?>
                                </a>
                            </li>
                            
                        <?php 
                                }
                            }
                        ?>
                   </ul>
              </div>

            </div>
          </div>
        </div>
            <!-- Side Widget -->
        <div class="card my-4">
         <!-- <h5 class="card-header">Side Widget</h5> -->
          <div class="card-body">
              <a class="twitter-timeline" data-lang="fr" data-theme="light" data-height="500" href="https://twitter.com/<?= $twitter_account; ?>?ref_src=twsrc%5Etfw">
                </a>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
           </div>
        </div>
        </div>

    </div>



</div>




<?php 
$script = <<< JS
   let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let sel = selector;
        current_image = sel.data('image-id');
        $('#image-gallery-title')
          .text(sel.data('title'));
        $('#image-gallery-image')
          .attr('src', sel.data('image'));
        disableButtons(counter, sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

     
        
        
        
   $(document).ready(function(){
        $("#facebook-pataj").click(function(){
            var url = encodeURIComponent('$url');
            window.open('https://wwww.facebook.com/sharer.php?u='+url,'Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+'');
            return false;
                   
            });
        
        $("#twitter-pataj").click(function(){
            var url = encodeURIComponent('$url');
            var teks = '$all_article->title'; 
            window.open('https://twitter.com/share?url='+url+'&text='+teks,'Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+'');
            return false;
                   
        });
        
        
      });

JS;
$this->registerJs($script);

?>
