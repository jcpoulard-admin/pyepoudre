<?php
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu; 
use app\modules\cms\models\Component; 
use app\modules\cms\models\Image;

$this->title = $one_blog->title;
$article_id = $one_blog->id; 

setlocale(LC_TIME, "fr_FR");

if(isset($_GET['upm'])){
    $upm = $_GET['upm']; 
    
}

if(isset($_GET['ori'])){
    $origin = $_GET['ori']; 
    $origin_menu = Menu::findOne(['slug'=>$origin])->name; 
}

$url =  "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );

if(isset($twitter_account) && $twitter_account !== ''){
    $twitter_account = infoGeneralConfig('twitter_account'); 
}else{
    $twitter_account = 'jcpoulard'; 
}

?>

<div class="container">
    <!-- Page Heading/Breadcrumbs -->

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      <li class="breadcrumb-item">
          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $origin ?>&upm=<?= $upm ?>"><?= $origin_menu; ?></a>
      </li>
      <li class="breadcrumb-item active"><?= $one_blog->title;  ?></li>
    </ol>

    <div class="row">
         
        <div class="col-lg-8">
            <!-- Share the article on social media --> 
            <?php 
                if($one_blog->components_id != NULL || $one_blog->components_id != ''){
                    // detect if the component is a album 
                  
                           if(isset(Component::findOne(['id'=>$one_blog->components_id,'is_publish'=>1])->type)){
                                 $component_type = Component::findOne(['id'=>$one_blog->components_id,'is_publish'=>1])->type;    
                   if($component_type == 'album'){
                            
                            // Load all image associate with this component 
                            if(isset(Image::findOne(['component_id'=>$one_blog->components_id, 'is_publish'=>1])->id)){
                                $all_image_in_album = Image::find(['component_id'=>$one_blog->components_id, 'is_publish'=>1])->orderBy('id ASC')->limit(8)->all(); 
                                
                       ?>
            
                        <!--  Place for Gallerie Photo here -->
            <div class="row">
		<div class="row">
                   
                    <?php 
                        foreach($all_image_in_album as $aia){
                    ?>
                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                        <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="<?= $aia->title; ?>"
                           data-image="<?= $aia->name; ?>"
                           data-target="#image-gallery">
                            <img class="img-thumbnail"
                                 src="<?= $aia->name; ?>"
                                 alt="<?= $aia->title; ?>">
                        </a>
                    </div>

                    <?php
                        }
                    ?>

                </div>


        <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
	</div>
           
            <!-- End of place for gallery photo -->
            <?php 
                            }
                   }elseif($component_type == 'carousel'){
                        // Load all image associate with this component 
                            if(isset(Image::findOne(['component_id'=>$one_blog->components_id, 'is_publish'=>1])->id)){
                                $all_carousel = Image::find(['component_id'=>$one_blog->components_id, 'is_publish'=>1])->orderBy('id ASC')->limit(8)->all();
                                ?>
            <header>
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                            $k = 0;
                            foreach($all_carousel as $carou){
                                if($one_blog->components_id == $carou->component_id){
                        ?>
                      <li data-target="#carouselExampleIndicators" data-slide-to="<?= $k?>" class="<?php if($k==0) echo "active" ?>"></li>
                      
                            <?php 
                            $k++; 
                                }
                            } 
                            ?>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                          $i = 0;
                          foreach($all_carousel as $ac){
                              if($one_blog->components_id == $ac->component_id){
                              ?>
                        <!-- Slide One - Set the background image for this slide in the line below -->
                      <div class="carousel-item <?php if($i==0) echo "active" ?>" style="background-image: url('<?= $ac->name; ?>')">
                        <div class="carousel-caption d-none d-md-block">
                          <h3><?= $ac->title; ?> </h3>

                        </div>
                      </div>
                        <?php
                        
                          $i++;
                              }
                          }
                        ?>
                        
                        

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </header>
            
            <?php 
                                
                                
                    }
                       
                       
                   }
                   }
           }
           ?>    
            
             <!-- Preview Image -->
             <?php
                    // Teste si un album photo ou un carrusel n'a pas ete choisi lors de la creation d'un article
                    if($one_blog->components_id == '' || $one_blog->components_id == NULL) {
                       
            ?>
              <img class="img-fluid rounded" src="<?= $one_blog->images_id ?>" alt="<?= $one_blog->images_id; ?>">
                 
             <?php }else{
                
                 ?>
                
                <?php 
             } ?>
            
<h1 class="mt-4 mb-3">
     <?= $one_blog->title;  ?>
   </h1>
            <?php
                $url = Yii::getAlias('@web')."/index.php/portal/default/article-details-by-id?id=".$one_blog->id; 
               $title = $one_blog->title;
               if(strlen($one_blog->body) > 2000)
                    echo  closetags(truncate($one_blog->body,2000,$url,$title));
               else 
                   echo closetags($one_blog->body);
                   // $one_blog->body;  
            ?>
             
                 <!-- Date/Time -->
                <p class="dat">
                    <?php Yii::$app->formatter->locale = 'fr_FR';
                    echo Yii::t('app','Posted at {date}',['date'=>Yii::$app->formatter->format($one_blog->create_date, 'date')]);?>
                </p>
                 <div class="row" id="social-media-share">
                     <div class="col-lg-2">
                         <a id="facebook-pataj" >
                            <i class="fab fa-facebook fa-3x" style="color : #4267B2;" title="<?= Yii::t('app','Share on facebook'); ?>"></i>
                        </a>
                     </div>
                     <div class="col-lg-2">
                         <a id="twitter-pataj" >
                             <i class="fab fa-twitter fa-3x" style="color : #1A91DA;" title="<?= Yii::t('app','Share on twitter'); ?>"></i>
                         </a>
                     </div>
                 </div>
             
             
        </div>
        <div class="col-md-4 kote">
            <!-- Search Widget -->
            <!--
            <div class="card mb-4">
              <h5 class="card-header"><?= Yii::t('app','Search'); ?></h5>
              <div class="card-body">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="<?= Yii::t('app','Search for ...'); ?>">
                  <span class="input-group-btn">
                    <button class="btn btn-secondary" type="button"><?= Yii::t('app','Go!'); ?></button>
                  </span>
                </div>
              </div>
            </div>
            -->
            <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header"><?= Yii::t('app','Related articles'); ?></h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <ul class="list-unstyled mb-0">
                  <?php 
                        
                        $all_related_article = relatedArticle($article_id); 
                        if(!empty($all_related_article)){
                            for($i=0; $i<count($all_related_article); $i++){
                                $une_article_lie = Article::findOne($all_related_article[$i]); 
                                $menu_id = $une_article_lie->menus_id; 
                                if(isset(Menu::findOne($menu_id)->parent_id)){
                                    $parent_id = Menu::findOne($menu_id)->parent_id; 
                                    $upm = Menu::findOne($parent_id)->slug; 
                                }else{
                                     Menu::findOne($menu_id)->slug;
                                }
                                
                            ?>
                            <li>
                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $une_article_lie->slug; ?>&upm=<?= $upm; ?>&ori=<?= $upm ?>">
                                    <?= $une_article_lie->title; ?>
                                </a>
                            </li>
                            
                        <?php 
                                }
                            }
                        ?>
                </ul>
              </div>

            </div>
          </div>
        </div>
            
            <div class="card my-4">
        <!--  <h5 class="card-header">Side Widget</h5> -->
          <div class="card-body">
              <a class="twitter-timeline" data-lang="fr" data-theme="light" data-height="500" href="https://twitter.com/<?= $twitter_account; ?>?ref_src=twsrc%5Etfw">
                </a>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
            </div>
        </div>
            <!-- Side Widget -->
       <!--   
        <div class="card my-4">
          <h5 class="card-header">Side Widget</h5>
          <div class="card-body">
            You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
          </div>
        </div>
          -->  
        </div>

    </div>



</div>

<?php 
$script = <<< JS
    let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image,
        selector,
        counter = 0;

      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let sel = selector;
        current_image = sel.data('image-id');
        $('#image-gallery-title')
          .text(sel.data('title'));
        $('#image-gallery-image')
          .attr('src', sel.data('image'));
        disableButtons(counter, sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });    
   
    $(document).ready(function(){
        $("#facebook-pataj").click(function(){
            var url = encodeURIComponent('$url');
            window.open('https://wwww.facebook.com/sharer.php?u='+url,'Facebook','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+'');
            return false;
                   
            });
        
        $("#twitter-pataj").click(function(){
            var url = encodeURIComponent('$url');
            var teks = '$one_blog->title'; 
            window.open('https://twitter.com/share?url='+url+'&text='+teks,'Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+'');
            return false;
                   
        });
      });

JS;
$this->registerJs($script);

?>

<!-- 
<div class="twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share On Twitter">
						
						<a class="twitter" onclick="window.open('http://twitter.com/share?url=https://fokal.org/index.php/nouvel-fokal/31-nos-programmes/1463-mois-de-l-histoire-des-noirs-black-history-month-au-cine-corner&amp;text=Mois%20de%20l'histoire%20des%20Noirs%20(%20Black%20History%20Month%20)%20au%20Ciné%20Corner','Twitter share','width=600,height=300,left='+(screen.availWidth/2-300)+',top='+(screen.availHeight/2-150)+''); return false;" href="http://twitter.com/share?url=https://fokal.org/index.php/nouvel-fokal/31-nos-programmes/1463-mois-de-l-histoire-des-noirs-black-history-month-au-cine-corner&amp;text=Mois%20de%20l'histoire%20des%20Noirs%20(%20Black%20History%20Month%20)%20au%20Ciné%20Corner">
							<i class="fa fa-twitter"></i>
						</a>

</div>
--> 
