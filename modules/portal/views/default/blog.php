<?php
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu; 

if(isset(Menu::findOne(['slug'=>$slug])->id)){
    $this->title = Menu::findOne(['slug'=>$slug])->name;
}else{
     $this->title = 'Site web Pyepoudre';
}

setlocale(LC_TIME, "fr_FR");
$twitter_account = infoGeneralConfig('twitter_account'); 

if(isset($twitter_account) && $twitter_account !== ''){
    $twitter_account = infoGeneralConfig('twitter_account'); 
}else{
    $twitter_account = 'jcpoulard'; 
}

$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>

<div class="container">
   

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/index"><?= Yii::t('app','Accueil'); ?></a>
      </li>
      <li class="breadcrumb-item active"><?= $this->title;  ?></li>
    </ol>
    
    
    <div class="row">
        <div class="col-lg-9">
            <div class="row"> 
        <?php 
        $k = 0; 
        foreach($all_article as $aa){
            ?>
        <div class="col-lg-6">
            <?php 
             $slug = "";
              $upm = "";
              if(isset(Menu::findOne($aa->menus_id)->slug)){
                  $slug = Menu::findOne($aa->menus_id)->slug;
              }

              if(isset(Menu::findOne($aa->menus_id)->parent_id)){
                  $id_parent = Menu::findOne($aa->menus_id)->parent_id;
                  $upm = Menu::findOne($id_parent)->slug;
              }

            ?>
              <div class="col-lg-11                                                                                                                                                                                                                                                                                                                                                             kat">
                      <div class="card h-100">
                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $aa->slug; ?>&upm=<?= $upm ?>&ori=<?= $slug ?>">
                          <img class="card-img-top" src="<?= $aa->images_id; ?>" alt=""></a>
                      <!--  http://logipam.com/s/portal_assets/ckfinder/core/connector/php/images/images/Aide.png -->
                      <h6 class="card-header">
                          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $aa->slug; ?>&upm=<?= $upm ?>&ori=<?= $slug ?>">
                              <?= $aa->title; ?>
                          </a>
                      </h6>
                        <div class="card-body">
                          <p class="card-text"><?= substr(strip_tags($aa->body), 0,300); ?>.</p>
                        </div>
                        <div class="card-footer">
                          <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $aa->slug; ?>&upm=<?= $upm ?>&ori=<?= $slug ?>" class="btn btn-primary"><?= Yii::t('app','Learn more'); ?></a>
                        </div>
                      </div>
             </div>
        </div>
        <?php 
        }
        ?>
            </div>
        </div>
        <div class="col-lg-3">
            
            <a class="twitter-timeline" data-lang="fr" data-theme="light" data-height="500" href="https://twitter.com/<?= $twitter_account; ?>?ref_src=twsrc%5Etfw">
                </a>
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
        </div>
    </div>
    

</div>
<!---
<a class="twitter-timeline" data-lang="fr" data-height="300" data-theme="dark" href="https://twitter.com/TwitterDev?ref_src=twsrc%5Etfw">Tweets by TwitterDev</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
