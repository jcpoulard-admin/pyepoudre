<?php
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu;
use app\modules\cms\models\Image;
use app\modules\cms\models\Calendar; 

$this->title = "Site web Pyepoudre";
$articles_latest = Article::findBySql("SELECT a.id, a.menus_id, a.body, a.images_id, a.title FROM  articles a INNER JOIN menus m ON (a.menus_id = m.id) INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.name = 'Blog' ORDER BY a.id DESC LIMIT 1")->all();
$carousel_accueil = Image::findBySql("SELECT i.name, i.title FROM images i INNER JOIN components c ON (i.component_id = c.id) WHERE c.is_home = 1 AND c.is_publish = 1 AND i.is_publish = 1 ORDER BY i.id DESC")->all();
$home_article = Article::findOne(['is_home'=>1]);
$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1"; 
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();
$sql_event = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 3"; 
$all_events_proche = Calendar::findBySql($sql_event )->all(); 
setlocale(LC_TIME, "fr_FR.utf8");

?>

<div class="container akey">
<div class="row">
          <div class="col-lg-9">
                <header>
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                            $k = 0; 
                            foreach($carousel_accueil as $carou){
                        ?>
                      <li data-target="#carouselExampleIndicators" data-slide-to="<?= $k; ?>" class="<?php if($k==0) echo "active" ?>"></li>
                            <?php 
                            
                            $k++; 
                            } ?>
                      
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                          $i = 0;
                          foreach($carousel_accueil as $ca){
                              ?>
                        <!-- Slide One - Set the background image for this slide in the line below -->
                      <div class="carousel-item <?php if($i==0) echo "active" ?>" style="background-image: url('<?= $ca->name; ?>')">
                        <div class="carousel-caption d-none d-md-block">
                          <h3><?= $ca->title; ?></h3>

                        </div>
                      </div>
                        <?php
                          $i++;
                          }
                        ?>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </header>

                <div class="col-lg-12">
                    <div class="row">
                        &nbsp;
                    </div>
                    <!-- Place les evements ici  -->
                            <div class="row eve">
                            <div class="col-lg-12">
                               
                                    <h2> Nos prochains événements </h2>
                               
                                </div>
                            <?php 
                                foreach($all_events_proche as $aep){
                                    $youtube_arr = explode("=",$aep->media);
                                    if(isset($youtube_arr[1])){
                                        $youtube_link = $youtube_arr[1];
                                        $youtube_id = str_replace("&list","",$youtube_link); 
                                        $make_media_link = "<img src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg' class='card-img-top youtube-link' youtubeid='$youtube_id'/>";  
                                         }else{
                                             $make_media_link = "<img  class='card-img-top' width='400px' heigth='300px' src='$aep->media'/>";
                                         }
                                    ?>
                                <div class="col-lg-4 col-sm-6 portfolio-item">
                                <div class="card h-100">
                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                       <?=$make_media_link; ?>
                                    </a>
                                  <div class="card-body">
                                    <h4 class="card-title">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                            <?= $aep->title;  ?>
                                      </a>
                                    </h4>
                                    <p class="card-text">
                                       <?php
                                            if(isset($aep->intervenant)){
                                       ?>
                                        <strong>
                                            <span style="font-size : medium"><?= $aep->intervenant; ?></span>
                                        </strong>
                                        <br/>
                                            <?php } ?>
                                        <strong>
                                            <?php 
                                            
                                           $string_day = date("d",strtotime($aep->date_start));
                                           $string_month = strftime("%b",strtotime($aep->date_start));
                                           $string_year = date("Y",strtotime($aep->date_start));
                                           $str_time = date("h:i a",strtotime($aep->date_start));
                                           echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;
                                           
                                          
                                            
                                            ?>
                                        </strong>
                                        <br/>
                                        <strong>
                                            <?= $aep->location;  ?>
                                        </strong>
                                    </p>
                                  </div>
                                </div>
                              </div>
                                
                            <?php 
                                }
                            ?>
<a  class="col-lg-12 button-jonn" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/list-event?pos=prog">

    <h5>
        <?= Yii::t('app','See more events'); ?>
        <!-- Need to be push to the right -->
    </h5>

</a>

   </div>

                    <!--  Fin des evenements --> 
                    
                    <!-- Features Section -->
                     <?php
                     if(isset($home_article->id)){
                     ?>
                     <div class="row">
                       <div class="col-lg-12">
                         <h2><?= $home_article->title; ?></h2>
                         <div class="atik"><?= $home_article->body; ?>
                             <img class="img-fluid rounded" src="<?= $home_article->images_id; ?>" alt=""></div>
                        </div>
                     </div>
                     <?php } ?>
                     
                    
                    </div>
            </div>

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->


                <div class="col-lg-3">


                    <!-- Page Content -->
                    <div class="container kote">

                      <h4 class="my-4"><?= Yii::t('app','The last news'); ?></h4>

                      <!-- Marketing Icons Section -->
                      <div class="row">
                          <?php
                              foreach($articles_latest as $al){
                                  $slug = "";
                                  $upm = "";
                                  if(isset(Menu::findOne($al->menus_id)->slug)){
                                      $slug = Menu::findOne($al->menus_id)->slug;
                                  }

                                  if(isset(Menu::findOne($al->menus_id)->parent_id)){
                                      $id_parent = Menu::findOne($al->menus_id)->parent_id;
                                      $upm = Menu::findOne($id_parent)->slug;
                                  }

                                  ?>
                          <div class="col-lg-11 kat">
                                  <div class="card h-100">
                                  <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>">
                                      <img class="card-img-top" src="<?= $al->images_id; ?>" alt=""></a>
                                  <!--  http://logipam.com/s/portal_assets/ckfinder/core/connector/php/images/images/Aide.png -->
                                  <h6 class="card-header">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>">
                                          <?= $al->title; ?>
                                      </a>
                                  </h6>
                                    <div class="card-body">
                                      <p class="card-text"><?= substr(strip_tags($al->body), 0,280); ?>.</p>
                                    </div>
                                    <div class="card-footer">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $slug; ?>&upm=<?= $upm ?>" class="btn btn-primary"><?= Yii::t('app','Learn more'); ?></a>
                                    </div>
                                  </div>
                         </div>
                                  <?php
                                      }
                                  ?>
                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="social" style="align-content: center">
                                      <a href="#"><i class="fab fa-facebook-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-twitter-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-youtube-square fa-3x"></i></a>
                                      <a href="#"><i class="fab fa-instagram fa-3x"></i></a>
                                  </div>
                              </div>
                          </div>
                          
                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="message-item" style="align-content: center">
                                      <ul class="list-unstyled">
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie">
                                                <?= Yii::t('app','Gallery Photos'); ?>
                                              </a>
                                          </li>
                                          <li>
                                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/contact">
                                                    <?= Yii::t('app','Contact Us'); ?>
                                                </a>
                                          </li>
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                                <?= Yii::t('app','Libray Catalog')?></a>
                                          </li>
                                          <li>
                                              <a href="">
                                                  <?= Yii::t('app','Bulletin'); ?>
                                              </a>
                                          </li>
                                          <li>
                                              <a href="#">
                                                  <?= Yii::t('app','Partners'); ?>
                                              </a>
                                          </li>
                                          
                                          <?php 
                                            foreach($menu_lateral as $ml){
                                                ?>
                                          <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $ml->slug; ?>"><?= $ml->name; ?></a>
                                          </li>
                                          <?php 
                                            }
                                          ?>
                                          
                                      </ul>
                                  </div>
                              </div>
                          </div>

                            </div>
                              
                            </div>


            </div>

</div>
