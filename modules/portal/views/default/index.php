<?php
use app\modules\cms\models\Article;
use app\modules\cms\models\Menu;
use app\modules\cms\models\Image;
use app\modules\cms\models\Calendar;
use app\modules\cms\models\Epingles;

$this->title = "Site web Pyepoudre";
$articles_latest = Article::findBySql("SELECT a.id, a.menus_id, a.body, a.slug, a.images_id, a.title FROM  articles a INNER JOIN menus m ON (a.menus_id = m.id) INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.name = 'Blog' AND a.is_publish = 1 ORDER BY a.id DESC LIMIT 3")->all();
$carousel_accueil = Image::findBySql("SELECT i.name, i.title FROM images i INNER JOIN components c ON (i.component_id = c.id) WHERE c.is_home = 1 AND c.is_publish = 1 AND i.is_publish = 1 ORDER BY i.id DESC")->all();
$home_article = Article::findOne(['is_home'=>1]);
$sql_menu_lat = "SELECT m.name, m.slug, m.id FROM menus m INNER JOIN layouts l ON (m.layouts_id = l.id) WHERE l.slug = 'menu-lateral' AND m.publish = 1 AND l.is_publish = 1";
$menu_lateral = Menu::findBySql($sql_menu_lat)->all();
$sql_event = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 0,3";
$sql_event1 = "SELECT * FROM calendars where YEAR(date_start) = YEAR(NOW()) ORDER BY ABS( DATEDIFF( date_start, NOW())), date_start DESC LIMIT 3,6";
$all_events_proche = Calendar::findBySql($sql_event)->all();
$all_events_proche_next = Calendar::findBySql($sql_event1)->all();
$count_events_proche_next = Calendar::findBySql($sql_event1)->count();
$count_events_proche = Calendar::findBySql($sql_event)->count();

setlocale(LC_TIME, "fr_FR.utf8");

$all_epingles = Epingles::findbySql("SELECT * FROM epingles WHERE is_publish = 1 ORDER BY id DESC LIMIT 5 ")->all();

$sql_last_album = "SELECT * FROM  images WHERE component_id = (SELECT id FROM components WHERE is_publish = 1 AND type = 'album' ORDER BY id LIMIT 0,1) LIMIT 0,6";

$last_six_images = Image::findBySql($sql_last_album)->all();


?>

<div class="container akey">
<div class="row">
          <div class="col-lg-9">
                <header>
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php
                            $k = 0;
                            foreach($carousel_accueil as $carou){
                        ?>
                      <li data-target="#carouselExampleIndicators" data-slide-to="<?= $k; ?>" class="<?php if($k==0) echo "active" ?>"></li>
                            <?php

                            $k++;
                            } ?>

                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                          $i = 0;
                          foreach($carousel_accueil as $ca){
                              ?>
                        <!-- Slide One - Set the background image for this slide in the line below -->
                      <div class="carousel-item <?php if($i==0) echo "active" ?>" style="background-image: url('<?= $ca->name; ?>')">
                        <div class="carousel-caption d-none d-md-block">
                          <h3><?= $ca->title; ?></h3>

                        </div>
                      </div>
                        <?php
                          $i++;
                          }
                        ?>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                </header>

                <div class="col-lg-12">
                    <div class="row">
                        &nbsp;
                    </div>
                    <!-- Place les evements ici  -->
                            <div class="row eve">
                            <div class="col-lg-12">

                                    <h2> Nos prochains événements </h2>

                                </div>

                            <?php
                                foreach($all_events_proche as $aep){
                                    $youtube_arr = explode("=",$aep->media);
                                    if(isset($youtube_arr[1])){
                                        $youtube_link = $youtube_arr[1];
                                        $youtube_id = str_replace("&list","",$youtube_link);
                                        $make_media_link = "<img src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg' class='card-img-top youtube-link' youtubeid='$youtube_id'/>";
                                         }else{
                                             $make_media_link = "<img  class='card-img-top' width='400px' heigth='300px' src='$aep->media'/>";
                                         }
                                    ?>
                                <div class="col-lg-4 col-sm-6 portfolio-item">
                                <div class="card h-100">
                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                       <?=$make_media_link; ?>
                                    </a>
                                  <div class="card-body">
                                    <h4 class="card-title">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aep->id?>&pos=prog">
                                            <?= $aep->title;  ?>
                                      </a>
                                    </h4>
                                    <p class="card-text">
                                       <?php
                                            if(isset($aep->intervenant)){
                                       ?>
                                        <strong>
                                            <span style="font-size : medium"><?= $aep->intervenant; ?></span>
                                        </strong>
                                        <br/>
                                            <?php } ?>
                                        <strong>
                                            <?php

                                           $string_day = date("d",strtotime($aep->date_start));
                                           $string_month = strftime("%b",strtotime($aep->date_start));
                                           $string_year = date("Y",strtotime($aep->date_start));
                                           $str_time = date("h:i a",strtotime($aep->date_start));
                                           echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;



                                            ?>
                                        </strong>
                                        <br/>
                                        <strong>
                                            <?= $aep->location;  ?>
                                        </strong>
                                    </p>
                                  </div>
                                </div>
                              </div>

                            <?php
                                }
                                ?>
                                 </div>

          <div id="next_zone" class="row eve">
                                <?php
                                $konte_si_gen_lot = 0;
                                 if($count_events_proche_next > 0){
                                     $konte_si_gen_lot++;
                                     foreach($all_events_proche_next as $aepn){
                                         $youtube_arr_1 = explode("=",$aepn->media);
                                    if(isset($youtube_arr_1[1])){
                                        $youtube_link_1 = $youtube_arr_1[1];
                                        $youtube_id_1 = str_replace("&list","",$youtube_link_1);
                                        $make_media_link_1 = "<img src='https://img.youtube.com/vi/$youtube_id_1/hqdefault.jpg' class='card-img-top youtube-link' youtubeid='$youtube_id_1'/>";
                                         }else{
                                             $make_media_link_1 = "<img  class='card-img-top' width='400px' heigth='300px' src='$aepn->media'/>";
                                         }
                                         ?>

                                <div class="col-lg-4 col-sm-6 portfolio-item next_batch_event">
                                <div class="card h-100">
                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aepn->id?>&pos=prog">
                                       <?=$make_media_link_1; ?>
                                    </a>
                                  <div class="card-body">
                                    <h4 class="card-title">
                                      <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/event-details?id=<?= $aepn->id?>&pos=prog">
                                            <?= $aepn->title;  ?>
                                      </a>
                                    </h4>
                                    <p class="card-text">
                                       <?php
                                            if(isset($aepn->intervenant)){
                                       ?>
                                        <strong>
                                            <span style="font-size : medium"><?= $aepn->intervenant; ?></span>
                                        </strong>
                                        <br/>
                                            <?php } ?>
                                        <strong>
                                            <?php

                                           $string_day = date("d",strtotime($aepn->date_start));
                                           $string_month = strftime("%b",strtotime($aepn->date_start));
                                           $string_year = date("Y",strtotime($aepn->date_start));
                                           $str_time = date("h:i a",strtotime($aepn->date_start));
                                           echo $string_day.' '.$string_month.' '.$string_year.' '.$str_time;



                                            ?>
                                        </strong>
                                        <br/>
                                        <strong>
                                            <?= $aepn->location;  ?>
                                        </strong>
                                    </p>
                                  </div>
                                </div>
                              </div>

                                <?php


                                     }

                                 }


                            ?>
                            <a  class="col-lg-12 button-jonn"  id="next_event" href="#next_zone">

                                <h5>
                                    <?= Yii::t('app','See more events'); ?><?= Yii::t('app',' ...'); ?>
                                    <!-- Need to be push to the right -->
                                </h5>

                            </a>
                            <a  class="col-lg-12 button-jonn"  id="last_event" href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/list-event?pos=prog">

                                <h5>
                                    <?= Yii::t('app','See more events'); ?><?= Yii::t('app',' ...'); ?>
                                    <!-- Need to be push to the right -->
                                </h5>

                            </a>
              </div>

<hr/>

                    <!-- Place les 3 dernieres actualites ici -->
                    <?php
                        if(infoGeneralConfig("show_block_actualites")==1){
                    ?>
                            <div class="row eve">
                            <div class="col-lg-12">

                                    <h2> Nouvelles en Bref </h2>

                              </div>
                             <?php
                                foreach($articles_latest as $al){
                                      $slug = "";
                                      $upm = "";
                                      if(isset(Menu::findOne($al->menus_id)->slug)){
                                          $slug = Menu::findOne($al->menus_id)->slug;
                                      }

                                      if(isset(Menu::findOne($al->menus_id)->parent_id)){
                                          $id_parent = Menu::findOne($al->menus_id)->parent_id;
                                          $upm = Menu::findOne($id_parent)->slug;
                                      }
                                    ?>
                                <div class="col-lg-4 col-sm-6 portfolio-item">
                                    <div class="card h-100">
                                        <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $al->slug; ?>&upm=<?= $upm ?>&ori=<?= $slug ?>">
                                            <img  class='card-img-top' width='400px' heigth='300px' src='<?= $al->images_id;?>'/>
                                        </a>
                                        <div class="card-body">
                                            <h4 class="card-title">
                                                <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/blog-details?slug=<?= $al->slug; ?>&upm=<?= $upm ?>&ori=<?= $slug ?>">
                                                    <?= $al->title;  ?>
                                                </a>
                                            </h4>
                                            <p class="card-text">
                                                <strong>
                                                <?php

                                                echo  substr(strip_tags($al->body), 0,300);
                                                ?>
                                                </strong>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <?php

                                }

                             ?>
                             <a  class="col-lg-12 button-jonn"   href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=NiKiMuskaChange">

                                 <h5>
                                         <?= Yii::t('app','Voir plus d\'actualit&eacute;s ...'); ?>
                 <!-- Need to be push to the right -->
                             </h5>

                             </a>
                                 <?php } ?>


                            </div>


                    <!-- Fin des 3 dernieres actualites ici -->

                    <!--  Fin des evenements -->

                    <!-- Features Section -->
                     <?php
                     if(isset($home_article->id)){
                     ?>
                     <div class="row">
                       <div class="col-lg-12">
                         <h2><?= $home_article->title; ?></h2>
                         <div class="atik">
                        <?php
                            $url = Yii::getAlias('@web')."/index.php/portal/default/article-details-by-id?id=".$home_article->id;
                            $title = $home_article->title;
                            echo  truncate($home_article->body,2000,$url,$title);
                                                                    ?>


                             <!--
                             <img class="img-fluid rounded" src="<?= $home_article->images_id; ?>" alt="">
                             -->
                         </div>

                        </div>
                     </div>
                     <?php } ?>


                    </div>
            </div>





<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->


                <div class="col-lg-3 kot">

                    <!-- Page Content -->
                    <div class="container kote">


                      <!-- Marketing Icons Section -->
                      <div class="row">
                          <div class="col-lg-11 kat">
                          <div class="card h-100">
                              <div class="message-item" style="align-content: center">


                                  <div class="social" style="align-content: center">
                                      <h4> <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                              &nbsp;&nbsp;<span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>  <?= Yii::t('app','Libray Catalog')?></a>
                                      </h4>
                                  </div>
                              </div>
                          </div>
                          </div>

                        <div class="col-lg-11 kat">
                          <div class="card h-100">
                                

                             <!--
                            <div class="col-lg-11 kat">
                                <div class="card h-100">

                                    <div class="col-lg-3 col-md-12 col-sm-12 img-list">

                                          <?php
                                              foreach($last_six_images as $lp){
                                                  ?>
                                              <a href="<?= $lp->url; ?>" target="_new">
                                                  <img class="tips" src="<?= $lp->name; ?>" alt="<?= $lp->title; ?>" title="<?= $lp->title; ?>">
                                              </a>

                                          <?php
                                              }
                                          ?>

                                  </div>
                                </div>
                            </div>
                              -->
                                    <ul class="list-unstyled">
                                        <li>
                                            <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/library">
                                              <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span>  <?= Yii::t('app','Bulletin'); ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/galerie">
                                              <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span><?= Yii::t('app','Gallery Photos'); ?>
                                            </a>
                                        </li>
                                        <?php
                                          foreach($menu_lateral as $ml){
                                              ?>
                                        <li>
                                            <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span><a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/article-details?slug=<?= $ml->slug; ?>"><?= $ml->name; ?></a>
                                        </li>
                                        <?php
                                          }
                                        ?>
                                        <li>
                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/portal/default/contact">
                                                  <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span><?= Yii::t('app','Contact Us'); ?>
                                              </a>
                                        </li>

                                        <!--
                                        <li>
                                            <a href="#">
                                                <?= Yii::t('app','Partners'); ?>
                                            </a>
                                        </li>
                                        -->


                                    </ul>
                                </div>
                            </div>
                        </div>


<div class="row">


                          <div class="col-lg-11 kat ping">



                                  <div class="card h-100">
<h4 class="my-4"> <i class="fas fa-thumbtack">&nbsp;&nbsp;&nbsp;&nbsp;<?= Yii::t('app','Epingl&eacute;s'); ?></i></h4>

                                      <div class="message-item" style="align-content: center">
                                          <ul class="list-unstyled" style="list-style-type:none; padding: 0px; ">
                                              <?php
                                                foreach($all_epingles as $ae){
                                                    ?>
                                              <li style="padding-bottom: 10px;">
                                                  <a href="<?= $ae->url_epingle;?>" target="_new">
                                                      <span class="fas fa-arrow-alt-circle-right" style="color: #FDCA1A">&nbsp; </span> <?= $ae->nom_epingle; ?>
                                                  </a>
                                              </li>
                                              <?php
                                                }
                                              ?>

                                          </ul>
                                      </div>
                                  </div>
                            </div>
</div>


  <!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++-->











<div class="row">

                          <div class="col-lg-11 kat">
                            <div class="card h-100">
                                  <div class="social" style="align-content: center">
                                      <a href="https://www.facebook.com/312pyepoudre" target="_blank"><i class="fab fa-facebook-square fa-3x"></i></a>
                                      <a href="https://twitter.com/312pyepoudre" target="_blank"><i class="fab fa-twitter-square fa-3x"></i></a>
                                      <a href="https://www.youtube.com/channel/UC4u6XGQcrpibrQ2idtLOaWA" target="_blank"><i class="fab fa-youtube-square fa-3x"></i></a>
                                      <a href="https://instagram.com/312pyepoudre" target="_blank"><i class="fab fa-instagram fa-3x"></i></a>
                                  </div>
                              </div>
                          </div>

                            </div>

                            </div>
</div>

            </div>

</div>


<?php
$script = <<< JS
   $(document).ready(function(){
            $('.next_batch_event').hide();
            $('#last_event').hide();
            $('#next_event').click(function(){
                    $('.next_batch_event').show();
                    $('#next_event').hide();
                    $('#last_event').show();
                });
        });


JS;
$this->registerJs($script);

?>
