<?php

namespace app\modules\portal\controllers;


use yii\web\Controller;
use app\modules\cms\models\Article; 
use app\modules\cms\models\Stat; 
use app\modules\cms\models\Menu; 
use app\modules\cms\models\Layout; 
use app\modules\cms\models\Component; 
use app\modules\cms\models\Image; 

/**
 * Default controller for the `portal` module
 */
class DefaultController extends Controller
{
    
    

    /**
     * Renders the index view for the module
     * @return string
     */
    
    public $layout = "/portal";
    
    public function actionIndex()
    {
            $stat = new Stat(); 
            $stat->article_visit = "home";
            $stat->menu_click = "home"; 
            $stat->adresse_ip = $this->getUserIpAddr(); 
            $stat->date_visit =  date('Y-m-d H:i:s');
            $stat->browser_tyoe = $this->getBrowser(); //$_SERVER['HTTP_USER_AGENT']; 
            $stat->device_type =  $this->getOS();
            $stat->save();
        return $this->render('index');
    }
    
    public function actionArticleDetails($slug){
        if(isset($slug)){
            
        $sql_1 = "SELECT a.id, a.slug, a.body, a.menus_id, a.title, a.create_date, a.images_id, a.components_id FROM articles a INNER JOIN menus m ON (m.id = a.menus_id) WHERE m.slug = '$slug' AND a.is_publish = 1 ORDER BY a.id DESC LIMIT 1"; 
        $all_article = Article::findBySql($sql_1)->one(); 
        if(isset($all_article)){
            if(isset(Menu::findOne($all_article->menus_id)->layouts_id)){
                $layout_id = Menu::findOne($all_article->menus_id)->layouts_id; 
                $layout_name = Layout::findOne($layout_id)->name; 
             }else{
                 $layout_name = ''; 
             } 
            
            if($layout_name !== 'Blog'){
                $stat = new Stat(); 
                $stat->article_visit = "$all_article->id";
                $stat->menu_click = "$all_article->menus_id"; 
                $stat->adresse_ip = $this->getUserIpAddr(); 
                $stat->date_visit =  date('Y-m-d H:i:s');
                $stat->browser_tyoe = $this->getBrowser(); //$_SERVER['HTTP_USER_AGENT']; 
                $stat->device_type =  $this->getOS();
                $stat->save();
            
                    return $this->render('article_details',['slug'=>$slug,'all_article'=>$all_article,'article_id'=>$all_article->id]); 
            }
            elseif($layout_name =='Blog'){
                    $sql_1 = "SELECT a.id, a.slug, a.body, a.menus_id, a.title, a.create_date, a.images_id FROM articles a INNER JOIN menus m ON (m.id = a.menus_id) WHERE m.slug = '$slug' AND a.is_publish = 1 ORDER BY a.id DESC"; 
                    $all_article = Article::findBySql($sql_1)->all(); 
                    $stat = new Stat(); 
                    $stat->article_visit = "blog";
                    $stat->menu_click = "$slug"; 
                    $stat->adresse_ip = $this->getUserIpAddr(); 
                    $stat->date_visit =  date('Y-m-d H:i:s');
                    $stat->browser_tyoe = $this->getBrowser(); //$_SERVER['HTTP_USER_AGENT']; 
                    $stat->device_type =  $this->getOS();
                    $stat->save();
                    return $this->render('blog',['slug'=>$slug,'all_article'=>$all_article]); 
            }
        }elseif(isset ($_GET['slug']) && $_GET['slug']=='NiKiMuskaChange'){
                    $sql_1 = "SELECT a.id, a.slug, a.body, a.menus_id, a.title, a.create_date, a.images_id FROM articles a INNER JOIN menus m ON (m.id = a.menus_id) INNER JOIN layouts l ON (a.layouts_id = l.id)  WHERE l.name = 'Blog' AND a.is_publish = 1 ORDER BY a.id DESC"; 
                    $all_article = Article::findBySql($sql_1)->all(); 
                    $stat = new Stat(); 
                    $stat->article_visit = "blog";
                    $stat->menu_click = "$slug"; 
                    $stat->adresse_ip = $this->getUserIpAddr(); 
                    $stat->date_visit =  date('Y-m-d H:i:s');
                    $stat->browser_tyoe = $this->getBrowser(); //$_SERVER['HTTP_USER_AGENT']; 
                    $stat->device_type =  $this->getOS();
                    $stat->save();
                    return $this->render('blog',['slug'=>$slug,'all_article'=>$all_article]); 
        }
        else{
            $stat = new Stat(); 
            $stat->article_visit = "error";
            $stat->menu_click = "$slug"; 
            $stat->adresse_ip = $this->getUserIpAddr(); 
            $stat->date_visit =  date('Y-m-d H:i:s');
            $stat->browser_tyoe = $this->getBrowser(); //$_SERVER['HTTP_USER_AGENT']; 
            $stat->device_type =  $this->getOS();
            $stat->save();
            return $this->render('error',['slug'=>$slug]);
        }
        }else{
            $slug = "page not found";
            return $this->render('error',['slug'=>$slug]);
        }
    }
    
    public function actionBlogDetails($slug){
        $one_blog = Article::findOne(['slug'=>$slug, 'is_publish'=>1]); 
        
        return $this->render('blog_details',['slug'=>$slug,'one_blog'=>$one_blog]);
    }
    
    public function actionGalerie(){
        $all_galerie = Component::find()->andWhere(['<>', 'name', "Logo"])->andWhere(['=','type','album'])->andWhere(['=','is_publish',1])->all();
        return $this->render('galerie',['all_galerie'=>$all_galerie,'all_image']); 
    }
    
    public function actionGalerieDetail($id){
        $all_image = Image::findAll(['component_id'=>$id,'is_publish'=>1]);
        return $this->render('galerie-details',['all_image'=>$all_image,'id'=>$id]);
    }
    
    public function actionListEvent(){
        return $this->render('list_event');
    }
    
    public function actionEventDetails($id){
        if(isset($id)){
                return $this->render('event_details',['id'=>$id]);
        }else{
            return $this->render('error',['slug'=>$id]);
        }
    }
    
    
    
    public function actionLibrary(){
        return $this->render('library');
    }
    
    public function actionContact(){
        return $this->render('contact');
    }
    
    function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    function getOS() { 
    $user_agent = $_SERVER['HTTP_USER_AGENT'];    
    //global $user_agent;

    $os_platform  = "Unknown OS Platform";

    $os_array     = array(
                          '/windows nt 10/i'      =>  'Windows 10',
                          '/windows nt 6.3/i'     =>  'Windows 8.1',
                          '/windows nt 6.2/i'     =>  'Windows 8',
                          '/windows nt 6.1/i'     =>  'Windows 7',
                          '/windows nt 6.0/i'     =>  'Windows Vista',
                          '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                          '/windows nt 5.1/i'     =>  'Windows XP',
                          '/windows xp/i'         =>  'Windows XP',
                          '/windows nt 5.0/i'     =>  'Windows 2000',
                          '/windows me/i'         =>  'Windows ME',
                          '/win98/i'              =>  'Windows 98',
                          '/win95/i'              =>  'Windows 95',
                          '/win16/i'              =>  'Windows 3.11',
                          '/macintosh|mac os x/i' =>  'Mac OS X',
                          '/mac_powerpc/i'        =>  'Mac OS 9',
                          '/linux/i'              =>  'Linux',
                          '/ubuntu/i'             =>  'Ubuntu',
                          '/iphone/i'             =>  'iPhone',
                          '/ipod/i'               =>  'iPod',
                          '/ipad/i'               =>  'iPad',
                          '/android/i'            =>  'Android',
                          '/blackberry/i'         =>  'BlackBerry',
                          '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
    }
    
    function getBrowser() {

   // global $user_agent;
    $user_agent = $_SERVER['HTTP_USER_AGENT']; 
    $browser        = "Unknown Browser";

    $browser_array = array(
                            '/msie/i'      => 'Internet Explorer',
                            '/firefox/i'   => 'Firefox',
                            '/safari/i'    => 'Safari',
                            '/chrome/i'    => 'Chrome',
                            '/edge/i'      => 'Edge',
                            '/opera/i'     => 'Opera',
                            '/netscape/i'  => 'Netscape',
                            '/maxthon/i'   => 'Maxthon',
                            '/konqueror/i' => 'Konqueror',
                            '/mobile/i'    => 'Handheld Browser'
                     );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}


public function actionArticleDetailsById($id){
    $all_article = Article::findOne($id); 
    return $this->render('article_details_by_id',['id'=>$id,'all_article'=>$all_article]);
}
    
    
     

    
}
