<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Ressource */

$this->title = Yii::t('app', 'Create Ressource');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ressources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ressource-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
