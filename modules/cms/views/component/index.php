<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\cms\models\Component; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cms\models\SearchComponent */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Components');
$this->params['breadcrumbs'][] = $this->title;

$components  = Component::find()->orderBy(['id'=>'DESC'])->all(); 
?>
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
      <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
</div>


<div class="table-responsive">
    <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= Yii::t('app','Name'); ?></th>
                <th><?= Yii::t('app','Type'); ?></th>
                <th><?= Yii::t('app','Publish'); ?></th>
                <th><?= Yii::t('app','Set Home'); ?></th>
                <th></th>
                <th></th>
                
            </tr>
        </thead>
        <tbody>
            <?php 
                foreach($components as $c){
                    ?>
            <tr>
                <td><?= $c->name; ?></td>
                <td><?= $c->type; ?></td>
                <td>
                    <?php
                    if(Yii::$app->user->can('cms-component-update')){
                        if ($c->is_publish == 1) {
                            $options = [
                            'title' => Yii::t('app', 'Un-publish'),
                            'aria-label' => Yii::t('app', 'Un-publish'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this component ?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/component/publish?id='.$c->id, $options);
                        }

                     elseif ($c->is_publish == 0) {
                        $options = [
                            'title' => Yii::t('app', 'Publish'),
                            'aria-label' => Yii::t('app', 'Publish'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to publish this component ?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/component/publish?id='.$c->id, $options); 
                     }
                    }
                    ?>
                </td>
                <td>
                    <?php
                    if(Yii::$app->user->can('cms-component-update')){
                        if ($c->is_home == 1) {
                            $options = [
                            'title' => Yii::t('app', 'Unset home'),
                            'aria-label' => Yii::t('app', 'Unset home'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to unset home this component ?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/component/set-home?id='.$c->id, $options);
                        }

                     elseif ($c->is_home == 0) {
                        $options = [
                            'title' => Yii::t('app', 'Set home'),
                            'aria-label' => Yii::t('app', 'Set home'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to set home this component ?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/component/set-home?id='.$c->id, $options); 
                     }
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if(Yii::$app->user->can('cms-component-update')){
                       echo Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/cms/component/update?id='.$c->id, [
                            'title' => Yii::t('app', 'Update'),
                            ]); 
                    }
                    ?>
                </td>
                <td>
                   <?php 
                    if(Yii::$app->user->can('cms-component-delete')){
                        echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/component/delete?id='.$c->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this component ?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                        ]); 
                    }
                   ?>
                </td>
                
            </tr>
            <?php 
                }
            ?>
        </tbody>
    </table>
</div>

<?php 
$src_txt = Yii::t('app','Search');
$layout_text = Yii::t('app','Components');
$script = <<< JS
   $(document).ready(function(){
            $('#zero_config').DataTable({
                pageLength: 50,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage $layout_text _START_ &agrave; _END_ sur _TOTAL_ $layout_text",
                    infoEmpty:      "Affichage $layout_text 0 &agrave; 0 sur 0 $layout_text",
                    infoFiltered:   "(sur _MAX_ $layout_text filtr&eacute; au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun $layout_text&agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Enabled Users List'},
                    {extend: 'pdf', title: 'Enabled Users List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });


JS;
$this->registerJs($script);

