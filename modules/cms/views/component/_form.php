<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cms\models\Menu;
use yii\helpers\ArrayHelper;
use app\modules\cms\models\Layouts; 
use app\modules\cms\models\Component; 

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Component */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin(); ?>
<div class="row mb-3">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?php 
            $type_items = ['album'=>'Album','carousel'=>'Carousel']; 
            echo $form->field($model,'type')->dropDownlist($type_items,['prompt'=>Yii::t('app','Select type')]);
        ?>
         
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-6">
        <?php 
            $layout_items = ArrayHelper::map(Layouts::findAll(['is_publish'=>1]),'id','name' );
            echo $form->field($model, 'layouts_id')->dropDownList($layout_items,['prompt'=>Yii::t('app','Select Layout')]);
        ?>
    </div>
    <div class="col-lg-6">
         <?= $form->field($model, 'is_publish')->checkbox(); ?>
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-6">
        <?= $form->field($model, 'description')->textarea(['rows' => 3]) ?>
    </div>
</div>
    
 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS
        $(document).ready(function(){
                $("#component-type").select2();
                $("#component-layouts_id").select2();
            }); 
JS;
$this->registerJs($script);        
?>