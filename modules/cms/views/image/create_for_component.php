<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Image */

$this->title = Yii::t('app', 'Create Image');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Images'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if(isset($_GET['id'])){
    $id = $_GET['id']; 
}
?>
<div class="image-create">

   
    <?= $this->render('_form_for_component', [
        'model' => $model,
        'id'=>$id,
    ]) ?>

</div>
