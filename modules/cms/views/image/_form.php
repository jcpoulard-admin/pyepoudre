<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cms\models\Layouts; 
use app\modules\cms\models\Component; 
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>
 <?php $form = ActiveForm::begin(); ?>

<div class="row mb-3">
    <div class="col-lg-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
         <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    </div>
</div>
<div class="row mb-3">
    <div class="col-lg-4">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class='col-lg-2'>
        <div class="form-group field-article-images_id">
            <label class="control-label" for="valider-image"> &nbsp;&nbsp;&nbsp;</label>
            <a id="valider-image" class="form-control btn btn-success" name="valider_image">
                <?= Yii::t('app','Valider image'); ?>
            </a>
            <div class="help-block"></div>
        </div>
        
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'url')->textInput(['maxlength'=>true]) ?>
    </div>
</div>
<div class="row mb-3" >
    <div class='col-lg-6' id='place-validation'>
        
    </div>
</div>
<div class="row mb-3">
    <div class="col-lg-6">
        <?php 
            $layout_items = ArrayHelper::map(Layouts::findAll(['is_publish'=>1]),'id','name' );
            echo $form->field($model, 'layouts_id')->dropDownList($layout_items,['prompt'=>Yii::t('app','Select Layout')]);
        ?>
        
    </div>
    <div class="col-lg-6">
        <?php 
            $components_items = ArrayHelper::map(Component::findAll(['is_publish'=>1]),'id','name' );
            echo $form->field($model, 'component_id')->dropDownList($components_items,['prompt'=>Yii::t('app','Select Component')]);
        ?>
        
    </div>
</div>
<div class="row mb-3">
    <div class="col-lg-6">
         <?= $form->field($model, 'is_publish')->checkbox();  ?>
    </div>
</div>
<input type='hidden' name='Image[image_invisible]' id='image_invisible'>

 <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php 
$script = <<< JS
    // Transforme un string en SLUG    
    function string_to_slug(str) {
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

          return str;
    }    
    $(document).ready(function(){
            $("#image-title").keyup(function(){
                var Text = $(this).val();
                
                $("#image-slug").val(string_to_slug(Text));        
            });
        
       
        $("#image-layouts_id").select2();
        $("#image-component_id").select2();
        
                    
        // Image chooser popup 
        var featured_image = document.getElementById( 'image-name' );
        //var description; 

        
        $('#image-name').click(function(){
                selectFileWithCKFinder( 'image-name' );
               
            });
        
        $('#valider-image').click(function(){
                var image_aficher = "<img src='"+featured_image.value+"' height='100' width='100'>";
                $('#place-validation').html(image_aficher);
                $('#image_invisible').val(featured_image.value); 
                $('#article-images_id').val(featured_image.value);
               
            });
       
        
        function selectFileWithCKFinder( elementId ) {
            CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
                                 output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
                } );
        }
    });

JS;
$this->registerJs($script);
// menu-parent_id
?>