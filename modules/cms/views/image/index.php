<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\cms\models\Image; 
use app\modules\cms\models\Component; 

/* @var $this yii\web\View */
/* @var $searchModel app\modules\cms\models\SearchImage */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photo gallery & Carrousel setup');
$this->params['breadcrumbs'][] = $this->title;
$all_components = Component::find()->all(); 

?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
      <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
</div>

<div class="container-fluid">


    <?php 
        foreach($all_components as $ac){
            ?>
    
           
                <h4 class="page-title"><?= $ac->name; ?></h4>
                <div class="row el-element-overlay">
                <?php 
                    $all_images = Image::findAll(['component_id'=>$ac->id]); 
                    if(isset($all_images)){
                    foreach($all_images as $ai){
                        ?>
                    <div class="col-lg-2 col-md-3">
                        <div class="card">
                            <div class="el-card-item" style="max-height: 125px;">
                                <div class="el-overlay-1" style="max-height: 100px;"> <img src="<?= $ai->name; ?>" alt="user" />
                                    <div class="el-overlay">
                                        <ul class="list-style-none el-info">
                                            <?php 
                                             if(Yii::$app->user->can('cms-image-update')){
                                                 
                                             ?>
                                            <li class="el-item"><a class="btn default btn-outline el-link" href="update?id=<?= $ai->id; ?>"><i class="fa fa-edit"></i></a></li>
                                             <?php } ?>
                                              <?php
                                                    if(Yii::$app->user->can('cms-image-update')){
                                                        ?>
                                            <li class="el-item">
                                              <?php 
                                                        if ($ai->is_publish == 1) {
                                                            $options = [
                                                            'title' => Yii::t('app', 'Un-publish'),
                                                            'aria-label' => Yii::t('app', 'Un-publish'),
                                                            'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this image ?'),
                                                           // 'data-method' => 'get',
                                                            'data-pjax' => '0',
                                                            'class'=>'btn default btn-outline el-link'    
                                                        ];
                                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/image/publish?id='.$ai->id, $options);
                                                        }

                                                     elseif ($ai->is_publish == 0) {
                                                        $options = [
                                                            'title' => Yii::t('app', 'Publish'),
                                                            'aria-label' => Yii::t('app', 'Publish'),
                                                            'data-confirm' => Yii::t('app', 'Are you sure you want to publish this image ?'),
                                                            //'data-method' => 'get',
                                                            'data-pjax' => '0',
                                                            'class'=>'btn default btn-outline el-link' 
                                                        ];
                                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/image/publish?id='.$ai->id, $options); 
                                                     }
                                                     ?>
                                                </li>
                                                <?php 
                                                    }
                                                ?>
                                            
                                             <?php 
                                                    if(Yii::$app->user->can('cms-image-delete')){
                                                        ?>
                                            
                                            <li class="el-item">
                                               <?php 
                                                        echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/image/delete?id='.$ai->id, [
                                                                    'title' => Yii::t('app', 'Delete'),
                                                                    'aria-label' => Yii::t('app', 'Delete'),
                                                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this image ?'),
                                                                    'data-method' => 'post',
                                                                    'data-pjax' => '0',
                                                                    'class'=>'btn default btn-outline el-link'
                                                        ]); 
                                                        ?>
                                            </li>
                                                <?php 
                                                    }
                                                ?>
                                           
                                        </ul>
                                    </div>
                                </div>
                                <div class="el-card-content">
                                    <h5 class="m-b-0"><?= $ai->title; ?></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    
               
                <?php 
                        }
                        ?>
                    <div class="col-lg-1 col-md-1">
                        <div class="card" style="max-height: 125px;">
                            <div class="el-card-item">
                                <div class="el-overlay-1">
                                    <a class="btn default btn-outline el-link" href="create-for-component?id=<?= $ac->id; ?>" >
                                        <i class="fa fa-plus fa-7x"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php 
                    }
                ?>
                </div> 
           
   
    <?php 
        }
    ?>
    
</div>


