<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cms\models\Menu;
use yii\helpers\ArrayHelper;
use app\modules\cms\models\Layouts; 
use app\modules\cms\models\Component; 

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>
<?php $form = ActiveForm::begin(); ?>

<div class="row mb-3">
    <div class="col-lg-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-6">
        
        <?php
            $menu_items = ArrayHelper::map(Menu::findAll(['is_parent'=>0,'publish'=>1]),'id','name' ); 
            echo $form->field($model, 'menus_id')->dropDownList($menu_items,['prompt'=>Yii::t('app','Select Menu')]);
        ?>
    </div>
    <div class="col-lg-6">
       
        <?php 
            $layout_items = ArrayHelper::map(Layouts::findAll(['is_publish'=>1]),'id','name' );
            echo $form->field($model, 'layouts_id')->dropDownList($layout_items,['prompt'=>Yii::t('app','Select Layout')]);
        ?>
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-4">
        <?= $form->field($model, 'images_id')->textInput() ?>
    </div>
    <div class='col-lg-2'>
        <div class="form-group field-article-images_id">
            <label class="control-label" for="valider-image"> &nbsp;&nbsp;&nbsp;</label>
            <a id="valider-image" class="form-control btn btn-success" name="valider_image">
                <?= Yii::t('app','Valider image'); ?>
            </a>
            <div class="help-block"></div>
        </div>
        
    </div>
    <div class="col-lg-6">
        <?php 
            $components_items = ArrayHelper::map(Component::findAll(['is_publish'=>1]),'id','name' );
            echo $form->field($model, 'components_id')->dropDownList($components_items,['prompt'=>Yii::t('app','Select Components')]);
        ?>
        <?php // $form->field($model, 'components_id')->textInput() ?>
    </div>
</div>
<div class="row mb-3" >
    <div class='col-lg-6' id='place-validation'>
        
    </div>
</div>
<div class='row mb-3'>
    <div class='col-lg-12'>
        <?= $form->field($model, 'is_publish')->checkbox() ?>
    </div>
</div>

<div class="row mb-3">
    <div class="col-lg-12">
        <?= $form->field($model, 'body')->textarea(); ?>
    </div>
</div>
<!--
<div class="row mb-3">
    <div class="col-lg-6">
        <?php // $form->field($model, 'is_publish')->checkbox() ?>
    </div>
</div>
-->

<input type='hidden' name='Article[image_invisible]' id='image_invisible'>
<div class="row mb-3">
    <div class="col-lg-6">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'id'=>'save-article']) ?>
    </div>
</div>

<div class="article-form">

    
    <div class="form-group">
        
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS
    // Transforme un string en SLUG    
    function string_to_slug(str) {
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

          return str;
    }    
    $(document).ready(function(){
      
            $("#article-title").keyup(function(){
                var Text = $(this).val();
                
                $("#article-slug").val(string_to_slug(Text));        
            });
        
        $("#article-menus_id").select2();
        $("#article-layouts_id").select2();
        $("#article-components_id").select2();
        /*
         ClassicEditor.create( document.querySelector( '#article-body' ), {
                ckfinder: {
                            //uploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                    },
                language: 'fr',
               
                
            })
		.then( editor => {
			window.editor = editor;
                        description = editor; 
		} )
		.catch( err => {
			console.error( err.stack );
		} );
         */
        
          
        
        // Image chooser popup 
        var featured_image = document.getElementById( 'article-images_id' );
        //var description; 

      /*
        featured_image.onclick = function() {
               selectFileWithCKFinder( 'article-images_id' );
          };
       */ 
        
        $('#article-images_id').click(function(){
                selectFileWithCKFinder( 'article-images_id' );
               // $('#article-images_id').val(featured_image.value);
               // $('#image_invisible').val(featured_image.value);
            });
        
        $('#valider-image').click(function(){
                $('#place-validation').html(featured_image.value);
                $('#image_invisible').val(featured_image.value); 
                $('#article-images_id').val(featured_image.value);
               
            });
        /*
        $('#article-images_id').change(function(){
                alert(featured_image.value);
                $('#image_invisible').val(featured_image.value);
            });
        */
        
        function selectFileWithCKFinder( elementId ) {
            CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
                                 output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
                } );
        }
        
        ClassicEditor
			.create( document.querySelector( '#article-body' ), {
				
				toolbar: {
					items: [
						'heading',
						'bold',
						'italic',
						'link',
						'bulletedList',
						'numberedList',
						'|',
						'indent',
						'outdent',
						'|',
						'CKFinder',
						'imageUpload',
						'blockQuote',
						'insertTable',
						'mediaEmbed',
						'undo',
						'redo',
						'codeBlock',
						'code',
						'fontBackgroundColor',
						'alignment',
						'fontColor',
						'fontSize',
						'fontFamily',
						'horizontalLine',
						'highlight',
						'MathType',
						'ChemType',
						'pageBreak',
						'strikethrough',
						'subscript',
						'superscript',
						'todoList',
						'underline'
					]
				},
				language: 'fr',
				image: {
					toolbar: [
						'imageTextAlternative',
						'imageStyle:full',
						'imageStyle:side'
					]
				},
				table: {
					contentToolbar: [
						'tableColumn',
						'tableRow',
						'mergeTableCells'
					]
				},
				licenseKey: 'VDFWJR903.HPV738LXR839',
				
			} )
			.then( editor => {
				window.editor = editor;
		
				
				
				
			} )
			.catch( error => {
				console.error( error );
			} );
        
        
    });

JS;
$this->registerJs($script);
// menu-parent_id
?>