<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calendar-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row mb-3">
        <div class="col-lg-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-1">
                         
             <span id="icon-youtube">
                 <i class="fas fa-toggle-off fa-4x" style="color: green"></i>
                    <br/>
                    <?= Yii::t('app','Youtube') ?>
             </span>

             <span id="icon-image">
                 <i class="fas fa-toggle-on fa-4x" style="color: green"></i>
                 <br/>
                        <?= Yii::t('app','Image') ?>
             </span>
                         
         </div>
        <div class="col-lg-5" id="youtube-place">
            <?= $form->field($model, 'media')->textInput(['maxlength' => true,]) ?>
            
        </div>
        <div class="col-lg-5" id="image-place">
            <?= $form->field($model, 'media')->textInput(['maxlength' => true,'id'=>'calendar-media-image']) ?>
        </div>
        
    </div>
    
    <div class="row mb-3">
        <div class="col-lg-6">
            <?= $form->field($model, 'date_start')->textInput() ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'date_end')->textInput() ?>
        </div>
    </div>
    
    <div class="row mb-3">
        <div class="col-lg-6">
            <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'description')->textArea(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-6">
            <?= $form->field($model, 'prix')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'intervenant')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    
 
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php 
$script = <<< JS
        $(document).ready(function(){
            $("#icon-image").hide();
            $("#image-place").hide();
        
            $("#icon-youtube").click(function(){
                    $("#icon-youtube").hide();
                    $("#icon-image").show();
                    $("#youtube-place").hide();
                    $("#image-place").show();
                  
                });
                $("#icon-image").click(function(){
                    $("#icon-youtube").show();
                    $("#icon-image").hide();
                    $("#youtube-place").show();
                    $("#image-place").hide();
                  
                });
                    
             // Image chooser popup 
        var featured_image = document.getElementById( 'calendar-media-image' );
        function selectFileWithCKFinder( elementId ) {
            CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
                                 output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
                } );
        }
        
        $('#calendar-media-image').click(function(){
                selectFileWithCKFinder( 'calendar-media-image' );
               
            });   
        
                $(function () {
                    $('#calendar-date_start').datetimepicker(
                            {
                         locale: 'fr', 
                         format: 'Y-MM-D H:mm',    
                         icons: {
                            time: "fas fa-clock",
                            date: "fa fa-calendar",
                            up: "fa fa-arrow-up",
                            down: "fa fa-arrow-down",
                            next: "fa fa-arrow-right",
                            previous : "fa fa-arrow-left",
                        }
                         });
        
                    $('#calendar-date_end').datetimepicker({
                        useCurrent: false, //Important! See issue #1075
                        locale: 'fr',
                        format: 'Y-MM-D H:mm',
                         icons: {
                            time: "fas fa-clock",
                            date: "fa fa-calendar",
                            up: "fa fa-arrow-up",
                            down: "fa fa-arrow-down",
                            next: "fa fa-arrow-right",
                            previous : "fa fa-arrow-left",
                        }
                    });
                    $("#calendar-date_start").on("dp.change", function (e) {
                        $('#calendar-date_end').data("DateTimePicker").minDate(e.date);
                    });
                    $("#calendar-date_end").on("dp.change", function (e) {
                        $('#calendar-date_start').data("DateTimePicker").maxDate(e.date);
                    });
                });
            });
JS;
$this->registerJs($script);