<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\cms\models\SearchCalendar */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Calendars');
$this->params['breadcrumbs'][] = $this->title;

?>

  <div class="row">
                    <div class="col-md-12">
                        <h5 class="card-title">Calender</h5>
                        <div class="card">
                            <div class="">
                                <div class="row">
                                    
                                    <div class="col-lg-9">
                                        <div class="card-body b-l calender-sidebar">
                                            <div id="calendar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
   </div>

 <!-- BEGIN MODAL -->
                <div class="modal none-border" id="my-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add Event</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Add Category -->
                <div class="modal fade none-border" id="add-new-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add</strong> a category</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">Category Name</label>
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Choose Category Color</label>
                                            <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                                <option value="success">Success</option>
                                                <option value="danger">Danger</option>
                                                <option value="info">Info</option>
                                                <option value="primary">Primary</option>
                                                <option value="warning">Warning</option>
                                                <option value="inverse">Inverse</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->
                
                
 <!-- Modal pour la visualisation des evenements  -->
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modalTitle" class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?= Yii::t('app','Close') ?></span></button>
                
            </div>
            <div id="modalBody" class="modal-body">
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Title'); ?></b> : </span>
                        <span id="title"></span>
                    </div>
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Location'); ?></b> : </span>
                        <span id="location"></span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Date start'); ?></b> : </span>
                        <span id='dateStart'></span>
                    </div>
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Date end'); ?></b> : </span>
                        <span id='dateEnd'></span><br/>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div id="media">
                             
                        </div>
                    </div>
                    
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div id="description"></div>
                    </div>
                    
                </div>
                <div class="row mb-3">
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Prix'); ?></b> : </span>
                        <span id="prix"></span>
                    </div>
                    <div class="col-lg-6">
                        <span><b><?= Yii::t('app','Intervenant'); ?></b> : </span>
                        <span id="intervenant"></span>
                    </div>
                    
                </div>
                
                
               
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <a id="eventUpdate"  data-id='' class='btn btn-warning' href="" title="<?= Yii::t('app','Update') ?>"> <i class="fa fa-edit"></i></a>
                
                <?= Html::a('<i class="fa fa-trash"></i>', [''], ['class' => 'btn btn-danger', 'id'=>'holidayDelete','title'=>Yii::t('app','Delete event'),
            'data'=>[
                'confirm' => Yii::t('app', 'Are you sure you want to delete this event ?'),
                'method' => 'post',
                ]
            ]) ?>
                
                
                </div>
        </div>
    </div>
</div>  
 
 <!-- Pour la creation d'element de calendrier  --> 
 <div id="create-calendar" class="modal fade">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
             <div class="modal-header">
                 <h4 class="modal-title">
                     <?= Yii::t('app','Create event on ');  ?><span id="titre-modal-creation"></span>
                 </h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only"><?= Yii::t('app','Close') ?></span></button>
                    
                </div>
             <div id="modal-body-creation" class="modal-body">
                 <div class="row mb-3">
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-title required">
                            <label class="control-label" for="calendar-title"><?= Yii::t('app','Event title'); ?></label>
                            <input type="text" id="calendar-title" class="form-control" name="calendar_title" maxlength="255" aria-required="true">

                         </div>
                     </div>
                     <div class="col-lg-2">
                         
                         <span id="icon-youtube">
                             <i class="fas fa-toggle-off fa-4x" style="color: green"></i>
                                <br/>
                                <?= Yii::t('app','Youtube') ?>
                         </span>
                         
                         <span id="icon-image">
                             <i class="fas fa-toggle-on fa-4x" style="color: green"></i>
                             <br/>
                                    <?= Yii::t('app','Image') ?>
                         </span>
                         
                     </div>
                     <div class="col-lg-4" id="youtube-place">
                        <div class="form-group field-calendar-media required">
                            <label class="control-label" for="calendar-media-youtube"><?= Yii::t('app','Media link');  ?></label>
                            <input type="text" id="calendar-media-youtube" placeholder="<?= Yii::t('app','Paste youtube url here'); ?>" class="form-control" name="calendar_media_youtube" maxlength="255" aria-required="true">
                            
                         </div>
                     </div>
                     <div class="col-lg-4" id="image-place">
                        <div class="form-group field-calendar-media required">
                            <label class="control-label" for="calendar-media-image"><?= Yii::t('app','Media link');  ?></label>
                            <input type="text" id="calendar-media-image" placeholder="<?= Yii::t('app','Click here to add image'); ?>" class="form-control" name="calendar_media_image" maxlength="255" aria-required="true">
                            
                         </div>
                     </div>
                 </div>
                 
                 <div class="row mb-3">
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-date_start required">
                            <label class="control-label" for="calendar-date_start"><?= Yii::t('app','Date start'); ?></label>
                            <input type="text" id="calendar-date_start" class="form-control" name="calendar_date_start" maxlength="255" aria-required="true">

                         </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-date_end required">
                            <label class="control-label" for="calendar-date_end"><?= Yii::t('app','Date end'); ?></label>
                            <input type="text" id="calendar-date_end" class="form-control" name="calendar_date_end" maxlength="255" aria-required="true">

                         </div>
                     </div>
                 </div>
                 
                 <div class="row mb-3">
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-location required">
                            <label class="control-label" for="calendar-location"><?= Yii::t('app','Location'); ?></label>
                            <input type="text" id="calendar-location" class="form-control" name="calendar_location" maxlength="255" aria-required="true">

                         </div>
                     </div>
                     <div class="col-lg-6">
                         <div class="form-group field-calendar-description required">
                            <label class="control-label" for="calendar-description"><?= Yii::t('app','Desciption'); ?></label>
                            <textarea id="calendar-description" class="form-control" name="calendar_description">
                                
                            </textarea>
                            
                         </div>
                     </div>
                 </div>
                 
                 <div class="row mb-3">
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-location required">
                            <label class="control-label" for="calendar-prix"><?= Yii::t('app','Price'); ?></label>
                            <input type="text" id="calendar-prix" class="form-control" name="calendar_prix" maxlength="255" aria-required="true">

                         </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group field-calendar-intervenant required">
                            <label class="control-label" for="calendar-intervenant"><?= Yii::t('app','Intervenant'); ?></label>
                            <input type="text" id="calendar-intervenant" class="form-control" name="calendar_intervenant" maxlength="255" aria-required="true">

                         </div>
                     </div>
                 </div>

             </div>
             <div id="test">
     
            </div>
             <div class="modal-footer">
                 <form method="POST">
                    <a id="btn-create-event"  data-id='' class='btn btn-success' href="#" title="<?= Yii::t('app','Save') ?>"> <?= Yii::t('app','Save'); ?></a>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?= Yii::t('app','Close'); ?></button>
                 </form>
              </div>
         </div>
     </div>
 </div>
 
 


<?php
 $str_json = "";
        foreach($events as $e){
         $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
        }
  $base_url = Yii::$app->request->BaseUrl; 
  $message_valide_empty = Yii::t('app',"Title and start date can't be empty !");
        
$script = <<< JS
    
    $(document).ready(function(){
        $("#image-place").hide(); 
        $("#icon-image").hide();
        
         // Image chooser popup 
        var featured_image = document.getElementById( 'calendar-media-image' );
        function selectFileWithCKFinder( elementId ) {
            CKFinder.popup( {
		chooseFiles: true,
		width: 800,
		height: 600,
		onInit: function( finder ) {
			finder.on( 'files:choose', function( evt ) {
				var file = evt.data.files.first();
				var output = document.getElementById( elementId );
                                 output.value = file.getUrl();
			} );

			finder.on( 'file:choose:resizedImage', function( evt ) {
				var output = document.getElementById( elementId );
				output.value = evt.data.resizedUrl;
			} );
		}
                } );
        }
        
        $('#calendar-media-image').click(function(){
                selectFileWithCKFinder( 'calendar-media-image' );
               
            });
        
         
        $('#calendar').fullCalendar({
                // put your options and callbacks here
        locale: 'fr',
        plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
        header: {
            left: 'prev,next today',
            center: 'title',
            //right: 'dayGridMonth,timeGridWeek,timeGridDay',
            right: 'month,agendaWeek'
        },
        editable: true,
        droppable: true,
        drop: function(info) {
            alert('DROP and drag');
            // is the "remove after drop" checkbox checked?
            if (checkbox.checked) {
                // if so, remove the element from the "Draggable Events" list
                info.draggedEl.parentNode.removeChild(info.draggedEl);
                }
            },
        
        events: [
                    $str_json
               ],
        eventClick : function(event, jsEvent, view){
             $('#modalTitle').html(event.title);
               $("#dateStart").html(moment(event.start).format('Do MMMM YYYY h:mm A'));
               $("#dateEnd").html(moment(event.end).format('Do MMMM YYYY h:mm A'));
               $("#title").html(event.title);
               $("#location").html(event.location);
               $("#media").html(event.media);
               $("#description").html(event.description);
               $("#prix").html(event.prix);
               $("#intervenant").html(event.intervenant);
               $('#holidayDelete').attr('href',event.url_delete);
               $('#eventUpdate').attr('target','_self');
               $('#eventUpdate').attr('href',event.url_update);
               $('#eventUpdate').attr('data-id',event.id);
               $('#fullCalModal').modal();
                   // Demo video 1
                $(".youtube-link").grtyoutube({
                        autoPlay:true,
                        theme: "dark"
                });
                // Demo video 2
                $(".youtube-link-dark").grtyoutube({
                        autoPlay:false,
                        theme: "light"
                });
            }, 
        eventDrop : function(event){
                    
              },
        dayClick : function(date, jsEvent, view, resourceObj) {
                $('#create-calendar').modal();
                var date_click = new Date(date.format().replace(/-/g, '\/')); 
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                var date_format = date_click.toLocaleDateString("fr-FR", options);
                $('#titre-modal-creation').html(date_format);
                $("#calendar-date_start").val(date.format('D-MM-Y'));
                $("#icon-youtube").click(function(){
                    $("#icon-youtube").hide();
                    $("#icon-image").show();
                    $("#youtube-place").hide();
                    $("#image-place").show();
                  
                });
                $("#icon-image").click(function(){
                    $("#icon-youtube").show();
                    $("#icon-image").hide();
                    $("#youtube-place").show();
                    $("#image-place").hide();
                  
                });      
                  
            }
        
        });
            
        
        
        $(function () {
            $('#calendar-date_start').datetimepicker(
                    {
                 locale: 'fr', 
                 format: 'D-MM-Y H:mm',    
                 icons: {
                    time: "fas fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    next: "fa fa-arrow-right",
                    previous : "fa fa-arrow-left",
                }
                 });
            $('#calendar-date_end').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                locale: 'fr',
                format: 'D-MM-Y H:mm',
                 icons: {
                    time: "fas fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    next: "fa fa-arrow-right",
                    previous : "fa fa-arrow-left",
                }
            });
            $("#calendar-date_start").on("dp.change", function (e) {
                $('#calendar-date_end').data("DateTimePicker").minDate(e.date);
            });
            $("#calendar-date_end").on("dp.change", function (e) {
                $('#calendar-date_start').data("DateTimePicker").maxDate(e.date);
            });
        });
        
        $("#btn-create-event").click(function(){
                var titre = $("#calendar-title").val(); 
                var date_debut = $("#calendar-date_start").val();
                var date_fin = $("#calendar-date_end").val();
                var loc = $("#calendar-location").val();
                var med_youtube = $("#calendar-media-youtube").val();
                var med_image = $("#calendar-media-image").val();
                var price = $("#calendar-prix").val();
                var artiste = $("#calendar-intervenant").val();
                var med = ''; 
                if(med_youtube !==''){
                        med = med_youtube; 
                    } else if(med_image !==''){
                            med = med_image;
                        }    
                var desc = $("#calendar-description").val(); 
                if(titre === '' || date_debut === ''){
                        alert("$message_valide_empty"); 
                    }else{
               $.get('$base_url/index.php/cms/calendar/create-event',{
                    title : titre,
                    date_start : date_debut,
                    date_end : date_fin,
                    location : loc, 
                    media : med,
                    description : desc,
                    prix : price,
                    intervenant:artiste
            
                    },function(data){
               $(location).attr('href','$base_url/index.php/cms/calendar/index')
               }); 
               }
        
            });
        
       });

JS;
$this->registerJs($script);
// menu-parent_id
?>
 
<!-- 

<input type="text" id="calendar-description" class="form-control" name="calendar_description" maxlength="1024" aria-required="true">

<?= Html::a('<i class="fa fa-trash"></i>', [''], ['class' => 'btn btn-danger', 'id'=>'holidayDelete','title'=>Yii::t('app','Delete holiday'),
                'data'=>[
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this holiday ?'),
                    'method' => 'post',
                    ]
                ]) ?>

-->

