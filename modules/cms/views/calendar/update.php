<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Calendar */

$this->title = Yii::t('app', 'Update Calendar: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Calendars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="row">
    <div class="col-lg-7">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
        <p>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index'], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this event ?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
</div> 

<div class="calendar-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
