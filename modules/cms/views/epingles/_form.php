<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Epingles */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin(); ?>
    <div class="row mb-3">
        <div class="col-lg-6">
            <?= $form->field($model, 'nom_epingle')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'url_epingle')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-lg-6">
             <?= $form->field($model, 'is_publish')->checkbox() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

