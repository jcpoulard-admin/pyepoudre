<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\cms\models\Epingles; 


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Epinglés');
$this->params['breadcrumbs'][] = $this->title;
$epingles = Epingles::find()->orderBy(['id'=>'DESC'])->all();  

?>
<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
      <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
</div>

<div class="table-responsive">
    <table id="zero_config" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th><?= Yii::t('app','Titre'); ?>.</th>
                
                <th><?= Yii::t('app','Url'); ?></th>
                <th><?= Yii::t('app','Publish');  ?></th>
                <th></th>
                <th></th>
                
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($epingles as $e){
                    ?>
            <tr>
                <td><?= $e->nom_epingle; ?></td>
               
                <td>
                    <a href="<?= $e->url_epingle ?>" target="_new"> <?= $e->url_epingle ?></a>
                </td>
                <td>
                    <?php
                    if(Yii::$app->user->can('cms-epingle-update')){
                        if ($e->is_publish == 1) {
                            $options = [
                            'title' => Yii::t('app', 'Un-publish'),
                            'aria-label' => Yii::t('app', 'Un-publish'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this pin?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/epingles/publish?id='.$e->id, $options);
                        }

                     elseif ($e->is_publish == 0) {
                        $options = [
                            'title' => Yii::t('app', 'Publish'),
                            'aria-label' => Yii::t('app', 'Publish'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to publish this pin?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/epingles/publish?id='.$e->id, $options); 
                     }
                    }
                    ?>
                </td>
                
                <td>
                    <?php 
                    if(Yii::$app->user->can('cms-epingle-update')){
                       echo Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/cms/epingles/update?id='.$e->id, [
                            'title' => Yii::t('app', 'Update'),
                            ]); 
                    }
                    ?>
                </td>
                <td>
                   <?php 
                    if(Yii::$app->user->can('cms-layout-delete')){
                        echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/epingles/delete?id='.$e->id, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this pin ?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                        ]); 
                    }
                   ?>
                </td>
            </tr>
            <?php 
                }
            ?>
        </tbody>
    </table>
</div>

<?php 

$src_txt = Yii::t('app','Search');
$script = <<< JS
   $(document).ready(function(){
            $('#zero_config').DataTable({
                pageLength: 50,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "$src_txt",
                    lengthMenu:    " _MENU_ ",
                    info:           "Affichage articles _START_ &agrave; _END_ sur _TOTAL_ articles",
                    infoEmpty:      "Affichage article 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(sur _MAX_ articles filtr&eacute; au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun article &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Enabled Users List'},
                    {extend: 'pdf', title: 'Enabled Users List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });


JS;
$this->registerJs($script);

?>
