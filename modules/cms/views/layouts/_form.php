<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Layouts */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-3">
        
    </div>
    <div class="layouts-form col-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_publish')->checkbox(); ?>
    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

    <div class="col-3">
        
    </div>
    
</div>

<?php 
$script = <<< JS
    // Transforme un string en SLUG    
    function string_to_slug(str) {
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

          return str;
    }    
    $(document).ready(function(){
            $("#layouts-name").keyup(function(){
                var Text = $(this).val();
                
                $("#layouts-slug").val(string_to_slug(Text));        
            });
      });

JS;
$this->registerJs($script);

?>