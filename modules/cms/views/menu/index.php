<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\cms\models\Menu; 
/* @var $this yii\web\View */
/* @var $searchModel app\modules\cms\models\SearchMenu */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
$all_parent_menu = Menu::findBySql("SELECT * FROM menus WHERE is_parent = 1 ORDER BY absolute_position ASC")->all();  //Menu::find(['is_parent'=>1])->orderBy(['absolute_position'=>'ASC'])->all();
$all_main_menu = Menu::findBySql("SELECT * FROM menus WHERE is_parent = 0 AND parent_id is NULL ORDER BY absolute_position ASC")->all();
?>

<div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
      <?= Html::a('<i class="fa fa-plus"></i> ', ['create'], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
</div>

<div class="row">
<div class="accordion" id="accordionExample">
    <?php 
        $i = 0;
        foreach($all_parent_menu as $apm){
            if($i==0){
                ?>
                <div class="card m-b-0">
                    <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                        <a  data-toggle="collapse" data-target="#collapse<?= $apm->id;?>" aria-expanded="true" aria-controls="collapseOne">
                            <i class="m-r-5 mdi  mdi-menu" aria-hidden="true"></i>
                            <span><?= $apm->name; ?></span>
                            
                        </a>
                          <?php
                                if(Yii::$app->user->can('cms-menu-update')){
                                    if ($apm->publish == 1) {
                                        $options = [
                                        'title' => Yii::t('app', 'Un-publish'),
                                        'aria-label' => Yii::t('app', 'Un-publish'),
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this menu ?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ];
                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$apm->id, $options);
                                    }

                                 elseif ($apm->publish == 0) {
                                    $options = [
                                        'title' => Yii::t('app', 'Publish'),
                                        'aria-label' => Yii::t('app', 'Publish'),
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to publish this menu?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                                    ];
                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$apm->id, $options); 
                                 }
                                }
                            ?>
                          <?php if(Yii::$app->user->can('cms-menu-update')){ ?>     
                                  <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/update?id='.$apm->id ?>" title="<?= Yii::t('app','Update')?>">
                                        <i class="fa fa-edit"></i>
                                  </a>
                          <?php } ?>
                          
                          <?php 
                            if(Yii::$app->user->can('cms-menu-delete')){
                                echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/menu/delete?id='.$apm->id, [
                                            'title' => Yii::t('app', 'Delete'),
                                            'aria-label' => Yii::t('app', 'Delete'),
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this menu ?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                ]); 
                            }
                         ?>
                      </h5>
                    </div>
                    <div id="collapse<?= $apm->id;?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                          
                          <?php 
                               if(isset($apm->id)){
                                    
                                    $all_sub_menu = Menu::findBySql("SELECT * FROM menus WHERE parent_id =  $apm->id ORDER BY relative_position ASC")->all(); 
                                    $konte = 0;
                                    foreach($all_sub_menu as $asm){
                                        
                                        ?>
                              <div class="d-flex flex-row comment-row m-t-0">
                                    
                                    <div class="comment-text w-100">
                                        <h6 class="font-medium"><?= $asm->name; ?></h6>
                                        
                                        <div class="comment-footer">
                                            
                                            
                                               <?php if(Yii::$app->user->can('cms-menu-update')){ ?> 
                                                        <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/deplase-up?id='.$asm->id ?>&konte=<?= $asm->relative_position;?>" title="<?= Yii::t('app','Move up')?>">
                                                            <i class="fa fa-arrow-up"></i>
                                                        </a>
                                                        <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/deplase-down?id='.$asm->id ?>&konte=<?= $asm->relative_position;?>" title="<?= Yii::t('app','Move down')?>">
                                                            <i class="fa fa-arrow-down"></i>
                                                        </a>
                                                        <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/update?id='.$asm->id ?>" title="<?= Yii::t('app','Update')?>">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                            
                                            <?php } ?>
                                            <?php
                                                if(Yii::$app->user->can('cms-menu-update')){
                                                    if ($asm->publish == 1) {
                                                        $options = [
                                                        'title' => Yii::t('app', 'Un-publish'),
                                                        'aria-label' => Yii::t('app', 'Un-publish'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this menu ?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                                    ];
                                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$asm->id, $options);
                                                    }

                                                 elseif ($asm->publish == 0) {
                                                    $options = [
                                                        'title' => Yii::t('app', 'Publish'),
                                                        'aria-label' => Yii::t('app', 'Publish'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure you want to publish this menu?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                                    ];
                                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$asm->id, $options); 
                                                 }
                                                }
                                            ?>
                                           <?php 
                                                if(Yii::$app->user->can('cms-menu-delete')){
                                                    echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/menu/delete?id='.$asm->id, [
                                                                'title' => Yii::t('app', 'Delete'),
                                                                'aria-label' => Yii::t('app', 'Delete'),
                                                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this menu ?'),
                                                                'data-method' => 'post',
                                                                'data-pjax' => '0',
                                                    ]); 
                                                }
                                            ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                  
                                                    
                              <?php 
                                    
                              $konte++;
                                 }
                                    ?>
                          
                               
                                
                           
                          <?php
                                }
                                
                          ?>
                      
                      </div>
                    </div>
                </div>
    
    <?php 
            
                
            }else{
                ?>
                <div class="card m-b-0 border-top">
                                <div class="card-header" id="headingTwo">
                                  <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-target="#collapse<?= $apm->id;?>" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="m-r-5 mdi  mdi-menu" aria-hidden="true"></i>
                                        <span><?= $apm->name;?></span>
                                    </a>
                                    <?php
                                    if(Yii::$app->user->can('cms-menu-update')){
                                        if ($apm->publish == 1) {
                                            $options = [
                                            'title' => Yii::t('app', 'Un-publish'),
                                            'aria-label' => Yii::t('app', 'Un-publish'),
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this menu ?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ];
                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$apm->id, $options);
                                        }

                                     elseif ($apm->publish == 0) {
                                        $options = [
                                            'title' => Yii::t('app', 'Publish'),
                                            'aria-label' => Yii::t('app', 'Publish'),
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to publish this menu?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ];
                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$apm->id, $options); 
                                     }
                                    }
                                    ?>
                                 <?php if(Yii::$app->user->can('cms-menu-update')){ ?>     
                                  <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/update?id='.$apm->id ?>" title="<?= Yii::t('app','Update')?>">
                                        <i class="fa fa-edit"></i>
                                  </a>
                                 <?php } ?>
                                      
                                 <?php 
                                    if(Yii::$app->user->can('cms-menu-delete')){
                                        echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/menu/delete?id='.$apm->id, [
                                                    'title' => Yii::t('app', 'Delete'),
                                                    'aria-label' => Yii::t('app', 'Delete'),
                                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this menu ?'),
                                                    'data-method' => 'post',
                                                    'data-pjax' => '0',
                                        ]); 
                                    }
                            ?>     
                                  </h5>
                                </div>
                                <div id="collapse<?= $apm->id;?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                  <div class="card-body">
                                      <?php 
                               if(isset($apm->id)){
                                    
                                    $all_sub_menu = Menu::findBySql("SELECT * FROM menus WHERE parent_id =  $apm->id ORDER BY relative_position ASC")->all(); 
                                    
                                    foreach($all_sub_menu as $asm){
                                        
                                        ?>
                              <div class="d-flex flex-row comment-row m-t-0">
                                    
                                    <div class="comment-text w-100">
                                        <h6 class="font-medium"><?= $asm->name; ?></h6>
                                        
                                        <div class="comment-footer">
                                            
                                            
                                               <?php if(Yii::$app->user->can('cms-menu-update')){ ?> 
                                                    <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/deplase-up?id='.$asm->id ?>&konte=<?= $asm->relative_position;?>" title="<?= Yii::t('app','Move up')?>">
                                                            <i class="fa fa-arrow-up"></i>
                                                    </a>
                                                     <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/deplase-down?id='.$asm->id ?>&konte=<?= $asm->relative_position;?>" title="<?= Yii::t('app','Move down')?>">
                                                            <i class="fa fa-arrow-down"></i>
                                                     </a>
                                                    <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/update?id='.$asm->id ?>" title="<?= Yii::t('app','Update')?>">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                            <?php } ?>
                                            <?php
                                                if(Yii::$app->user->can('cms-menu-update')){
                                                    if ($asm->publish == 1) {
                                                        $options = [
                                                        'title' => Yii::t('app', 'Un-publish'),
                                                        'aria-label' => Yii::t('app', 'Un-publish'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this menu ?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                                    ];
                                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$asm->id, $options);
                                                    }

                                                 elseif ($asm->publish == 0) {
                                                    $options = [
                                                        'title' => Yii::t('app', 'Publish'),
                                                        'aria-label' => Yii::t('app', 'Publish'),
                                                        'data-confirm' => Yii::t('app', 'Are you sure you want to publish this menu?'),
                                                        'data-method' => 'post',
                                                        'data-pjax' => '0',
                                                    ];
                                                    echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$asm->id, $options); 
                                                 }
                                                }
                                            ?>
                                           <?php 
                                                if(Yii::$app->user->can('cms-menu-delete')){
                                                    echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/menu/delete?id='.$asm->id, [
                                                                'title' => Yii::t('app', 'Delete'),
                                                                'aria-label' => Yii::t('app', 'Delete'),
                                                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this menu ?'),
                                                                'data-method' => 'post',
                                                                'data-pjax' => '0',
                                                    ]); 
                                                }
                                            ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                  
                                                    
                              <?php 
                                    }
                                    ?>
                          
                               
                                
                           
                          <?php
                                }
                                
                          ?>
                      
                      
                                    </div>
                                </div>
                            </div>
    <?php
            }
            ?>
    
    <?php 
        $i++;
        }
    foreach($all_main_menu as $amm){
        ?>
   <div class="card m-b-0"> 
        <div class="card-header" id="headingOne">
          <h5 class="mb-0">
                
                    <i class="m-r-5 mdi  mdi-menu" aria-hidden="true"></i>
                    <span><?= $amm->name; ?></span>
                    <?php
                                    if(Yii::$app->user->can('cms-menu-update')){
                                        if ($amm->publish == 1) {
                                            $options = [
                                            'title' => Yii::t('app', 'Un-publish'),
                                            'aria-label' => Yii::t('app', 'Un-publish'),
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to un-publish this menu ?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ];
                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$amm->id, $options);
                                        }

                                     elseif ($amm->publish == 0) {
                                        $options = [
                                            'title' => Yii::t('app', 'Publish'),
                                            'aria-label' => Yii::t('app', 'Publish'),
                                            'data-confirm' => Yii::t('app', 'Are you sure you want to publish this menu?'),
                                            'data-method' => 'post',
                                            'data-pjax' => '0',
                                        ];
                                        echo '&nbsp'.Html::a('<span class="fa fa-toggle-on" style="color:red;"></span>', Yii::getAlias('@web').'/index.php/cms/menu/publish?id='.$amm->id, $options); 
                                     }
                                    }
                                    ?>
                                 <?php if(Yii::$app->user->can('cms-menu-update')){ ?>     
                    
                    <a href="<?=Yii::getAlias('@web').'/index.php/cms/menu/update?id='.$amm->id ?>" title="<?= Yii::t('app','Update')?>">
                            <i class="fa fa-edit"></i>
                        </a>
                   
                                 <?php } ?>
                    <?php 
                        if(Yii::$app->user->can('cms-menu-delete')){
                            echo Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/cms/menu/delete?id='.$amm->id, [
                                        'title' => Yii::t('app', 'Delete'),
                                        'aria-label' => Yii::t('app', 'Delete'),
                                        'data-confirm' => Yii::t('app', 'Are you sure you want to delete this menu ?'),
                                        'data-method' => 'post',
                                        'data-pjax' => '0',
                            ]); 
                        }
                     ?>
                
            </h5>
        </div>
   </div>
    <?php 
        
    }
    ?>
                         
</div>

</div>
