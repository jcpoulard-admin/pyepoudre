<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cms\models\Menu; 
use yii\helpers\ArrayHelper;
use app\modules\cms\models\Layouts; 

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
    <?php 
        $layout_items = ArrayHelper::map(Layouts::findAll(['is_publish'=>1]),'id','name' );
        echo $form->field($model, 'layouts_id')->dropDownList($layout_items,['prompt'=>Yii::t('app','Select Layout')]);
    ?>

    <?= $form->field($model, 'is_parent')->checkbox(); ?>

    <?php // $form->field($model, 'parent_id')->textInput() ?>
    <?php
    $menu_items = ArrayHelper::map(Menu::findAll(['is_parent'=>1,'publish'=>1]),'id','name' ); 
    echo $form->field($model, 'parent_id')->dropDownList($menu_items,['prompt'=>Yii::t('app','Select Parent Menu')]);
    
    ?>

    

    <?php  
    
    $max_menu = Menu::find(['parent_id'=>NULL,'publish'=>1])->count(); 
    $absolute_menu_item = [];
    for($i=1; $i<=$max_menu;$i++){
        $absolute_menu_item[$i] = $i;
    }
    echo $form->field($model, 'absolute_position')->dropDownList($absolute_menu_item,['prompt'=>Yii::t('app','Select absolute position')]); 
           // $form->field($model, 'absolute_position')->textInput() 
            ?>

    <?php // $form->field($model, 'relative_position')->textInput() ?>
    <?= $form->field($model, 'is_home')->checkbox(); ?>
    
    <?= $form->field($model, 'publish')->checkbox(); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$script = <<< JS
    // Transforme un string en SLUG    
    function string_to_slug(str) {
          str = str.replace(/^\s+|\s+$/g, ''); // trim
          str = str.toLowerCase();

          // remove accents, swap ñ for n, etc
          var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
          var to   = "aaaaeeeeiiiioooouuuunc------";
          for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
          }

          str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

          return str;
    }    
    $(document).ready(function(){
            $("#menu-name").keyup(function(){
                var Text = $(this).val();
                
                $("#menu-slug").val(string_to_slug(Text));        
            });
        
           $("#menu-parent_id").select2();
           $("#menu-layouts_id").select2();
           $("#menu-absolute_position").select2();
           
            $('#menu-is_parent').change(function(){
                   if($(this).prop('checked')) {
                        $('.field-menu-parent_id').hide();
                        // $('.field-menu-relative_position').hide();
                    } else {
                        $('.field-menu-parent_id').show();
                        // $('.field-menu-relative_position').show();
                    }
        
                    
            });
        
            if($('#menu-is_parent').prop('checked')) {
                        $('.field-menu-parent_id').hide();
                    } else {
                        $('.field-menu-parent_id').show();
                    }
        /*
         if($('#menu-is_parent').prop('checked')) {
                        $('.field-menu-relative_position').hide();
                    } else {
                        $('.field-menu-relative_position').show();
                    }
        */
            
      });

JS;
$this->registerJs($script);
// menu-parent_id
?>