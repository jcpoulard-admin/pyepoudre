<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\cms\models\Menu */

$this->title = Yii::t('app', 'Update Menu: {name}', [
    'name' => $model->name,
]);

?>

<div class="row">
    <div class="col-lg-7">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <div class="col-lg-5">
        <p>
        <?= Html::a('<i class="fa fa-plus"></i> '.Yii::t('app', 'Add'), ['create', ], ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::a('<i class="fa fa-tasks"></i> '.Yii::t('app', 'List'), ['index', 'id' => $model->name,], ['class' => 'btn btn-info btn-sm']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> '.Yii::t('app', 'Delete'), ['delete', 'id' => $model->name], [
            'class' => 'btn btn-danger btn-sm',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this menu?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
</div> 

<div class="menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
