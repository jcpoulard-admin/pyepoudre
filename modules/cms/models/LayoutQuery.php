<?php

namespace app\modules\cms\models;

/**
 * This is the ActiveQuery class for [[Layout]].
 *
 * @see Layout
 */
class LayoutQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Layout[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Layout|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
