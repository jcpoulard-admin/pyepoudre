<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "manage_ressources".
 *
 * @property int $id
 * @property int $ressources_id
 * @property string|null $date_allocation
 * @property string|null $end_allocation
 * @property string|null $description
 * @property string|null $allocate_to
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $date_update
 * @property string|null $update_by
 *
 * @property Ressource $ressources
 */
class ManageRessource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manage_ressources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ressources_id'], 'required'],
            [['ressources_id'], 'integer'],
            [['date_allocation', 'end_allocation', 'date_create', 'date_update'], 'safe'],
            [['description'], 'string'],
            [['allocate_to'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['ressources_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ressource::className(), 'targetAttribute' => ['ressources_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ressources_id' => Yii::t('app', 'Ressources ID'),
            'date_allocation' => Yii::t('app', 'Date Allocation'),
            'end_allocation' => Yii::t('app', 'End Allocation'),
            'description' => Yii::t('app', 'Description'),
            'allocate_to' => Yii::t('app', 'Allocate To'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRessources()
    {
        return $this->hasOne(Ressource::className(), ['id' => 'ressources_id']);
    }

    /**
     * {@inheritdoc}
     * @return ManageRessourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ManageRessourceQuery(get_called_class());
    }
}
