<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string|null $value
 * @property string|null $description
 * @property string|null $category
 * @property string|null $create_date
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'label'], 'required'],
            [['value', 'description'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['name', 'label'], 'string', 'max' => 255],
            [['category', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'label' => Yii::t('app', 'Label'),
            'value' => Yii::t('app', 'Value'),
            'description' => Yii::t('app', 'Description'),
            'category' => Yii::t('app', 'Category'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ConfigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfigQuery(get_called_class());
    }
}
