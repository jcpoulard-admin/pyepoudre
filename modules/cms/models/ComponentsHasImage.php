<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "components_has_images".
 *
 * @property int $components_id
 * @property int $images_id
 * @property string|null $create_date
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 * @property int|null $is_publish
 *
 * @property Component $components
 * @property Image $images
 */
class ComponentsHasImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'components_has_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['components_id', 'images_id'], 'required'],
            [['components_id', 'images_id', 'is_publish'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['components_id', 'images_id'], 'unique', 'targetAttribute' => ['components_id', 'images_id']],
            [['components_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['components_id' => 'id']],
            [['images_id'], 'exist', 'skipOnError' => true, 'targetClass' => Image::className(), 'targetAttribute' => ['images_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'components_id' => Yii::t('app', 'Components ID'),
            'images_id' => Yii::t('app', 'Images ID'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
            'is_publish' => Yii::t('app', 'Is Publish'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasOne(Component::className(), ['id' => 'components_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasOne(Image::className(), ['id' => 'images_id']);
    }

    /**
     * {@inheritdoc}
     * @return ComponentsHasImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ComponentsHasImageQuery(get_called_class());
    }
}
