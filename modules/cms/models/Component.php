<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "components".
 *
 * @property int $id
 * @property string|null $name
 * @property string $type
 * @property int|null $layouts_id
 * @property string|null $description
 * @property string|null $create_date
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 * @property int|null $is_publish
 *
 * @property Article[] $articles
 * @property Layout $layouts
 * @property ComponentsHasImage[] $componentsHasImages
 * @property Image[] $images
 */
class Component extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'components';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['layouts_id', 'is_publish'], 'integer'],
            [['description'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['type', 'create_by', 'update_by'], 'string', 'max' => 45],
            [['name'], 'unique'],
            [['layouts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::className(), 'targetAttribute' => ['layouts_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'layouts_id' => Yii::t('app', 'Layouts ID'),
            'description' => Yii::t('app', 'Description'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
            'is_publish' => Yii::t('app', 'Is Publish'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['components_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLayouts()
    {
        return $this->hasOne(Layout::className(), ['id' => 'layouts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponentsHasImages()
    {
        return $this->hasMany(ComponentsHasImage::className(), ['components_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'images_id'])->viaTable('components_has_images', ['components_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ComponentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ComponentQuery(get_called_class());
    }
}
