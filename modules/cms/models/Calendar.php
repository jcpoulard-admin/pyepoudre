<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "calendars".
 *
 * @property int $id
 * @property string $title
 * @property string $date_start
 * @property string|null $date_end
 * @property string|null $location
 * @property string|null $media
 * @property string|null $description
 * @property string|null $prix
 * @property string|null $intervenant
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $date_update
 * @property string|null $update_by
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'date_start'], 'required'],
            [['date_start', 'date_end', 'date_create', 'date_update'], 'safe'],
            [['description'], 'string'],
            [['title', 'location', 'media', 'prix', 'intervenant', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'location' => Yii::t('app', 'Location'),
            'media' => Yii::t('app', 'Media'),
            'description' => Yii::t('app', 'Description'),
            'prix' => Yii::t('app', 'Prix'),
            'intervenant' => Yii::t('app', 'Intervenant'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
