<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "cms_index".
 *
 * @property int $id
 * @property int|null $element_id
 * @property string|null $element_slug
 * @property string|null $cms_indexcol
 * @property string $element_type
 * @property string|null $index_value
 * @property string|null $date_create
 * @property string|null $index_keywords
 */
class CmsIndex extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cms_index';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['element_id'], 'integer'],
            [['element_type'], 'required'],
            [['index_value', 'index_keywords'], 'string'],
            [['date_create'], 'safe'],
            [['element_slug', 'cms_indexcol'], 'string', 'max' => 255],
            [['element_type'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'element_id' => Yii::t('app', 'Element ID'),
            'element_slug' => Yii::t('app', 'Element Slug'),
            'cms_indexcol' => Yii::t('app', 'Cms Indexcol'),
            'element_type' => Yii::t('app', 'Element Type'),
            'index_value' => Yii::t('app', 'Index Value'),
            'date_create' => Yii::t('app', 'Date Create'),
            'index_keywords' => Yii::t('app', 'Index Keywords'),
        ];
    }
}
