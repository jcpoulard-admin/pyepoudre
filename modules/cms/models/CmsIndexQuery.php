<?php

namespace app\modules\cms\models;

/**
 * This is the ActiveQuery class for [[CmsIndex]].
 *
 * @see CmsIndex
 */
class CmsIndexQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CmsIndex[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CmsIndex|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
