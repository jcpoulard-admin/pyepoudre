<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "menus".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int|null $is_parent
 * @property int|null $parent_id
 * @property int|null $layouts_id
 * @property int|null $absolute_position
 * @property int|null $relative_position
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $date_update
 * @property string|null $update_by
 * @property int|null $publish
 *
 * @property Article[] $articles
 * @property Layout $layouts
 * @property Menu $parent
 * @property Menu[] $menus
 */
class Menu extends \yii\db\ActiveRecord
{
    
    public $max_position; 
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['is_parent', 'parent_id', 'layouts_id', 'absolute_position', 'relative_position', 'publish'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['name', 'slug', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
            [['layouts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::className(), 'targetAttribute' => ['layouts_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'is_parent' => Yii::t('app', 'Is Parent'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'layouts_id' => Yii::t('app', 'Layouts ID'),
            'absolute_position' => Yii::t('app', 'Absolute Position'),
            'relative_position' => Yii::t('app', 'Relative Position'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
            'publish' => Yii::t('app', 'Publish'),
            'is_home'=>Yii::t('app','Is Home'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['menus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLayouts()
    {
        return $this->hasOne(Layout::className(), ['id' => 'layouts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return MenuQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuQuery(get_called_class());
    }
}
