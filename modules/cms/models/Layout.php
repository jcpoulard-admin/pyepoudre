<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "layouts".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $date_update
 * @property string|null $update_by
 * @property int|null $is_publish
 *
 * @property Article[] $articles
 * @property Component[] $components
 * @property Image[] $images
 * @property Menu[] $menus
 */
class Layout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'layouts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['date_create', 'date_update'], 'safe'],
            [['is_publish'], 'integer'],
            [['name', 'slug', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
            'is_publish' => Yii::t('app', 'Is Publish'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['layouts_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasMany(Component::className(), ['layouts_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['layouts_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['layouts_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return LayoutQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LayoutQuery(get_called_class());
    }
}
