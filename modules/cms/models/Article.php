<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $body
 * @property int|null $menus_id
 * @property int|null $layouts_id
 * @property string|null $images_id
 * @property int|null $components_id
 * @property int|null $is_publish
 * @property int|null $is_home
 * @property string|null $create_date
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 *
 * @property Component $components
 * @property Layout $layouts
 * @property Menu $menus
 */
class Article extends \yii\db\ActiveRecord
{
    
    
     public $image_invisible; 
     
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['body', 'images_id'], 'string'],
            [['menus_id', 'layouts_id', 'components_id', 'is_publish', 'is_home'], 'integer'],
            [['create_date', 'update_date'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['create_by', 'update_by'], 'string', 'max' => 45],
            [['components_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['components_id' => 'id']],
            [['layouts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::className(), 'targetAttribute' => ['layouts_id' => 'id']],
            [['menus_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menus_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'body' => Yii::t('app', 'Body'),
            'menus_id' => Yii::t('app', 'Menus ID'),
            'layouts_id' => Yii::t('app', 'Layouts ID'),
            'images_id' => Yii::t('app', 'Images ID'),
            'components_id' => Yii::t('app', 'Components ID'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'is_home' => Yii::t('app', 'Is Home'),
            'create_date' => Yii::t('app', 'Create Date'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * Gets query for [[Components]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasOne(Component::className(), ['id' => 'components_id']);
    }

    /**
     * Gets query for [[Layouts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLayouts()
    {
        return $this->hasOne(Layout::className(), ['id' => 'layouts_id']);
    }

    /**
     * Gets query for [[Menus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menus_id']);
    }
}
