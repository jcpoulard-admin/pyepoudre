<?php

namespace app\modules\cms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cms\models\Menu;

/**
 * SearchMenu represents the model behind the search form of `app\modules\cms\models\Menu`.
 */
class SearchMenu extends Menu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_parent', 'parent_id', 'layouts_id', 'absolute_position', 'relative_position', 'publish'], 'integer'],
            [['name', 'slug', 'date_create', 'create_by', 'date_update', 'update_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Menu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'is_parent' => $this->is_parent,
            'parent_id' => $this->parent_id,
            'layouts_id' => $this->layouts_id,
            'absolute_position' => $this->absolute_position,
            'relative_position' => $this->relative_position,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
            'publish' => $this->publish,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
