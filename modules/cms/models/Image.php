<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string|null $name
 * @property int|null $layouts_id
 * @property string|null $url
 * @property int|null $component_id
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $date_update
 * @property string|null $update_by
 * @property string|null $is_publish
 *
 * @property ComponentsHasImage[] $componentsHasImages
 * @property Component[] $components
 * @property Layout $layouts
 * @property Component $component
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['layouts_id', 'component_id', 'is_publish'], 'integer'],
            [['url'], 'string'],
            [['date_create', 'date_update'], 'safe'],
            [['title', 'slug', 'name', 'create_by', 'update_by'], 'string', 'max' => 255],
           
            [['slug'], 'unique'],
            [['name'], 'unique'],
            [['layouts_id'], 'exist', 'skipOnError' => true, 'targetClass' => Layout::className(), 'targetAttribute' => ['layouts_id' => 'id']],
            [['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'name' => Yii::t('app', 'Name'),
            'layouts_id' => Yii::t('app', 'Layouts ID'),
            'url' => Yii::t('app', 'Url'),
            'component_id' => Yii::t('app', 'Component ID'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'date_update' => Yii::t('app', 'Date Update'),
            'update_by' => Yii::t('app', 'Update By'),
            'is_publish' => Yii::t('app', 'Is Publish'),
        ];
    }

    /**
     * Gets query for [[ComponentsHasImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponentsHasImages()
    {
        return $this->hasMany(ComponentsHasImage::className(), ['images_id' => 'id']);
    }

    /**
     * Gets query for [[Components]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasMany(Component::className(), ['id' => 'components_id'])->viaTable('components_has_images', ['images_id' => 'id']);
    }

    /**
     * Gets query for [[Layouts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLayouts()
    {
        return $this->hasOne(Layout::className(), ['id' => 'layouts_id']);
    }

    /**
     * Gets query for [[Component]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['id' => 'component_id']);
    }
}
