<?php

namespace app\modules\cms\models;

/**
 * This is the ActiveQuery class for [[Ressource]].
 *
 * @see Ressource
 */
class RessourceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Ressource[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Ressource|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
