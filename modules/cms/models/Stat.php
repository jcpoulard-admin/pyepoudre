<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "stat".
 *
 * @property int $id
 * @property string|null $article_visit
 * @property string|null $menu_click
 * @property string|null $image_click
 * @property string|null $adresse_ip
 * @property string|null $browser_tyoe
 * @property string|null $device_type
 * @property string|null $date_visit
 */
class Stat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_visit'], 'safe'],
            [['article_visit', 'menu_click', 'image_click', 'adresse_ip', 'browser_tyoe', 'device_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'article_visit' => Yii::t('app', 'Article Visit'),
            'menu_click' => Yii::t('app', 'Menu Click'),
            'image_click' => Yii::t('app', 'Image Click'),
            'adresse_ip' => Yii::t('app', 'Adresse Ip'),
            'browser_tyoe' => Yii::t('app', 'Browser Tyoe'),
            'device_type' => Yii::t('app', 'Device Type'),
            'date_visit' => Yii::t('app', 'Date Visit'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return StatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StatQuery(get_called_class());
    }
}
