<?php

namespace app\modules\cms\models;

/**
 * This is the ActiveQuery class for [[ManageRessource]].
 *
 * @see ManageRessource
 */
class ManageRessourceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ManageRessource[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ManageRessource|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
