<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "epingles".
 *
 * @property int $id
 * @property string $nom_epingle
 * @property string $url_epingle
 * @property int $is_publish
 * @property string|null $date_create
 * @property string|null $date_update
 * @property string|null $create_by
 * @property string|null $update_by
 */
class Epingles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'epingles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nom_epingle', 'url_epingle', 'is_publish'], 'required'],
            [['is_publish'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['nom_epingle', 'url_epingle', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nom_epingle' => Yii::t('app', 'Nom Epingle'),
            'url_epingle' => Yii::t('app', 'Url Epingle'),
            'is_publish' => Yii::t('app', 'Is Publish'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }
}
