<?php

namespace app\modules\cms\models;

/**
 * This is the ActiveQuery class for [[ComponentsHasImage]].
 *
 * @see ComponentsHasImage
 */
class ComponentsHasImageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return ComponentsHasImage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return ComponentsHasImage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
