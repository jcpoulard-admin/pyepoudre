<?php

namespace app\modules\cms\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cms\models\Article;

/**
 * SearchArticle represents the model behind the search form of `app\modules\cms\models\Article`.
 */
class SearchArticle extends Article
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'menus_id', 'layouts_id', 'images_id', 'components_id', 'is_publish'], 'integer'],
            [['title', 'slug', 'body', 'create_date', 'create_by', 'update_date', 'update_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Article::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'menus_id' => $this->menus_id,
            'layouts_id' => $this->layouts_id,
            'images_id' => $this->images_id,
            'components_id' => $this->components_id,
            'is_publish' => $this->is_publish,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
