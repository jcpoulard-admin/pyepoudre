<?php

namespace app\modules\cms\models;

use Yii;

/**
 * This is the model class for table "ressources".
 *
 * @property int $id
 * @property string $name
 * @property string $short_name
 * @property string|null $properties
 * @property string|null $description
 * @property string|null $url
 * @property string|null $date_create
 * @property string|null $create_by
 * @property string|null $update_date
 * @property string|null $update_by
 *
 * @property ManageRessource[] $manageRessources
 */
class Ressource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ressources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'short_name'], 'required'],
            [['properties', 'description'], 'string'],
            [['date_create', 'update_date'], 'safe'],
            [['name', 'url', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['short_name'], 'string', 'max' => 128],
            [['name'], 'unique'],
            [['short_name'], 'unique'],
            [['url'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'properties' => Yii::t('app', 'Properties'),
            'description' => Yii::t('app', 'Description'),
            'url' => Yii::t('app', 'Url'),
            'date_create' => Yii::t('app', 'Date Create'),
            'create_by' => Yii::t('app', 'Create By'),
            'update_date' => Yii::t('app', 'Update Date'),
            'update_by' => Yii::t('app', 'Update By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManageRessources()
    {
        return $this->hasMany(ManageRessource::className(), ['ressources_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return RessourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RessourceQuery(get_called_class());
    }
}
