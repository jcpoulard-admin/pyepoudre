<?php

namespace app\modules\cms\controllers;

use Yii;
use app\modules\cms\models\Component;
use app\modules\cms\models\SearchComponent;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\cms\models\CmsIndex;
use yii\helpers\Html;

/**
 * ComponentController implements the CRUD actions for Component model.
 */
class ComponentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Component models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchComponent();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Component model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Component model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( (Yii::$app->user->can('cms-article-create'))){ 
            $model = new Component();

            if ($model->load(Yii::$app->request->post())) {
                $model->create_by = currentUser(); 
                $model->create_date = date('Y-m-d H:i:s');
                $model->save();
                $cms_index = new CmsIndex();
                $cms_index->element_id = $model->id; 
                $cms_index->cms_indexcol = $model->name; 
                $cms_index->element_type = "component";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->description);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
                return $this->redirect(['index']);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
     public function actionPublish($id)
    {
       if( (Yii::$app->user->can('cms-component-update'))){  
        $is_publish = $this->findModel($id)->is_publish;
        if($is_publish == 1){
            $model = $this->findModel($id);
            $model->is_publish = 0; 
            $model->update_by = currentUser(); 
            $model->update_date = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }else{
            $model = $this->findModel($id);
            $model->is_publish = 1; 
            $model->update_by = currentUser(); 
            $model->update_date = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }

       
       }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
     public function actionSetHome($id){
        if( (Yii::$app->user->can('cms-component-update'))){
            $is_home = $this->findModel($id)->is_home;
            if($is_home == 1){
            $model = $this->findModel($id);
            $model->is_home = 0; 
            $model->update_by = currentUser(); 
            $model->update_date = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }else{
            $model = $this->findModel($id);
            $model->is_home = 1; 
            $model->update_by = currentUser(); 
            $model->update_date = date('Y-m-d H:i:s');
            $count_home = Component::findBySql("SELECT * FROM components WHERE is_home = 1")->count(); 
            if($count_home >= 1){
                Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You can t add more than one component as home !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized action') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
            }else{
                $model->save();
                return $this->redirect(['index','test'=>$count_home]);
            }
        }
            
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Updates an existing Component model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if( (Yii::$app->user->can('cms-component-update'))){ 
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $model->update_by = currentUser(); 
                $model->update_date = date('Y-m-d H:i:s');
                // Mise a jour de l'indexation 
            $cms_index = CmsIndex::findOne(['element_id'=>$id,'element_type'=>'component']);
            if(isset($cms_index->id)){
                $cms_index->element_id = $model->id; 
                $cms_index->cms_indexcol = $model->name; 
                $cms_index->element_type = "component";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->description);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
            }else{
                $cms_index = new CmsIndex();
                $cms_index->element_id = $model->id; 
                $cms_index->cms_indexcol = $model->name; 
                $cms_index->element_type = "component";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->description);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
            }
                $model->save(); 
                
                return $this->redirect(['index']);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Component model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if( (Yii::$app->user->can('cms-component-delete'))){ 
            $cms_index = CmsIndex::findOne(['element_id'=>$id,'element_type'=>'component']);
            if(isset($cms_index->id)){
                $cms_index->delete(); 
                $this->findModel($id)->delete();
            }
            return $this->redirect(['index']);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Finds the Component model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Component the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Component::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
