<?php

namespace app\modules\cms\controllers;

use Yii;
use app\modules\cms\models\Menu;
use app\modules\cms\models\SearchMenu;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use app\modules\cms\models\CmsIndex;

/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Menu models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( (Yii::$app->user->can('cms-menu-index'))){ 
        $searchModel = new SearchMenu();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
        
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( (Yii::$app->user->can('cms-menu-create'))){  
        $model = new Menu();
        $cms_index = new CmsIndex();
        if ($model->load(Yii::$app->request->post())) {
            if(isset($_POST['Menu']['parent_id']) && $_POST['Menu']['parent_id'] !== ''){
                $parent_id = $_POST['Menu']['parent_id'];
                
                $previous_relative_position = Menu::findBySql("SELECT * FROM menus WHERE parent_id = $parent_id ORDER BY id DESC LIMIT 1")->one(); 
                if(isset($previous_relative_position)){
                    $model->relative_position = $previous_relative_position->relative_position + 1;
                }else{
                    $model->relative_position = 0;
                }
                 }else{
                     $model->relative_position = 0;
                 }
            $model->create_by = currentUser(); 
            $model->date_create = date('Y-m-d H:i:s');
            $model->save();
            // Indexation des elements 
            $cms_index->element_id = $model->id; 
            $cms_index->element_slug = $model->slug; 
            $cms_index->element_type = "menu";
            $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
            $cms_index->index_value = createIndex($index_remove_words, $model->name);
            $cms_index->date_create = date('Y-m-d H:i:s');
            $cms_index->save(); 
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
     public function actionPublish($id)
    {
       if( (Yii::$app->user->can('cms-menu-update'))){  
        $is_publish = $this->findModel($id)->publish;
        if($is_publish == 1){
            $model = $this->findModel($id);
            $model->publish = 0; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }else{
            $model = $this->findModel($id);
            $model->publish = 1; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }

       
       }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if( (Yii::$app->user->can('cms-menu-update'))){  
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            // Mise a jour de l'indexation 
            $cms_index = CmsIndex::findOne(['element_id'=>$id,'element_type'=>'menu']);
            if(isset($cms_index->id)){
                $cms_index->element_id = $model->id; 
                $cms_index->element_slug = $model->slug; 
                $cms_index->element_type = "menu";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->name);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
            }else{
                $cms_index = new CmsIndex();
                $cms_index->element_id = $model->id; 
                $cms_index->element_slug = $model->slug; 
                $cms_index->element_type = "menu";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->name);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
            }
            
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
    
    public function actionDeplaseUp($id,$konte){
         if( (Yii::$app->user->can('cms-menu-update'))){  
                $model = $this->findModel($id);
                if($konte > 0){
                    $konte_avan = $konte - 1; 
                    $relative_position_id_avant = Menu::findBySql("SELECT * FROM menus WHERE parent_id = $model->parent_id AND relative_position = $konte_avan")->one()->id;
                    $model_avant = $this->findModel($relative_position_id_avant);
                    $model_avant->relative_position = $konte;
                    $model_avant->update_by = currentUser(); 
                    $model_avant->date_update = date('Y-m-d H:i:s');
                    $model_avant->save();
                    $model->relative_position = $konte_avan; 
                    $model->update_by = currentUser(); 
                    $model->date_update = date('Y-m-d H:i:s');
                    $model->save();
                    return $this->redirect(['index']);
                }else{
                $model->update_by = currentUser(); 
                $model->date_update = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['index']);
                }
         }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
    public function actionDeplaseDown($id,$konte){
         if( (Yii::$app->user->can('cms-menu-update'))){ 
                $model = $this->findModel($id);
                $max_relative_position = Menu::findBySql("SELECT MAX(relative_position) max_position FROM `menus` where parent_id = $model->parent_id ")->one()->max_position;
                
                if($konte < $max_relative_position){
                    $konte_avan = $konte + 1; 
                    $relative_position_id_avant = Menu::findBySql("SELECT * FROM menus WHERE parent_id = $model->parent_id AND relative_position = $konte_avan")->one()->id;
                    $model_avant = $this->findModel($relative_position_id_avant);
                    $model_avant->relative_position = $konte;
                    $model_avant->update_by = currentUser(); 
                    $model_avant->date_update = date('Y-m-d H:i:s');
                    $model_avant->save();
                    $model->relative_position = $konte_avan; 
                    $model->update_by = currentUser(); 
                    $model->date_update = date('Y-m-d H:i:s');
                    $model->save();
                    return $this->redirect(['index']);
                }else{
                $model->update_by = currentUser(); 
                $model->date_update = date('Y-m-d H:i:s');
                $model->save();
                return $this->redirect(['index']);
                }
         }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if( (Yii::$app->user->can('cms-menu-delete'))){ 
            $is_parent = Menu::findBySql("SELECT * FROM menus WHERE parent_id = $id")->all();
            
        if(empty($is_parent)){
            $cms_index = CmsIndex::findOne(['element_id'=>$id,'element_type'=>'menu']);
            if(isset($cms_index->id)){
                $cms_index->delete(); 
            }
            $this->findModel($id)->delete();
             return $this->redirect(['index']);
        }
        else{
            return $this->redirect(Yii::$app->request->referrer);
        }
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
