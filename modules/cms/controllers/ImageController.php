<?php

namespace app\modules\cms\controllers;

use Yii;
use app\modules\cms\models\Image;
use app\modules\cms\models\CmsIndex;
use app\modules\cms\models\SearchImage;
use app\modules\cms\models\Component; 
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * ImageController implements the CRUD actions for Image model.
 */
class ImageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Image models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchImage();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Image model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Image model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if( (Yii::$app->user->can('cms-image-create'))){
            $model = new Image();

            if ($model->load(Yii::$app->request->post())) {
                $model->create_by = currentUser(); 
                $model->date_create = date('Y-m-d H:i:s');
                $model->name = $_POST['Article']['image_invisible'];
                $model->save(); 
                // Indexation des elements 
                $cms_index = new CmsIndex();
                $cms_index->element_id = $model->id; 
                $cms_index->element_slug = $model->slug; 
                $cms_index->element_type = "images";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->title);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
                return $this->redirect(['index']);
            }

            return $this->render('create', [
                'model' => $model,
            ]);
        } else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
    public function actionCreateForComponent($id)
    {
        if( (Yii::$app->user->can('cms-image-create'))){
             
            $model = new Image();

            if ($model->load(Yii::$app->request->post())) {
                $model->component_id = $id; 
                $model->create_by = currentUser(); 
                $model->date_create = date('Y-m-d H:i:s');
                $model->name = $_POST['Article']['image_invisible'];
                $model->save(); 
                // Indexation des elements 
                $cms_index = new CmsIndex();
                $cms_index->element_id = $model->id; 
                $cms_index->element_slug = $model->slug; 
                $cms_index->element_type = "image";
                $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                $cms_index->index_value = createIndex($index_remove_words, $model->title);
                $cms_index->date_create = date('Y-m-d H:i:s');
                $cms_index->save(); 
                return $this->redirect(['index']);
            }

            return $this->render('create_for_component', [
                'model' => $model,
                'id'=>$id
            ]);
        } else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Updates an existing Image model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if( (Yii::$app->user->can('cms-image-update'))){ 
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $model->update_by = currentUser(); 
                $model->date_update = date('Y-m-d H:i:s');
                if(isset($_POST['Image']['image_invisible']) && $_POST['Image']['image_invisible']!==""){
                    $model->name = $_POST['Image']['image_invisible'];
                }else{
                    $model->name = $model->name; 
                }
                
                // Mise a jour de l'indexation 
                $cms_index = CmsIndex::findOne(['element_id'=>$id,'element_type'=>'image']);
                if(isset($cms_index->id)){
                    $cms_index->element_id = $model->id; 
                    $cms_index->element_slug = $model->slug; 
                    $cms_index->element_type = "image";
                    $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                    $cms_index->index_value = createIndex($index_remove_words, $model->title);
                    $cms_index->date_create = date('Y-m-d H:i:s');
                    $cms_index->save(); 
                }else{
                    $cms_index = new CmsIndex();
                    $cms_index->element_id = $model->id; 
                    $cms_index->element_slug = $model->slug; 
                    $cms_index->element_type = "image";
                    $index_remove_words = explode(",",infoGeneralConfig('tri_element_non_pertinent'));
                    $cms_index->index_value = createIndex($index_remove_words, $model->title);
                    $cms_index->date_create = date('Y-m-d H:i:s');
                    $cms_index->save(); 
                }
                
                $model->save(); 
                return $this->redirect(['index']);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      } 
    }
    
    public function actionPublish($id)
       {
       if( (Yii::$app->user->can('cms-image-update'))){  
        $is_publish = $this->findModel($id)->is_publish;
        if($is_publish == 1){
            $model = $this->findModel($id);
            $model->is_publish = 0; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }else{
            $model = $this->findModel($id);
            $model->is_publish = 1; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }

       
       }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    

    /**
     * Deletes an existing Image model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Image model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Image the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Image::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
