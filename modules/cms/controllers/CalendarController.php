<?php

namespace app\modules\cms\controllers;

use Yii;
use app\modules\cms\models\Calendar;
use app\modules\cms\models\SearchCalendar;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * CalendarController implements the CRUD actions for Calendar model.
 */
class CalendarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calendar models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( (Yii::$app->user->can('cms-calendar-index'))){
        $searchModel = new SearchCalendar();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $calendar = Calendar::find()->all(); 
        $events = [];
        $j = 0; 
        foreach($calendar as $cal){
            $events[$j]['id'] = $cal->id; 
            $events[$j]['title'] = $cal->title;
            $events[$j]['start'] = $cal->date_start;
            $events[$j]['end'] = $cal->date_end;//date("Y-m-d",strtotime($cal->date_end."+1 day"));
            $events[$j]['location'] = $cal->location; 
            $youtube_arr = explode("=",$cal->media);
            if(isset($youtube_arr[1])){
                $youtube_link = $youtube_arr[1];
                $youtube_id = str_replace("&list","",$youtube_link); 
                $make_media_link = "<img src='https://img.youtube.com/vi/$youtube_id/hqdefault.jpg' class='img-responsive youtube-link' youtubeid='$youtube_id'/>";  
                 }else{
                     $make_media_link = "<img width='400px' heigth='300px' src='$cal->media' class='img-responsive'/>";
                 }
            
            $events[$j]['media'] = $make_media_link; 
            $events[$j]['description'] = $cal->description; 
            $events[$j]['prix'] = $cal->prix;
            $events[$j]['intervenant'] = $cal->intervenant;
            $events[$j]['url_delete'] = "delete?id=$cal->id";
            $events[$j]['url_update'] = "update?id=$cal->id&wh=pla_ho";
            $j++; 
        }
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'events'=>$events,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Displays a single Calendar model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    /*
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    */
    /**
     * Creates a new Calendar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
    public function actionCreate()
    {
        $model = new Calendar();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
     * 
     */
    
    
    public function actionCreateEvent($title, $date_start,$date_end, $location, $media, $description,$prix,$intervenant){
        if( (Yii::$app->user->can('cms-calendar-create-event'))){
           $model = new Calendar(); 
           $model->title = $title; 
           $model->date_start = date("Y-m-d H:i:s",strtotime($date_start));
           $model->date_end = date("Y-m-d H:i:s",strtotime($date_end));
           $model->location = $location; 
           $model->media = $media; 
           $model->description = $description;
           $model->prix = $prix;
           $model->intervenant = $intervenant; 
           $model->create_by = currentUser(); 
           $model->date_create = date("Y-m-d H:i:s");
           $model->save(); 
       
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Updates an existing Calendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if( (Yii::$app->user->can('cms-calendar-update'))){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      } 
    }

    /**
     * Deletes an existing Calendar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if( (Yii::$app->user->can('cms-calendar-update'))){
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      } 
    }

    /**
     * Finds the Calendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calendar::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
