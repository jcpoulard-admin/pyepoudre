<?php

namespace app\modules\cms\controllers;

use Yii;
use app\modules\cms\models\Epingles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * EpinglesController implements the CRUD actions for Epingles model.
 */
class EpinglesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Epingles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Epingles::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Epingles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Epingles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
      if( (Yii::$app->user->can('cms-epingle-create'))){   
        $model = new Epingles();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_by = currentUser(); 
            $model->date_create = date('Y-m-d H:i:s');
            $model->save(); 
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
      }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }
    
     public function actionPublish($id)
    {
       if( (Yii::$app->user->can('cms-epingle-update'))){  
        $is_publish = $this->findModel($id)->is_publish;
        if($is_publish == 1){
            $model = $this->findModel($id);
            $model->is_publish = 0; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }else{
            $model = $this->findModel($id);
            $model->is_publish = 1; 
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save();
            return $this->redirect(['index']);
        }

       
       }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Updates an existing Epingles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if( (Yii::$app->user->can('cms-epingle-update'))){ 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->update_by = currentUser(); 
            $model->date_update = date('Y-m-d H:i:s');
            $model->save(); 
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Deletes an existing Epingles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if( (Yii::$app->user->can('cms-epingle-delete'))){ 
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }else{
          if(Yii::$app->session['li_konekte']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
                else
               {  
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Administration!") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }
      }
    }

    /**
     * Finds the Epingles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Epingles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Epingles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
