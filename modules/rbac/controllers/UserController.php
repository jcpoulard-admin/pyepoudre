<?php


namespace app\modules\rbac\controllers; 

use Yii;
use yii\helpers\Html;
use app\modules\rbac\models\form\PasswordResetRequest;
use app\modules\rbac\models\form\ResetPassword;
use app\modules\rbac\models\form\Signup;
use app\modules\rbac\models\form\ChangePassword;
use app\modules\rbac\models\form\PasswordReset;
use app\modules\rbac\models\form\Login; 
use app\modules\rbac\models\User;
use app\modules\rbac\models\searchs\User as UserSearch;
use app\modules\rbac\models\searchs\Assignment as AssignmentSearch;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\base\UserException;
use yii\mail\BaseMailer;
/*
use app\modules\fi\models\SrcAcademicperiods;
use app\modules\fi\models\SrcPersons;

use app\modules\billings\models\SrcFees;
use app\modules\billings\models\SrcBillings;
use app\modules\billings\models\SrcBalance;
use app\modules\billings\models\SrcScholarshipHolder;
*/

use yii\db\IntegrityException;
use yii\web\ForbiddenHttpException;

/**
 * User controller
 */
class UserController extends Controller
{
    private $_oldMailPath;
    
     public $message=false;
	 
	 
	  public $full_scholarship=false;
	  public $internal;
          
          public $username_liste_select=1;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
//                        'actions' => ['signup', 'reset-password', 'login', 'request-password-reset'],
//                        'allow' => true,
//                        'roles' => ['?'],
//                    ],
//                    [
//                        'actions' => ['logout', 'change-password', 'index', 'view', 'delete', 'activate'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'logout' => ['post'],
                    'activate' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (Yii::$app->has('mailer') && ($mailer = Yii::$app->getMailer()) instanceof BaseMailer) {
                /* @var $mailer BaseMailer */
                $this->_oldMailPath = $mailer->getViewPath();
                $mailer->setViewPath('@mdm/admin/mail');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterAction($action, $result)
    {
        if ($this->_oldMailPath !== null) {
            Yii::$app->getMailer()->setViewPath($this->_oldMailPath);
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all Active User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->can('rbac-user-index'))
         {
         	  $searchModel = new UserSearch();
		        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		        				   
			   return $this->render('index', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    
    public function actionUsernamelist()
    {
        if(Yii::$app->user->can('rbac-user-usernamelist'))
         {
         	  $searchModel = new UserSearch();
		  
                if( (Yii::$app->user->can("Administration études")) )
                  {
                      //students  
                     $dataProvider = $searchModel->searchStudentUserForList();
                      $for='stud';
                  
                  }
                elseif( (Yii::$app->user->can("superadmin")) ||(Yii::$app->user->can("direction")) ||(Yii::$app->user->can("Administration économat")) )
                 {
                    if(isset($_POST['User']['username_list']))
		       $this->username_liste_select = $_POST['User']['username_list'];
                    
                      $searchModel->username_list = $this->username_liste_select;
                   
                      if($this->username_liste_select==0)
                        {
                         //staff
                         $dataProvider = $searchModel->searchStaffUserForList();
                          $for='staff';
                        }
                      elseif($this->username_liste_select==1)
                        {
                          //students  
                            $dataProvider = $searchModel->searchStudentUserForList();
                             $for='stud';
                      
                         }
                    
                  }
		        				   
			   return $this->render('usernamelist', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
                               'for'=>$for,
                               'username_liste'=>$this->username_liste_select
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

 /**
     * Lists all Inactive User models.
     * @return mixed
     */
    public function actionUsersdisabled()
    {
        if(Yii::$app->user->can('rbac-user-usersdisabled'))
         {
         	  $searchModel = new UserSearch();
		        $dataProvider = $searchModel->searchUsersDisabled(Yii::$app->request->queryParams);
		
		         if (isset($_GET['pageSize'])) 
				         	 {
						        Yii::$app->session->set('pageSize',(int)$_GET['pageSize']);
						          unset($_GET['pageSize']);
							   }
							   
			   return $this->render('index0', [
		                'searchModel' => $searchModel,
		                'dataProvider' => $dataProvider,
		        ]);
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->can('rbac-user-view'))
         {
					 return $this->render('view', [
		                'model' => $this->findModel($id),
		        ]);
		     }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->can('rbac-user-delete'))
         {
         	try{
         		 $user_id = $id;
         		 
				  $modelUser = $this->findModel($id);
				  
				   $person_id = $modelUser->person_id;
			        
			        $modelUser->delete();
			        
			        //retire tout role user sa
			           
						          	  $modelUserAssign = new AssignmentSearch;
						          	                        
                                       //return item_name in array  
                                                $items =  $modelUserAssign->searchByUserid($user_id);	
                          
						          	   if($items!=null)
						          	     {
						          	       $modelAssignment = new Assignment($user_id);
						          	       
						          	       $modelAssignment->revoke($items);
						          	       
						          	     }
						  
			              
		        return $this->redirect(['index','wh'=>'use']);
	        
	         } catch (IntegrityException $e) {
			    if($e->errorInfo[1] == 1451) {
			       Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"There are dependant elements, you have to delete them first.") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                          $this->redirect(Yii::$app->request->referrer);
			    } else {
			       // throw $e;
			        Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode($e),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
			    }
			    
			    			    
			}
			
			
           }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
              $this->redirect(Yii::$app->request->referrer);
               }

          }
    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
       
       $this->layout = "/layout_login";
       
      // $modelPers = new SrcPersons();
       
      // $user_profil = -1;
      // $student_parent_guest=0;
       
        if (!Yii::$app->getUser()->isGuest) {
        	return $this->goHome();
        }

        $model = new Login();
        if($model->load(Yii::$app->getRequest()->post()) && $model->login()) 
          {
            Yii::$app->session['li_konekte'] = 1;
              //$modelAcad = new SrcAcademicperiods;
             // $current_acad=$modelAcad->searchCurrentAcademicYear();
                   
            $modelUser = User::findOne(Yii::$app->user->identity->id); 
            
                        return $this->goHome(); 
                         
                         
                     }
                   
           
        else 
           {
              return $this->render('login', [
                    'model' => $model,
                ]);
           }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
       // Yii::$app->getUser()->logout();
        $last_Activity = '';
        $user_id = '';
		
		if(isset(Yii::$app->user->identity->id))
		 { $user_id = Yii::$app->user->identity->id;
		 
		    //return datetime (last_activity)
            // $last_Activity = isUserConnected($user_id);
		 }
		else
		  $user_id = '';
		  
		 
				
	        Yii::$app->getUser()->logout();
	         Yii::$app->session['li_konekte'] = ""; 
	        

        return $this->goHome();
    }

    /**
     * Signup new user
     * @return string
     */
    public function actionSignup()
    {
        /*
        if(Yii::$app->user->can('rbac-user-signup'))
            {
         * 
         */
              $model = new Signup();
	        if ($model->load(Yii::$app->getRequest()->post())) {
	            if ($user = $model->signup()) {
	                return $this->goHome();
	            }
	        }
	
	        return $this->render('signup', [
	                'model' => $model,
	        ]);
                /*
	      }
      /*        
      else
        {
            
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
                    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
                    'duration' =>120000,
                    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
                    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
                    'title' => Html::encode(Yii::t('app','Unthorized access') ),
                    'positonY' => 'top',   //   top,//   bottom,//
                    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                 return $this->redirect(Yii::$app->request->referrer);
              

          }
          */
	        
    }

public function actionViewonlineusers()
 {
 	
     if( (Yii::$app->user->can('rbac-user-viewonlineusers'))||(Yii::$app->user->can('superadmin')) )
         {
         	$user = new UserSearch();
            return $this->render('onlineusers',['model'=>$user]);
            
         }
       else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                  return $this->redirect(Yii::$app->request->referrer);
               }

          }
}
 

    /**
     * Request reset password
     * @return string
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = "/layout_login";
        $model = new PasswordResetRequest();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
                'model' => $model,
        ]);
    }

    /**
     * Reset password
     * @return string
     */
    public function actionResetPassword($token)
    {
        $this->layout = "/layout_login";
         
        try {
            $model = new ResetPassword($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->getRequest()->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                'model' => $model,
        ]);
    }

    /**
     * Change password
     * @return string
     */
    public function actionChangepassword()
    {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->change()) {
            return $this->goHome();
        }

        return $this->render('change-password', [
                'model' => $model,
        ]);
    }
    
    
     /**
     * Reset password
     * @return string
     */
    public function actionPasswordreset()
    {
        $model = new PasswordReset();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->reset($_GET['id'], $model->newPassword)) 
        {
              if(Yii::$app->user->can('rbac-user-index'))
                  return $this->redirect(['/rbac/user/index?wh=use&']);
               else
                   return $this->goHome();
        }

        return $this->render('password_reset', [
                'model' => $model,
        ]);
    }


    /**
     * Change profil
     * @return string
     */
    public function actionChangeprofil()
    {
        Yii::$app->session['profil_as'] = $_GET['profil'];
        
        //$this->redirect(Yii::$app->request->referrer);
        //$this->redirect('../../../index.php');
        return $this->redirect('../../reports/report/dashboardpedago');
        
    }
    

    /**
     * Activate new user
     * @param integer $id
     * @return type
     * @throws UserException
     * @throws NotFoundHttpException
     */
    public function actionActivate($id)
    {
        /* @var $user User */
        if(Yii::$app->user->can('rbac-user-activate'))
         {
		   if(Yii::$app->user->identity->id!=$id)
		     {
		       $user = $this->findModel($id);
		        if ($user->status == User::STATUS_INACTIVE) {
		            $user->status = User::STATUS_ACTIVE;
		            if ($user->save()) 
		             {
		            	//enable associate person
		            	
		            	
		                return $this->redirect('index?wh=use');
		            } else {
		                $errors = $user->firstErrors;
		                throw new UserException(reset($errors));
		            }
		        }elseif ($user->status == User::STATUS_ACTIVE) {
		            $user->status = User::STATUS_INACTIVE;
		            if ($user->save()) 
		             {
		             	

		             	
		                return $this->redirect('index?wh=use');
		            } else {
		                $errors = $user->firstErrors;
		                throw new UserException(reset($errors));
		            }
		        }
		        
		      }
		    else 
		      {
		      	  Yii::$app->getSession()->setFlash('warning', [
										    'type' => 'warning', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' => 36000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode( Yii::t('app','You are not allowed to enable/disable yourself.') ),
										    'title' => Html::encode(Yii::t('app','Warning') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
		      	  return $this->redirect(Yii::$app->request->referrer);
                          
                      }
		      	
		      	
		        return $this->redirect('index?wh=use');
		        
		        
		    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                   return $this->redirect(Yii::$app->request->referrer);
               }

          }        
		        
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*
     * Creer un utilisateur dans le système
     * 
     */
    public function actionCreate(){
       if(Yii::$app->user->can('rbac-user-create'))
         {
		    $model = new Signup();
		        if ($model->load(Yii::$app->getRequest()->post())) {
		            if ($user = $model->signup()) {
		                return $this->goHome();
		            }
		        }
		
		        return $this->render('create', [
		                'model' => $model,
		        ]);
		        
    }
      else
        {
            if(Yii::$app->session['currentId_academic_year']=='')
              {   
              	  return $this->redirect(['/rbac/user/login']); 
                }
             else
               {  
              //throw new ForbiddenHttpException;
              Yii::$app->getSession()->setFlash('Error', [
										    'type' => 'danger', //  success,//  info,// warning,//  danger,//  growl,//   minimalist,//   pastel,//
										    'duration' =>120000,
										    'icon' => 'glyphicon glyphicon-exclamation-sign',      // glyphicon glyphicon-ok-sign // glyphicon glyphicon-info-sign // glyphicon glyphicon-exclamation-sign  //    glyphicon glyphicon-remove-sign 
										    'message' => Html::encode(Yii::t('app',"You do not have the privilleges to perform this action. Please contact the Direction !") ),
										    'title' => Html::encode(Yii::t('app','Unthorized access') ),
										    'positonY' => 'top',   //   top,//   bottom,//
										    'positonX' => 'center'    //   right,//   center,//  left,//
										]);
                 return $this->redirect(Yii::$app->request->referrer);
               }

          }
        
    }
    
}
