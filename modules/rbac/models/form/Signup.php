<?php

namespace app\modules\rbac\models\form;

use Yii;
use app\modules\rbac\models\User; 
use yii\base\Model;

/**
 * Signup form
 */
class Signup extends Model
{
    public $username;
    public $full_name;
   // public $person_id;
   // public $is_parent;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['full_name','required'],
            ['username', 'unique', 'targetClass' => 'app\modules\rbac\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            //['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => 'app\modules\rbac\models\User', 'message' => 'This email address has already been taken.'],

            //[['person_id', 'is_parent' ], 'integer'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->full_name = $this->full_name;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    
    
  public function signup_from_person()
    {
       
            $user = new User();
            $user->username = $this->username;
            $user->person_id = $this->person_id;
            $user->is_parent = $this->is_parent;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            
            $user->save();
                
     }   
    
    
    
    
}
