<?php

namespace app\modules\rbac\models\form; 

use Yii;
use app\modules\rbac\models\User;
use yii\base\Model;

/**
 * Description of ChangePassword
 *
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class PasswordReset extends Model
{
   
    public $newPassword;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newPassword', 'retypePassword'], 'required'],
            [['newPassword'], 'string', 'min' => 6],
            [['retypePassword'], 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

      public function attributeLabels()
    {
        return [
            'newPassword' => Yii::t('app', 'New Password'),
            'retypePassword' => Yii::t('app', 'Retype Password'),
            
        ];
    }
    
    /**
     * Change password.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function reset($user_id, $new_password)
    {
        if ($this->validate()) {
            /* @var $user User */
            $userModel = new User;
            
            $user = $userModel->findIdentity($user_id);
            
            $user->setPassword($new_password);
            $user->generateAuthKey();
            if ($user->save()) {
                return true;
            }
        }

        return false;
    }
}
