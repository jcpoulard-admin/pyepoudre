<?php

namespace app\modules\rbac\models\searchs; 

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\rbac\models\User as UserModel;

use app\modules\fi\models\SrcPersons;


/**
 * User represents the model behind the search form about `mdm\admin\models\User`.
 */
class User extends UserModel
{
    public $globalSearch;
    
    public $username_list;
    
    //public $full_name;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email','globalSearch'], 'safe'],
        ];
    }

    public function attributeLabels()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
             return array(
			               'username_list' =>Yii::t('app','Sort username by'),
			               
		);
	}	
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserModel::find();//->where(['status' => self::STATUS_ACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

      /*  $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        */
        
         if(Yii::$app->user->can('superadmin'))
                  echo '';//$query->andFilterWhere(['not in', 'username',['_super_'] ])
              elseif(Yii::$app->user->can('direction'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam'] ]);
              elseif(Yii::$app->user->can('manager'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin'] ]);
              else
                 $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin',Yii::$app->user->identity->username] ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
    
  
  
public function searchStudentUserForList()
    {
        $query = SrcPersons::find()
                 ->joinWith(['users','studentLevel','studentOtherInfo0','studentOtherInfo0.applyShift', 'studentOtherInfo0.applyForProgram', 'studentLevel.room0',])
                ->where('is_student=1 and active in(1,2)');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

       
        return $dataProvider;
    }
    
    
  public function searchStaffUserForList()
    {
        $query = SrcPersons::find()
                 ->joinWith(['users','employeeInfos','personsHasTitles','courses0'])
                ->where('is_student=0 and active in(1,2)');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

       
        return $dataProvider;
    }
    
    
    
   public function searchUsersDisabled($params)
    {
        $query = UserModel::find()->where(['status' => self::STATUS_INACTIVE]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

      /*  $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        */
        
         if(Yii::$app->user->can('superadmin'))
                  echo '';//$query->andFilterWhere(['not in', 'username',['_super_'] ])
              elseif(Yii::$app->user->can('direction'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam'] ]);
              elseif(Yii::$app->user->can('manager'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin'] ]);
              else
                 $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin',Yii::$app->user->identity->username] ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
    
    
  public function searchGlobal($params)
    {
        $query = UserModel::find()->orderBy(['username'=>SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('1=0');
            return $dataProvider;
        }

      /*  $query->orFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
       */
              if(Yii::$app->user->can('superadmin'))
                  echo '';//$query->andFilterWhere(['not in', 'username',['_super_'] ])
              elseif(Yii::$app->user->can('direction'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam'] ]);
              elseif(Yii::$app->user->can('manager'))  
                    $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin'] ]);
              else
                 $query->andFilterWhere(['not in', 'username',['_super_','logipam','admin',Yii::$app->user->identity->username] ]);
                 
        $query->orFilterWhere(['like', 'username', $this->globalSearch])
            ->orFilterWhere(['like', 'auth_key', $this->globalSearch])
            ->orFilterWhere(['like', 'password_hash', $this->globalSearch])
            ->orFilterWhere(['like', 'password_reset_token', $this->globalSearch])
            ->orFilterWhere(['like', 'email', $this->globalSearch]);

        return $dataProvider;
    }



 public function getOnlineUsers()
    {
        //$sql = "SELECT session.user_id, session.last_ip, users.id, users.username, users.full_name, users.group_id FROM session LEFT JOIN users ON users.id=session.user_id WHERE users.username not in('_developer_','logipam','super_user','super_manager')";
        
        $sql = "SELECT session.user_id, session.last_ip, session.last_activity, user.id, user.username, user.person_id, user.is_parent, CONCAT(persons.first_name,\" \",persons.last_name) AS full_name FROM session LEFT JOIN user ON user.id=session.user_id LEFT JOIN persons ON persons.id=user.person_id WHERE user.username not in('_super_','logipam','super_user','super_manager')";
        $command = Yii::$app->db->createCommand($sql);
 
        return $command->queryAll();
    }

    
    
}
