<?php
namespace app\modules\rbac\models\searchs; 


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AssignmentSearch represents the model behind the search form about Assignment.
 * 
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class Assignment extends Model
{
    public $id;
    public $username;
    public $fullname;
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'username','firstname','lastname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'firstname' => Yii::t('app', 'First Name'),
            'lastname' => Yii::t('app', 'Last Name'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * Create data provider for Assignment model.
     * @param  array                        $params
     * @param  \yii\db\ActiveRecord         $class
     * @param  string                       $usernameField
     * @return \yii\data\ActiveDataProvider
     */
    public function search($params, $class, $usernameField)
    {
         if( (Yii::$app->user->can('superadmin')) )
             $query = $class::find()->orderBy(['username'=>SORT_ASC]);
          elseif( (Yii::$app->user->can('direction')) )
             $query = $class::find()->where(['not in', $usernameField, ['_super_',] ])->orderBy(['username'=>SORT_ASC]);
            elseif( (Yii::$app->user->can('manager')) )
             $query = $class::find()->where(['not in', $usernameField, ['_super_','admin',] ])->orderBy(['username'=>SORT_ASC]);
           else
             $query = $class::find()->where(['not in', $usernameField, ['_super_','admin',Yii::$app->user->identity->username] ])->orderBy(['username'=>SORT_ASC]);
             
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
        			'pageSize'=>10000000,
    			],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', $usernameField, $this->username]);

        return $dataProvider;
    }
    


//return user_id   
  public function searchByUseridItemname($user_id, $item_name)
    {
         $modelAuth_assign = Yii::$app->db->createCommand('select item_name, user_id from auth_assignment where user_id='.$user_id.' and item_name=\''.$item_name.'\'')->queryAll();
         
         if($modelAuth_assign!=null)
           {
           	   foreach($modelAuth_assign as $auth_assign)
           	    {
           	           return $auth_assign['user_id'];
           	     }
           	    	
           	}
          else
             return null;
         

       
    }


 //return item_name in array  
  public function searchByUserid($user_id)
    {
         $array_item_name = [];
         
         $modelAuth_assign = Yii::$app->db->createCommand('select item_name from auth_assignment where user_id='.$user_id)->queryAll();
         
         if($modelAuth_assign!=null)
           {
           	   foreach($modelAuth_assign as $auth_assign)
           	    {
           	           $array_item_name[] = $auth_assign['item_name'];
           	     }
           	     
           	    return $array_item_name;	
           	}
          else
             return null;
         

       
    }

      

}
