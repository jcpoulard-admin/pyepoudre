<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\Assignment */
/* @var $usernameField string */
/* @var $extraColumns string[] */

$this->title = Yii::t('app', 'Assignments');

?>

  <?php // $this->render('//layouts/rbacLayout'); ?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            
        
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content assignment-index">
   
   <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Username'); ?></th>
				            <th><?= Yii::t('app','Full Name'); ?></th>
				            
				           
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataAssignments = $dataProvider->getModels();
    
          foreach($dataAssignments as $assignment)
           {
           	   echo '  <tr >
                                                    <td >'.$assignment->username.' </td>
                                                    <td >'.$assignment->getFullNameByUserId($assignment->id).' </td>
                                                    
                                                    <td >'; 
                                                              
                                                          
                                                          if(Yii::$app->user->can('rbac-assignment-view')) 
                                                              {
												                echo Html::a('<span class="fa fa-key"></span>', Yii::getAlias('@web').'/index.php/rbac/assignment/view?id='.$assignment->id.'&wh=asi', [
                            'title' => Yii::t('app', 'Assign'),
                ]); 
                                                              }
                                                           
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>

 
</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Users Assignment List'},
                    {extend: 'pdf', title: 'Users Assignment List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>