<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\rbac\components\Helper; 

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');

?>

  <?= $this->render('//layouts/rbacLayout'); ?>
 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            
        
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 
<div class="wrapper wrapper-content user-index">
       
     <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Username'); ?></th>
				            <th><?= Yii::t('app','Full Name'); ?></th>
				            <th><?= Yii::t('app','Email'); ?></th>
				            <th><?= Yii::t('app','Status'); ?></th>
				           
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataUsers = $dataProvider->getModels();
    
          foreach($dataUsers as $user)
           {
           	   echo '  <tr >
                                                    <td >'.$user->username.' </td>
                                                    <td >'.$user->getFullNameByUserId($user->id).' </td>
                                                    <td >'.$user->email.' </td>
                                                    <td >'; echo $user->status == 0 ? Yii::t('app','Inactive') : Yii::t('app','Active'); echo ' </td>
                                                    
                                                    <td >'; 
                                                           if(Yii::$app->user->can('rbac-user-activate')) 
                                                              {
												              /*  if ($user->status == 10) {
											                            $options = [
											                            'title' => Yii::t('app', 'Desactivate'),
											                            'aria-label' => Yii::t('app', 'Desactivate'),
											                            'data-confirm' => Yii::t('app', 'Are you sure you want to desactivate this user?'),
											                            'data-method' => 'post',
											                            'data-pjax' => '0',
											                        ];
											                        echo Html::a('<span class="fa fa-toggle-on" style="color:green;"></span>', Yii::getAlias('@web').'/rbac/user/activate?id='.$user->id.'&wh=use', $options);
											                        }
											                      
											                     elseif ($user->status == 0) {
											                     */   $options = [
											                            'title' => Yii::t('app', 'Activate'),
											                            'aria-label' => Yii::t('app', 'Activate'),
											                            'data-confirm' => Yii::t('app', 'Are you sure you want to activate this user?'),
											                            'data-method' => 'post',
											                            'data-pjax' => '0',
											                        ];
											                        echo Html::a('<span class="fa fa-toggle-off" style="color:red;"></span>', Yii::getAlias('@web').'/rbac/user/activate?id='.$user->id.'&wh=use0', $options); 
											                   
											                   //  }
											                   
                                                              }
                                                          
                                                          if(Yii::$app->user->can('rbac-user-view')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/rbac/user/view?id='.$user->id.'&wh=use0', [
                                    'title' => Yii::t('app', 'View'),
                        ]); 
                                                              }
                                                              
                                                          
                                                              
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>
				    
            

</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'Users Disabled List'},
                    {extend: 'pdf', title: 'Users Disabled List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>