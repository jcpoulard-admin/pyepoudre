<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\rbac\components\Helper; 

use app\modules\fi\models\SrcPersons;
use app\modules\rbac\models\User;
use app\modules\rbac\models\searchs\User as UserSearch;

/* @var $this yii\web\View */
/* @var $searchModel mdm\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */



$acad = Yii::$app->session['currentId_academic_year'];
$count_user_online =0;
//$users = new UserSearch();
foreach($model->getOnlineUsers() as $user){
    $count_user_online++;
}


$this->title = Yii::t('app','Online user(s): {number}',array('number'=>$count_user_online));


$script1 = <<< JS
    $(document).ready(function() {
             $.ajax({
                     url: 'user/viewonlineusers',
                     type:"POST",
                     data:"dataPost",
                     success: function(data){
                     document.getElementById("dataPost").innerHTML = data;//why not use $('#dataPost').html(data) if you're already using jQuery?
                        setTimeout(refreshData, 3000);//will make the next call 3 seconds after the last one has finished.
                       //render the dynamic data into html
                     }
                   });
    });
    
JS;
$this->registerJs($script1);
?>



 
  <?= $this->render('//layouts/rbacLayout'); ?>
  
 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
            
        
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 
<div class="wrapper wrapper-content user-index">
       
     <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','User(s)'); ?></th>
				            <th><?= Yii::t('app','Connection Date'); ?></th>
				            <th><?= Yii::t('app','Connection time'); ?></th>
				            <th><?= Yii::t('app','Connected from'); ?></th>
				            
				           </tr>
				        </thead>
				        <tbody>     
<?php 

  //  $dataUsers = $dataProvider->getModels();
    
          foreach($model->getOnlineUsers() as $user)
           {
           	   $class_fa="fa fa-user";
           	   
           	   if($user["is_parent"]==1)  // Parent
           	     {
           	     	 $class_fa="fa fa-phone";
           	     	}
           	    else
           	      { 
           	      	$modelPers = new SrcPersons;  
           	      	$person = SrcPersons::findOne($user["person_id"]);
           	      	   
           	      	if($person!=null)
           	      	 {
	           	       if($person->id != null)
	           	      	 {
	           	      	   if($person->is_student==1)  // Students 
	           	      	    {
	           	      	    	$class_fa="fa fa-group";
	           	      	     }
	           	      	   else
	           	      	     {      
	           	      	         //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher
	                              $user_profil = $modelPers->isEmployeeOrTeacher($person->id, $acad);
	                              
	                              if($user_profil==1)// Teacher
	                                {
	                                	 $class_fa="fa fa-male";
	                                	}
	                               else
	                                 {
	                                 	  $class_fa="fa fa-user";
	                                 	}
	                              
	           	      	     	}
	           	      	     	
	           	      	 }
	           	      else
	                     {
	                        $class_fa="fa fa-user";
	                       }
	           	      	 
	           	      	 
           	      	 }	
           	      	     	
           	      	     	
           	      	}
           	   
           	   
           	                                    /*    //pou dekonekte l
           	                                           $options = [
											                            'title' => Yii::t('app', 'Disconnect'),
											                            'aria-label' => Yii::t('app', 'Disconnect'),
											                            'data-confirm' => Yii::t('app', 'Are you sure you want to disconnect this user?'),
											                            'data-method' => 'post',
											                            'data-pjax' => '0',
											                        ];
											         Html::a('<span class="'.$class_fa.' text-success" ></span>', Yii::getAlias('@web').'/index.php/rbac/user/disconnect?id='.$user["id"].'&wh=use', $options)
											               */
											                        
           	   echo '  <tr >
                                                    <td ><i class="'.$class_fa.' text-success"></i> ';
                                                  
                                                   if($user["full_name"]!='')
                                                       echo $user["full_name"];
                                                   else
                                                       echo 'Admin ADMIN';
                                                   
                                                    echo ' ('.$user["username"].') </td>
                                                    <td >'.Yii::$app->formatter->asDate( date("Y-m-d",strtotime($user["last_activity"]) )  ).' </td>
                                                    <td >'.Yii::$app->formatter->asTime(date("h:i:sa",strtotime($user["last_activity"]) ) ).' </td>
                                                    <td >'.$user["last_ip"].' </td>'; 
                                                           
                                                                                                                         
                                             echo '</tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>
				    
            

</div>

<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                   // { extend: 'copy'},
                    //{extend: 'csv'},
                    {extend: 'excel', title: 'User(s) Online List'},
                    {extend: 'pdf', title: 'User(s) Online List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>