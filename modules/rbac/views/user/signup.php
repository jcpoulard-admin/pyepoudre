<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title = Yii::t('app', 'Signup');


?>

<?= $this->render('//layouts/rbacLayout'); ?>

<div class="site-signup">
 <h1><?= Html::encode($this->title) ?></h1>  

    
    <?= Html::errorSummary($model)?>
    
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
     <div class="row">
        <div class="col-lg-6">
               <?= $form->field($model, 'full_name') ?>
         </div>
        <div class="col-lg-6">
               <?= $form->field($model, 'username') ?>
         </div>
     </div>
 <div class="row">
     <div class="col-lg-6">
               <?= $form->field($model, 'email') ?>
     </div>
     <div class="col-lg-6">
                <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
 </div>
   
            
      <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Signup') , ['name' => 'signup-button', 'class' => 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

            <?php ActiveForm::end(); ?>
      
</div>
