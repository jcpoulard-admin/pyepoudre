<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\ResetPassword */

$this->title = 'Reinitialiser  mot de passe';
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="bg-success">
     <div class="row">
         <div class="col-lg-3"></div>
         <div class="col-lg-6">
             <h1><?= Html::encode($this->title) ?></h1>

            <p>SV, Choisir un nouveau mot de passe :</p>
         </div>
         <div class="col-lg-3"></div>
     </div> 
    

    
        
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <div class="row">
                <div class="col-lg-2"></div>
                <div col-lg-4>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($model, 'retypePassword')->passwordInput() ?>
                </div>
                <div class="col-lg-2"></div>
            </div>
                
          
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>

 </div>