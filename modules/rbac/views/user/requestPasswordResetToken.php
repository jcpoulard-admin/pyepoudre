<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\PasswordResetRequest */

$this->title = 'Demande de reinitialisation de mot de passe';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6">
             <h1><?= Html::encode($this->title) ?></h1> 
             <p>SVP, saisir votre email. Un lien vous sera envoy&eacute; par email pour r&eacute;initialiser votre mot de passe.</p>
        </div>
        <div class="col-lg-3"></div>
    </div>
   

   

    <div class="row">
        <div class="col-lg-3">
            
            </div>
        <div class="col-lg-6">
            
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?= $form->field($model, 'email') ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-3">
            
        </div>
    </div>

