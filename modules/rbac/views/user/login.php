<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Login */

$this->title = Yii::t('app', 'Login');

//$school_acronym = infoGeneralConfig('school_acronym');

?>


 <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-light">
            <div class="auth-box bg-dark border-top border-secondary">
                <div id="loginform">
                    <p></p>
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db"><img src="<?= Url::to("@web/portal_assets/images/pyepoudre.png") ?>" width="150" height="50" alt="logo" /></span>
                    </div>
                    <!-- Form -->
                  <!--  <form class="form-horizontal m-t-20" id="loginform" action="index.html"> -->
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <div class="row p-b-30">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    
                                   <!-- <?= $form->field($model, 'username')->label(false)->textInput(['placeholder'=> Yii::t('app', 'Username'), 'class' => 'form-control form-control-lg','aria-label'=>'Username'])->error(false); ?> 
                                   -->
                                   
                                     <input type="text" name="Login[username]" id="login-username" class="form-control form-control-lg" placeholder="Nom utilisateur" aria-label="Username" aria-describedby="basic-addon1" required="">
                               
                                   </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" name="Login[password]" id="login-password" class="form-control form-control-lg" placeholder="Mot de passe" aria-label="Password" aria-describedby="basic-addon1" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <a class="btn btn-info" id="to-recover" href="<?= Yii::getAlias('@web') ?>/index.php/rbac/user/request-password-reset"><i class="fa fa-lock m-r-5"></i> Mot de passe oubli&eacute; ?</a>
                                        <button class="btn btn-success float-right" type="submit" name="login-button">Se connecter</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <?php ActiveForm::end(); ?>
               <!--     </form> -->
                </div>
                <div id="recoverform">
                    <div class="text-center">
                        <span class="text-white">Enter your e-mail address below and we will send you instructions how to recover a password.</span>
                    </div>
                    <div class="row m-t-20">
                        <!-- Form -->
                        <form class="col-12" action="index.html">
                            <!-- email -->
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-danger text-white" id="basic-addon1"><i class="ti-email"></i></span>
                                </div>
                                <input type="text" class="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                            <!-- pwd -->
                            <div class="row m-t-20 p-t-20 border-top border-secondary">
                                <div class="col-12">
                                    <a class="btn btn-success" href="#" id="to-login" name="action">Back To Login</a>
                                    <button class="btn btn-info float-right" type="button" name="action">Recover</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
 </div>
