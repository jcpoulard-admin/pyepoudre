<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\modules\rbac\components\RouteRule; 



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel mdm\admin\models\searchs\AuthItem */
/* @var $context mdm\admin\components\ItemController */




$context = $this->context;
 
$labels = $context->labels();
$link_anchor = null;
$base_url = null;
                       
if($labels['Item']=="Permission")
  {
    $this->title = Yii::t('app', 'Permissions');
    $link_anchor = "per"; 
    $base_url = "permission";
  }
else
  {
    $this->title = Yii::t('app', 'Roles');
    $link_anchor = "rol";
    $base_url = "role";
   }


$rules = array_keys(Yii::$app->getAuthManager()->getRules());
$rules = array_combine($rules, $rules);
unset($rules[RouteRule::RULE_NAME]);
?>

  <?= $this->render('//layouts/rbacLayout'); ?>

 <div class="row">
     <div class="" style="width:auto;float:left; margin-left:20px;">
      <?= Html::a('<i class="fa fa-plus"></i> ', ['create','wh'=>$link_anchor], ['class' => 'btn btn-primary btn-sm','title'=>Yii::t('app','Add')]) ?>
            
    </div>
    
    <div class="col-lg-6">
         <h3><?= $this->title; ?></h3>
     </div>
        
     
        
</div> 


<div class="wrapper wrapper-content role-index">
       
       <div class="col-md-14 table-responsive">
				        <table class='table table-striped table-bordered table-hover dataTables-example'>
				         <thead>
				            <tr>
				            <th><?= Yii::t('app','Name'); ?></th>
				            <th><?= Yii::t('app','Rule Name'); ?></th>
				           <th><?= Yii::t('app','Description'); ?></th>
				            <th></th>
				            
				            </tr>
				        </thead>
				        <tbody>     
<?php 

    $dataItems = $dataProvider->getModels();
    
          foreach($dataItems as $item)
           {
           	   echo '  <tr >
                                                    <td >'.$item->name.' </td>
                                                    <td >'.$item->ruleName.' </td>
                                                    <td >'.$item->description.' </td>
                                                    
                                                    <td >'; 
                       
                                                          
                                                          if(Yii::$app->user->can('rbac-'.$base_url.'-view')) 
                                                              {
												                echo Html::a('<span class="fa fa-eye"></span>', Yii::getAlias('@web').'/index.php/rbac/'.$base_url.'/view?id='.$item->name.'&wh='.$link_anchor, [
                            'title' => Yii::t('app', 'View'),
                ]); 
                                                              }
                                                              
                                                            if(Yii::$app->user->can('rbac-'.$base_url.'-update')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-edit"></span>', Yii::getAlias('@web').'/index.php/rbac/'.$base_url.'/update?id='.$item->name.'&wh='.$link_anchor, [
                            'title' => Yii::t('app', 'Update'),
                ]); 
                                                              }
                                                              
                                                           if(Yii::$app->user->can('rbac-'.$base_url.'-delete')) 
                                                              {
												                echo '&nbsp'.Html::a('<span class="fa fa-trash"></span>', Yii::getAlias('@web').'/index.php/rbac/'.$base_url.'/delete?id='.$item->name.'&wh='.$link_anchor, [
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this user?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                        ]); 
                                                              }
                                                           
                                                              
                                             echo ' </td>
                                                    
                                                </tr>';           	 
           	 }
?>
                             </tbody>
                    </table>
        

                 </div>



 
</div>


<?php

    $script = <<< JS
    $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: "Rechercher",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                        sortAscending:  ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    { extend: 'copy'},
                   // {extend: 'csv'},
                    {extend: 'excel', title: 'Permissions List'},
                    {extend: 'pdf', title: 'Permissions List'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

JS;
$this->registerJs($script);

?>