<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\rbac\components\RouteRule;
use app\modules\rbac\AutocompleteAsset;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model mdm\admin\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var $context mdm\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$rules = Yii::$app->getAuthManager()->getRules();
unset($rules[RouteRule::RULE_NAME]);
$source = Json::htmlEncode(array_keys($rules));

$js = <<<JS
    $('#rule_name').autocomplete({
        source: $source,
    });
JS;
AutocompleteAsset::register($this);
$this->registerJs($js);
?>

<div class="auth-item-form">
    <?php $form = ActiveForm::begin(['id' => 'item-form']); ?>
    
     <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>
        </div>
        
        <div class="col-lg-6">
            <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>
       </div>
    </div>
       
       
     <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'ruleName')->textInput(['id' => 'rule_name']) ?>
            </div>
        
        <div class="col-lg-6">
             <?= $form->field($model, 'data')->textarea(['rows' => 1]) ?>
        </div>
    </div>
    
  <div class="row">
        <div class="col-lg-4">
            
        </div>
        <div class="col-lg-4">
           <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save'), ['name' => $model->isNewRecord ? 'create' : 'update', 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        
        <?= Html::resetButton(Yii::t('app', 'Cancel'),['name'=>'cancel','class'=>'btn btn-warning']) ?>
        
        <?php //back button   
          $url=Yii::$app->request->referrer; 
          echo '<span class="btn btn-default"><a href="'.$url.'" >'.Yii::t('app','Back').'</a>';
        
        ?>
            </div>
        </div>
        <div class="col-lg-4">
            
        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>
