-- MySQL Workbench Synchronization
-- Generated: 2019-11-30 11:41
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Jean Came Poulard

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `pyepoudre`.`layouts` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(255) NULL DEFAULT NULL,
  `date_update` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(255) NULL DEFAULT NULL,
  `is_publish` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `slug_UNIQUE` (`slug` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`images` (
  `id` INT(11) NULL DEFAULT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `layouts_id` INT(11) NULL DEFAULT NULL,
  `url` TEXT NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(255) NULL DEFAULT NULL,
  `date_update` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(255) NULL DEFAULT NULL,
  `is_publish` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `slug_UNIQUE` (`slug` ASC),
  INDEX `fk_images_layouts_idx` (`layouts_id` ASC),
  CONSTRAINT `fk_images_layouts`
    FOREIGN KEY (`layouts_id`)
    REFERENCES `pyepoudre`.`layouts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`menus` (
  `id` INT(11) NULL DEFAULT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `is_parent` TINYINT(4) NULL DEFAULT NULL,
  `parent_id` INT(11) NULL DEFAULT NULL,
  `layouts_id` INT(11) NULL DEFAULT NULL,
  `absolute_position` INT(11) NULL DEFAULT NULL,
  `relative_position` INT(11) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(255) NULL DEFAULT NULL,
  `date_update` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(255) NULL DEFAULT NULL,
  `publish` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `slug_UNIQUE` (`slug` ASC),
  INDEX `fk_menus_menus1_idx` (`parent_id` ASC),
  INDEX `fk_menus_layouts1_idx` (`layouts_id` ASC),
  CONSTRAINT `fk_menus_menus1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `pyepoudre`.`menus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_menus_layouts1`
    FOREIGN KEY (`layouts_id`)
    REFERENCES `pyepoudre`.`layouts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`calendars` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `date_start` DATETIME NOT NULL,
  `date_end` DATETIME NULL DEFAULT NULL,
  `location` VARCHAR(255) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(255) NULL DEFAULT NULL,
  `date_update` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`ressources` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `short_name` VARCHAR(128) NOT NULL,
  `properties` TEXT NULL DEFAULT NULL,
  `description` LONGTEXT NULL DEFAULT NULL,
  `url` VARCHAR(255) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(255) NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  UNIQUE INDEX `short_name_UNIQUE` (`short_name` ASC),
  UNIQUE INDEX `url_UNIQUE` (`url` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`components` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL DEFAULT NULL,
  `type` VARCHAR(45) NOT NULL,
  `layouts_id` INT(11) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `create_date` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(45) NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(45) NULL DEFAULT NULL,
  `is_publish` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_components_layouts1_idx` (`layouts_id` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  CONSTRAINT `fk_components_layouts1`
    FOREIGN KEY (`layouts_id`)
    REFERENCES `pyepoudre`.`layouts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`table1` (
  `id` INT(11) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`components_has_images` (
  `components_id` INT(11) NOT NULL,
  `images_id` INT(11) NOT NULL,
  `create_date` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(45) NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(45) NULL DEFAULT NULL,
  `is_publish` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`components_id`, `images_id`),
  INDEX `fk_components_has_images_images1_idx` (`images_id` ASC),
  INDEX `fk_components_has_images_components1_idx` (`components_id` ASC),
  CONSTRAINT `fk_components_has_images_components1`
    FOREIGN KEY (`components_id`)
    REFERENCES `pyepoudre`.`components` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_components_has_images_images1`
    FOREIGN KEY (`images_id`)
    REFERENCES `pyepoudre`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`articles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `slug` VARCHAR(255) NOT NULL,
  `body` LONGTEXT NULL DEFAULT NULL,
  `menus_id` INT(11) NULL DEFAULT NULL,
  `layouts_id` INT(11) NULL DEFAULT NULL,
  `images_id` INT(11) NULL DEFAULT NULL,
  `components_id` INT(11) NULL DEFAULT NULL,
  `is_publish` TINYINT(4) NULL DEFAULT NULL,
  `create_date` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(45) NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_articles_menus1_idx` (`menus_id` ASC),
  INDEX `fk_articles_layouts1_idx` (`layouts_id` ASC),
  INDEX `fk_articles_images1_idx` (`images_id` ASC),
  INDEX `fk_articles_components1_idx` (`components_id` ASC),
  CONSTRAINT `fk_articles_menus1`
    FOREIGN KEY (`menus_id`)
    REFERENCES `pyepoudre`.`menus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_articles_layouts1`
    FOREIGN KEY (`layouts_id`)
    REFERENCES `pyepoudre`.`layouts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_articles_images1`
    FOREIGN KEY (`images_id`)
    REFERENCES `pyepoudre`.`images` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_articles_components1`
    FOREIGN KEY (`components_id`)
    REFERENCES `pyepoudre`.`components` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `label` VARCHAR(255) NOT NULL,
  `value` TEXT NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `category` VARCHAR(45) NULL DEFAULT NULL,
  `create_date` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(45) NULL DEFAULT NULL,
  `update_date` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `pyepoudre`.`manage_ressources` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ressources_id` INT(11) NOT NULL,
  `date_allocation` DATETIME NULL DEFAULT NULL,
  `end_allocation` DATETIME NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `allocate_to` VARCHAR(255) NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `create_by` VARCHAR(45) NULL DEFAULT NULL,
  `date_update` DATETIME NULL DEFAULT NULL,
  `update_by` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_manage_ressources_ressources1_idx` (`ressources_id` ASC),
  CONSTRAINT `fk_manage_ressources_ressources1`
    FOREIGN KEY (`ressources_id`)
    REFERENCES `pyepoudre`.`ressources` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- RBAC 
--
-- Structure de la table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `person_id` int(11) DEFAULT NULL,
  `is_parent` int(11) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(256) NOT NULL,
  `password_reset_token` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '10',
  `last_ip` varchar(225) DEFAULT NULL COMMENT 'add by logipam for session',
  `last_activity` datetime DEFAULT NULL COMMENT 'add by logipam for session',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Index pour la table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Index pour la table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3188;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `user`
  DROP `person_id`,
  DROP `is_parent`;

CREATE TABLE IF NOT EXISTS `stat` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `stat` 
ADD COLUMN `article_visit` VARCHAR(255) NULL DEFAULT NULL AFTER `id`,
ADD COLUMN `menu_click` VARCHAR(255) NULL DEFAULT NULL AFTER `article_visit`,
ADD COLUMN `image_click` VARCHAR(255) NULL DEFAULT NULL AFTER `menu_click`,
ADD COLUMN `adresse_ip` VARCHAR(255) NULL DEFAULT NULL AFTER `image_click`,
ADD COLUMN `browser_tyoe` VARCHAR(255) NULL DEFAULT NULL AFTER `adresse_ip`,
ADD COLUMN `device_type` VARCHAR(255) NULL DEFAULT NULL AFTER `browser_tyoe`,
ADD COLUMN `date_visit` DATETIME NULL DEFAULT NULL AFTER `device_type`;

CREATE TABLE IF NOT EXISTS `cms_index` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `elemment_id` INT(11) NULL DEFAULT NULL,
  `element_slug` VARCHAR(255) NULL DEFAULT NULL,
  `cms_indexcol` VARCHAR(255) NULL DEFAULT NULL,
  `element_type` VARCHAR(64) NOT NULL,
  `index_value` LONGTEXT NULL DEFAULT NULL,
  `date_create` DATETIME NULL DEFAULT NULL,
  `index_keywords` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- 19 decembre 2019
-- Modifier table utilisateur 
ALTER TABLE `user` ADD `full_name` VARCHAR(255) NOT NULL AFTER `username`, ADD INDEX (`full_name`);

-- 7 janvier 2020 
-- modification de la base de donnees 
ALTER TABLE articles DROP FOREIGN KEY fk_articles_images1;
ALTER TABLE articles DROP INDEX  fk_articles_images1_idx;
ALTER TABLE `articles` CHANGE `images_id` `images_id` TEXT NULL DEFAULT NULL;

-- Configuration generale 
INSERT INTO `config` (`id`, `name`, `label`, `value`, `description`, `category`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (NULL, 'tri_element_non_pertinent', "Tri des elements non pertinent dans l\'index', 'le, la, les, l\', the, a, un, une, des, aux, au, a, à, et, il, elle, vous, ils, elles", "Ensemble d’éléments non pertinents dans l\'indexation de la recherche sur le site web. Ecrire chaque mot séparé par une virgule. ", NULL, '2020-01-07 00:00:00', 'LOGIPAM', NULL, NULL);

ALTER TABLE `cms_index` CHANGE `elemment_id` `element_id` INT(11) NULL DEFAULT NULL;

-- 8 janvier 2020
ALTER TABLE `menus` ADD `is_home` BOOLEAN NULL AFTER `publish`;

-- 10 janvier 2020 
ALTER TABLE `articles` ADD `is_home` BOOLEAN NULL AFTER `is_publish`;

-- 23 janvier 2020
ALTER TABLE `components` ADD `is_home` BOOLEAN NULL AFTER `is_publish`;

-- 28 janvier 2020

ALTER TABLE `images` ADD `componnent_id` INT NULL AFTER `url`;
ALTER TABLE `images` CHANGE `componnent_id` `component_id` INT(11) NULL DEFAULT NULL;
ALTER TABLE `images` ADD FOREIGN KEY (`component_id`) REFERENCES `components`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;

-- 29 janvier 2020 
ALTER TABLE `images` CHANGE `is_publish` `is_publish` INT NULL DEFAULT NULL;

-- 2 fevrier 2020 (02022020) 
ALTER TABLE `calendars` ADD `media` VARCHAR(255) NULL AFTER `location`;
ALTER TABLE `calendars` ADD `description` LONGTEXT NULL AFTER `media`;

-- 04 fevrier 2020 
UPDATE `layouts` SET `name` = 'Blog', `slug` = 'blog' WHERE `layouts`.`id` = 2;
UPDATE `menus` SET `layouts_id` = '2' WHERE `menus`.`id` = 3;
UPDATE `menus` SET `layouts_id` = '1' WHERE `menus`.`id` = 20;
UPDATE `menus` SET `layouts_id` = '1' WHERE `menus`.`id` = 17;
UPDATE `menus` SET `layouts_id` = '1' WHERE `menus`.`id` = 13;
UPDATE `menus` SET `layouts_id` = '1' WHERE `menus`.`id` = 10;
UPDATE `menus` SET `layouts_id` = NULL WHERE `menus`.`id` = 6;
UPDATE `menus` SET `layouts_id` = NULL WHERE `menus`.`id` = 7;

-- 05 fevrier 2020 
INSERT INTO `config` (`id`, `name`, `label`, `value`, `description`, `category`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (NULL, 'twitter_account', 'Compte Twitter', 'jcpoulard', 'Placer le nom de l\'utilisateur twitter pour l\'affichage du flux. ', NULL, '2020-02-05 00:00:00', 'LOGIPAM', NULL, NULL);

-- 07 fevrier 2020 
ALTER TABLE `images` DROP `componnent_id`; 

-- 15 fevrier 2020 
UPDATE `layouts` SET `name` = 'Menu Principal', `slug` = 'menu-principal' WHERE `layouts`.`id` = 1;
ALTER TABLE `menus` CHANGE `layouts_id` `layouts_id` INT(11) NULL DEFAULT '1';
UPDATE `menus` SET `layouts_id` = '1' WHERE `menus`.`id` = 1;

-- 18 fevrier 2020 
ALTER TABLE `calendars` ADD `time_start` TIME NULL AFTER `date_end`; 
ALTER TABLE `calendars` ADD `time_end` TIME NULL AFTER `time_start`; 

-- 19 fevrier 2020 
ALTER TABLE `calendars`
  DROP `time_start`,
  DROP `time_end`;

-- 21 fevrier 2020 
ALTER TABLE `calendars` ADD `prix` VARCHAR(255) NULL AFTER `description`; 

-- 28 fevrier 2020 
ALTER TABLE `calendars` ADD `intervenant` VARCHAR(255) NULL AFTER `prix`; 

-- 05 mai 2020
-- Creation des epingles 
CREATE TABLE `epingles` (
  `id` int NOT NULL,
  `nom_epingle` varchar(255) NOT NULL,
  `urtl_epingle` varchar(255) NOT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 06 mai 2020 
ALTER TABLE `epingles` ADD `is_publish` BOOLEAN NOT NULL AFTER `urtl_epingle`; 
ALTER TABLE `epingles` CHANGE `id` `id` INT NOT NULL AUTO_INCREMENT, add PRIMARY KEY (`id`); 
ALTER TABLE `epingles` CHANGE `urtl_epingle` `url_epingle` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL; 

-- 10 mai 2020
-- ajout valeur dans config
INSERT INTO `config` (`id`, `name`, `label`, `value`, `description`, `category`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (NULL, 'show_block_actualites', 'Afficher actualitees', '1', 'Afficher le block actuialites', NULL, '2020-05-10 14:06:17', 'LOGIPAM', NULL, NULL);

-- 26 mai 2020 
-- Pour les articles long a ajouter lire la suite 
ALTER TABLE `articles` ADD `is_long` BOOLEAN NULL AFTER `is_home`; 












