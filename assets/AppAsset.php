<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'templates/fontawesome-free/css/fontawesome.min.css',
        'templates/matrix/assets/libs/flot/css/float-chart.css',
        'templates/matrix/dist/css/style.min.css',
        'templates/libs/jdatatables/datatables.min.css',
        'templates/matrix/assets/libs/select2/dist/css/select2.min.css',
        'templates/matrix/assets/libs/quill/dist/quill.snow.css',
        'templates/libs/css-common/custom.css',
        'templates/matrix/assets/libs/magnific-popup/dist/magnific-popup.css',
        'templates/matrix/assets/libs/fullcalendar/dist/fullcalendar.min.css',
        'templates/matrix/assets/extra-libs/calendar/calendar.css',
        'templates/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
        'templates/video_popup/grt-youtube-popup.css'
       // 'templates/bootstrap-datetimepicker/css/bootstrap-datetimepicker-standalone.css',
                
    ];
    public $js = [
       // 'templates/matrix/assets/libs/jquery/dist/jquery.min.js',
        'templates/matrix/dist/js/custom.min.js',
        
        'templates/matrix/assets/libs/popper.js/dist/umd/popper.min.js',
        'templates/matrix/assets/libs/bootstrap/dist/js/bootstrap.min.js',
        'templates/matrix/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js',
        'templates/matrix/assets/extra-libs/sparkline/sparkline.js',
        'templates/matrix/dist/js/waves.js',
        'templates/matrix/dist/js/sidebarmenu.js',
        
        'templates/matrix/assets/libs/flot/excanvas.js',
        'templates/matrix/assets/libs/flot/jquery.flot.js',
        'templates/matrix/assets/libs/flot/jquery.flot.pie.js',
        'templates/matrix/assets/libs/chart/matrix.interface.js',
        'templates/matrix/assets/libs/chart/excanvas.min.js',
        'templates/matrix/assets/libs/flot/jquery.flot.time.js',
        'templates/matrix/assets/libs/flot/jquery.flot.stack.js',
        'templates/matrix/assets/libs/flot/jquery.flot.crosshair.js',
        'templates/matrix/assets/libs/chart/jquery.peity.min.js',
        'templates/matrix/assets/libs/chart/matrix.charts.js',
        'templates/matrix/assets/libs/chart/jquery.flot.pie.min.js',
        'templates/matrix/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js',
        'templates/matrix/assets/libs/chart/turning-series.js',
        'templates/matrix/dist/js/pages/chart/chart-page-init.js',
        
        'templates/libs/jdatatables/datatables.min.js',
        
        'templates/matrix/assets/libs/select2/dist/js/select2.full.min.js',
        'templates/matrix/assets/libs/select2/dist/js/select2.min.js',
        'templates/matrix/assets/libs/quill/dist/quill.min.js', 
        
        'templates/share/ckeditor5-build-classic/build/ckfinder/ckfinder.js',
        
        'templates/share/ckeditor5-build-classic/build/ckeditor.js',
         'templates/share/ckeditor5-build-classic/build/translations/fr.js',
         
        //'templates/fontawesome-free/js/fontawesome.min.js',
        //'templates/ckeditor/build/ckeditor.js',
        'templates/matrix/assets/libs/moment/min/moment.min.js',
        'templates/matrix/assets/libs/fullcalendar/dist/fullcalendar.min.js',
        'templates/matrix/assets/libs/fullcalendar/dist/locale-all.js', 
        //'templates/matrix/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'templates/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        'templates/video_popup/grt-youtube-popup.js',
        
        
        
       // 'templates/matix/dist/js/pages/calendar/cal-init.js',
        
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}


   
  
   
    