<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use app\modules\rbac\models\User;
use app\modules\rbac\models\Assignment;
use app\modules\cms\models\Config; 
use app\modules\cms\models\Article; 


function getRoleByUserId($user_id)
  {
        $modelAssignment= new Assignment(0,0,[]);
              $user_role = $modelAssignment->getRoleByUserId($user_id); 
              
       return $user_role;  
              
   }
   
function currentUser()
  {
  	   if( (Yii::$app->user->identity->username=="_super_")||(Yii::$app->user->identity->username=="logipam") )
                  return "LOGIPAM";
        else
           return Yii::$app->user->identity->username;
  	}   
   
 function mailsend($to,$from,$subject,$message){
        
        $mail = Yii::app()->Smtpmail;
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        $mail->Host = infoGeneralConfig("email_relay_host");
        $mail->Port = infoGeneralConfig("host_relay_port"); // or 587
        $mail->Username = infoGeneralConfig("email_relay_username");
        $mail->Password = infoGeneralConfig("host_relay_password");
        $mail->IsHTML(true);
        $mail->SetFrom($from, infoGeneralConfig('school_name'));
        $mail->Subject    = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to, "");
        if(!$mail->Send()) {
           // echo "Mailer Error: " . $mail->ErrorInfo;
            return 0;
        }else {
            //echo "Message sent!";
            return 1;
        }
    }
    
function createIndex($words, $article){
    //$words = array('in', 'at', 'on', 'etc..');
    $pattern = '/\b(?:' . join('|', $words) . ')\b/i';
    $article_clean = preg_replace($pattern, ' ', strip_tags($article));
    return $article_clean; 
    
} 

function infoGeneralConfig($param_value){
        $item_value=null;
                
       $query = Config::find()
                ->where(['name'=>$param_value])
                ->all();

         if(isset($query)&&($query!=null))
           { foreach($query as $q)
                $item_value=$q->value;
           }
            
            return $item_value;
               
   }
 /**
  * 
  * @param int (id article) $article
  * @return array of id article
  */  
function relatedArticle($article){
    $similar_id = [];
    $one_article = Article::findOne($article);
    if(isset($one_article->id)){
    $sql_src = "SELECT * FROM articles WHERE id <> $one_article->id AND is_publish = 1"; 
    $all_other_articles = Article::findBySql($sql_src)->all();
    $k = 0; 
    foreach($all_other_articles as $aoa){
        $sim = similar_text(strip_tags($one_article->body), strip_tags($aoa->body), $perc);
        if($perc > 15.00){
            $similar_id[$k] = $aoa->id; 
            $k++;
        }
    }   

    return $similar_id; 
    }else{
        return $similar_id;
    }
}

/**
 * Truncates the given string at the specified length.
 *
 * @param string $str The input string.
 * @param int $width The number of chars at which the string will be truncated.
 * @return string
 */
function truncate($str, $width,$url, $title) {
    //  $title
    // <span class='fa fa-chevron-right'> </span>
    $url_construct = "<a class='btn '  href='$url'>
                               Lire la suite...	
                             </a>"; 
    return strtok(wordwrap($str, $width, "...\n"), "\n").'<br/>'.$url_construct;
}

function closetags($html) {
    preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
    $openedtags = $result[1];
    preg_match_all('#</([a-z]+)>#iU', $html, $result);
    $closedtags = $result[1];
    $len_opened = count($openedtags);
    if (count($closedtags) == $len_opened) {
        return $html;
    }
    $openedtags = array_reverse($openedtags);
    for ($i=0; $i < $len_opened; $i++) {
        if (!in_array($openedtags[$i], $closedtags)) {
            $html .= '</'.$openedtags[$i].'>';
        } else {
            unset($closedtags[array_search($openedtags[$i], $closedtags)]);
        }
    }
    return $html;
} 