<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PortalAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'portal_assets/vendor/bootstrap/css/bootstrap.min.css',
        'portal_assets/css/modern-business.css',
        'portal_assets/css/custom.css',
        'https://use.fontawesome.com/releases/v5.5.0/css/all.css',
        'portal_assets/css/templatemo-style.css',
        'portal_assets/css/programmation.css',
        'portal_assets/css/mbr-additional.css',
        'portal_assets/css/style-pye-de-paj.css',
       // 'portal_assets/css/bootstrap-reboot.min.css',
        //'portal_assets/css/mod_nobossbanners.min.css',
       // 'portal_assets/css/mod_nobosscalendar.min.css',
        // 'portal_assets/css/style.min.css',
        
        
       // 'css/site.css',
       // 'templates/matrix/assets/libs/flot/css/float-chart.css',
       // 'templates/matrix/dist/css/style.min.css',
    ];
    public $js = [
        'portal_assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
        'portal_assets/js/modernizr.min.js',
        'portal_assets/js/custom.js',
       
        /*
        'templates/matrix/dist/js/custom.min.js',
        'templates/matrix/assets/libs/jquery/dist/jquery.min.js',
        'templates/matrix/assets/libs/popper.js/dist/umd/popper.min.js',
        'templates/matrix/assets/libs/bootstrap/dist/js/bootstrap.min.js',
        'templates/matrix/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js',
        'templates/matrix/assets/extra-libs/sparkline/sparkline.js',
        'templates/matrix/dist/js/waves.js',
        'templates/matrix/dist/js/sidebarmenu.js',
        
        'templates/matrix/assets/libs/flot/excanvas.js',
        'templates/matrix/assets/libs/flot/jquery.flot.js',
        'templates/matrix/assets/libs/flot/jquery.flot.pie.js',
         * 
         */
        
        
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
